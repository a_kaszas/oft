SLIDES.slide2 = function() {

    //private
    function onInit(){
    	$('#play').hide();
    	
    	 $("#slide2 .reset").removeAttr('style');
    
    	header = $(".header-subtext2");
    	menuBox = $(".index-menu");
    	videoWrapper = $(".video-wrapper");
    	topB = $(".page2top");
        
        
        $(".page2-background").on("click", function () {
	      	pos = $(".index-menu").css("marginLeft");
	      	//console.log(pos); 
	      	$('video').get(0).pause();
	      	
	      	if(pos == "741px") {
		      	 TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
	      	}else{
		      	TweenMax.to($(".index-menu"), 0, {marginLeft: "741" ,delay:0, ease:Elastic.easeIn});
	      	}
	      	
        })
        
        menuBox.find("div").on("click", function () {
	        current_slide = $(this).attr("class");
        	switch (current_slide) {
		        case "link1" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 1);
		        break;
		        case "link2" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 2);
		        break;
		        case "link3" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 3);
		        break;
		        case "link4" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 4);
		        break;
		        case "link5" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 5);
		        break;
		        case "text-link1" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 1);
		        break;
		        case "text-link2" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 2);
		        break;
		        case "text-link3" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 3);
		        break;
		        case "text-link4" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 4);
		        break;
		        case "text-link5" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 5);
		        break;
	        }
        })
         
         $("video").on("click", function () {
	        $(this).get(0).play(); 
	        $('#play').hide();   
        })

		var checkTime = setInterval(function(){ 
		    if($('video').get(0).currentTime > 0 && $('video').get(0).ended!=true && $('video').get(0).paused != true) {
		       //console.log($('video').get(0).currentTime);
		       $('#play').hide();
		       TweenMax.to($(".header-subtext2"), 1, {marginLeft: "-380" ,delay:0, ease:Expo.easeOut});
			   TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
		    } else {
				videoend();
			}
		   
	    }, 800);  
    
	          
        $("#play").on("click", function () {
	        $('video').get(0).play();
	        //videoId = $(this).attr("class");
	        //console.log(videoId);
	        //$(this).parent().find("video").get(0).play();
	        //$(this).hide();
	        //TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
			//TweenMax.to($(".header-subtext2"), 1, {marginLeft: "-380" ,delay:0, ease:Elastic.easeOut});
        })
        
        
/*
        sublime.ready(function(){
			var player = sublime.player('59de3905');
			
			sublime.player('59de3905').on({
			play: function(player) { 
				TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
				TweenMax.to($(".header-subtext2"), 1, {marginLeft: "-380" ,delay:0, ease:Elastic.easeOut});
				
				 },
			end:   function(player) { 
				
				
				
				
			}
		});
		
		
		});
*/
        var a = 0;
        var b = 0;
        var c = 0;
        var j = 0;

        $("#play-start").on("click", function () {
            ga('send', 'event', 'Video', 'Play', 'Start Video');
            $("#slide1 video").get(0).play();
            $(this).hide();
        })
        
        var vid_play = document.getElementById("59de3905");
        vid_play.onplay = function() {
            /* Video Watched */
            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.25 * (this.duration)) {
                    if (a < 1) {
                        console.log("watched 25%");
                        ga('send', 'event', 'Videos', 'Watched 25%', $(this).attr('title'));
                    }
                    a = a + 1;
                }
            });

            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.50 * (this.duration)) {
                    if (b < 1) {
                        console.log("watched 50%");
                        ga('send', 'event', 'Videos', 'Watched 50%', $(this).attr('title'));
                    }
                    b = b + 1;
                }
            });

            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.75 * (this.duration)) {
                    if (c < 1) {
                        console.log("watched 75%");
                        ga('send', 'event', 'Videos', 'Watched 75%', $(this).attr('title'));
                    }
                    c = c + 1;
                }
            });

            /* Video Finished, Thanks */
            $("#59de3905").bind("ended", function() {
                if (j < 1) {
                    console.log("Finished 100%");
                    ga('send', 'event', 'Videos', 'Finished 100%', $(this).attr('title'));
                }
                j = j + 1;
            });
        };
        
        /*var vid_pause = document.getElementById("pit");
        vid_pause.onpause = function() {
            //console.log("pause");
            ga('send', 'event', 'Video', 'Play', 'Pause Video');
        };
        var vid_complete = document.getElementById("pit");
        vid_complete.onended = function() {
            //console.log("complete");
            ga('send', 'event', 'Video', 'Play', 'Complete Video');
        };
        var vid_continue = document.getElementById("pit");
        vid_continue.onplaying = function() {
            //console.log("continue");
            ga('send', 'event', 'Video', 'Play', 'Continue Video');
        };*/
        
        
    }

    function onEnter() {
	    TweenMax.to($(".index-menu"), 0, {marginLeft: "741" ,delay:0, ease:Elastic.easeIn});
            TweenMax.to($(".header-subtext2"), 1, {marginLeft: "0" ,delay:0, ease:Power4.easeIn});
        //TweenMax.from(header, 2, {left: "-400" ,delay:1, ease:Bounce.Elastic,onStart: function() {header.show()}});
        //TweenMax.from(menuBox, 1, {marginLeft: "1000" ,delay:2, ease:Bounce.Elastic,onStart: function() {menuBox.show()}});
        //TweenMax.from(videoWrapper, 3, {opacity: "0" ,delay:1, ease:Bounce.Elastic,onStart: function() {videoWrapper.show()}});
        //TweenMax.from(topB, 1, {top: "-292" ,delay:1, ease:Bounce.Elastic,onStart: function() {topB.show()}});

    }

    function onExit() {
    	 //$('#play').show();
    	 //console.log(videoId);
    	 //$("video").get(0).pause(); 
    	 //var player = sublime.player('59de3905');
    	 //player.pause();
    	 
       TweenMax.killAll();
        $("#slide2 .reset").removeAttr('style');
        //TweenMax.to($(".header-subtext2"), 1, {marginLeft: "-380" ,delay:0, ease:Power4.easeOut});
        //clearInterval(checkTime);
    }

	

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();

function videoend () {
		 $('#play').show();
	     
	     TweenMax.to($(".index-menu"), 0, {marginLeft: "741" ,delay:0, ease:Elastic.easeIn});
	     //TweenMax.to($(".header-subtext2"), 1, {marginLeft: "0" ,delay:0, ease:Elastic.easeIn});
	}