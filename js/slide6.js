SLIDES.slide6 = function() {

    //private
    function onInit(){

    	baseurl = $(".slidesWrapper").attr("id");
    	
        fotoid = $(".foto-holder").attr("id");
        
        /*$('.kundenportalImg').click(function(){
			$(this).parents().find(".large_img").print();
			/* var mywindow = window.open('', '', 'height=400,width=600');
		    mywindow.document.write('<img src="'+$(this).parents().find(".large_img img").attr('src')+'">');
	        window.onload = function() {
                    mywindow.print();
                    setTimeout(function() {
                        mywindow.close();
                    }, 1000);
                };
	 	});
	 	*/
	 	
	 	$(".emailImg").on("click",function () {
		 	if($(this).parents().find(".large_img img").length > 0){
			 	sharemedia = baseurl+$(this).parents().find(".large_img img").attr("src");
		 	} else {
			 	sharemedia = $(this).parents().find(".large_img video source").attr("src");
		 	}
		 	$(".emailImg").attr("href","mailto:?subject="+$('#slide6 .subheadline').html()+"&body="+encodeURIComponent(sharemedia));
		 	
	 	})
	 	
	 	$(".facebookImg").on("click", function (){
	 		//event.preventDefault();
	 		if($(this).parents().find(".large_img img").length > 0){
			 	medialink = baseurl+$(this).parents().find(".large_img img").attr("src");
		 	} else {
			 	medialink = $(this).parents().find(".large_img video source").attr("src");
		 	}

			facebook_link = 'https://www.facebook.com/sharer/sharer.php?u='+medialink+'&t='+$('.subheadline').html();
		    window.open(facebook_link,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=650,height=600');
	 	})


		var initMatrix = function(){
		
			Options= new Array ({
				Width: 1024,
				InnerWrapper: $('.matrix_inner_wraper'),
				ContainerLength: $('.matrix_inner_wraper .matrix_container').length,
				Draggalbe:$('.matrix_draggalbe'),
				Scroller:$('.matrix_scroller'),
				Items: $('.matrix_items'),
				Overlay: $('.matrix_layer_wrapper'),
				OverlayImg: $('.content_right .large_img'),
				BtLeft: $('.matrix_controller .bt_left'),
				BtRight: $('.matrix_controller .bt_right'),
				BtLeftOverlay: $('.bt_leftOverlay'),
				BtRightOverlay: $('.bt_rightOverlay'),
				DownloadImg: $('.downloadImg'),
				BrowserImg: $('.browserImg'),
				
				closeOverlay: $('.closeOverlay'),
				current: 0,
			});
		
			var m = Options[0];
			
			m.InnerWrapper.width(m.Width*m.ContainerLength);
			
			m.BtLeft.click(function(){
				if(m.current==0){
					return false;
				}else{
					$(this).hide();
					$(".block-btns").show();
					move('right');
					moveDragg('left');
				}
				setTimeout(function(){ $(".bt_left").show(); }, 1000);
			});
			
			m.BtRight.click(function(){

				if(m.current==m.ContainerLength-1){
					return false;
				}else{
					$(this).hide();
					$(".block-btns").show();
					move('left');
					moveDragg('right');
				}
				setTimeout(function(){ $(".bt_right").show(); }, 1000);
			});
			
			m.BtLeftOverlay.click(function(){
				if($('.matrix_items.current').prev().length){
					img = $('.matrix_items.current').prev().addClass('current_tmp').attr('data-img');
					video = $('.matrix_items.current').prev().attr('data-vid');
					m.Items.removeClass('current');
					$('.matrix_items.current_tmp').removeClass('current_tmp').addClass('current');
					
					if((video != undefined) && (video.indexOf(".mp4") != -1)) {
					m.OverlayImg.html('<div class="video_wrapper"><video controls poster="'+img+'"><source src="'+video+'" type="video/mp4"></video></div>');
					} else {
					m.OverlayImg.html('<img src="'+img+'" />');	
					}
					setDownload();
				}else{
					closeLayer();
				}
			});
			
			m.BtRightOverlay.click(function(){
				//console.log("in");
				if($('.matrix_items.current').next().length){
					img = $('.matrix_items.current').next().addClass('current_tmp').attr('data-img');
					video = $('.matrix_items.current').next().attr('data-vid');
					
					m.Items.removeClass('current');
					$('.matrix_items.current_tmp').removeClass('current_tmp').addClass('current');
					
					if((video != undefined) && (video.indexOf(".mp4") != -1)) {
					m.OverlayImg.html('<div class="video_wrapper"><video controls poster="'+img+'"><source src="'+video+'" type="video/mp4"></video></div>');
					} else {
					m.OverlayImg.html('<img src="'+img+'" />');	
					}
					setDownload();
				}else{
					closeLayer();
				}
			});
		
			
		m.Items.click(function(e){
			e.preventDefault();
			img = $(this).attr('data-img');
			video = $(this).attr('data-vid');

			if((video != undefined) && (video.indexOf(".mp4") != -1)) {
				openmedia = video;
			} else {
				openmedia = img;
			}
			openLayer(openmedia);
			$(this).addClass('current');
			setDownload();
		});
		
		m.closeOverlay.click(function(e){
			e.preventDefault();
			
			closeLayer();
			$('.nextNavBtn, .prevNavBtn').css('opacity', '1');
		});
		
		openLayer = function(media){
			if((media != undefined) && (media.indexOf(".mp4") != -1)) {
				//img = $('.matrix_items.current').attr('data-img');
				m.OverlayImg.html('<div class="video_wrapper"><video controls poster="'+img+'"><source src="'+media+'" type="video/mp4"></video></div>');
			} else {
				m.OverlayImg.html('<img src="'+media+'" />');
			}
			m.Overlay.show();
			$('.nextNavBtn, .prevNavBtn').css('opacity', '0.4');
		};
		
		closeLayer = function(){
			m.Overlay.hide();
			m.Items.removeClass('current');
			$('.nextNavBtn, .prevNavBtn').css('opacity', '1');
		};
		setDownload = function(){
			if(($('.matrix_items.current').attr('data-vid') != undefined) && $('.matrix_items.current').attr('data-vid').indexOf('.mp4') != -1) {
				m.DownloadImg.attr('download','OEAMTCFahrtechnik.mp4');
				m.DownloadImg.attr('href',$('.matrix_items.current').attr('data-vid'));
				m.DownloadImg.contents()[1]['textContent'] = 'Video';
				m.BrowserImg.attr('href',$('.matrix_items.current').attr('data-vid'));
			} else {
				m.DownloadImg.attr('download','OEAMTCFahrtechnik.jpg');
				m.DownloadImg.attr('href',$('.matrix_items.current').attr('data-img'));
				m.DownloadImg.contents()[1]['textContent'] = 'Foto';
				m.BrowserImg.attr('href',$('.matrix_items.current').attr('data-img'));
			}
		}
		
		// Scroll left and right
			/*$('.matrix_scroller').mousemove(function(ev){
				var wrapperOffset = ev.offsetX;
				$('.matrix_draggalbe').mousedown(function(e){
					mOffset = e.offsetX;
					console.log(mOffset);
					if( mOffset > parseInt($('.matrix_draggalbe').position().left)){
						moveDragg('right');
						//move('right');
					}else {
						//moveDragg('left');
						//move('right');
					}
				});
				
			}); 
			
			*//*
			$(function() {
			  new Dragdealer('simple-slider', {
				  callback: function(x) {
			
				    if(x >= 0) {
					    //moveDragg('right');
					    move('left');
				    } else {
					    //moveDragg('left');
					    //move('right');
				    }
				    
				  },
				  steps: $('.matrix_container').length,
				  vertical: false
				});
			}) */
					
		    
			m.Draggalbe.width(parseInt(m.Scroller.width()) / (m.ContainerLength));
			
			
			move = function(mDirection){
				m.InnerWrapper.animate({
				    left: mDirection=='left' ? parseInt(m.InnerWrapper.css('left'))-m.Width : parseInt(m.InnerWrapper.css('left'))+m.Width,
				  }, 1000, function() {
				    $(".block-btns").hide();
				});
			}
			
			moveDragg = function(mDirection){
				m.Draggalbe.animate({
					left: mDirection=='left' ?  parseInt(m.Draggalbe.css('left'))-parseInt(m.Scroller.width()) / (m.ContainerLength) : parseInt(m.Draggalbe.css('left'))+parseInt(m.Scroller.width()) / (m.ContainerLength)}, 1000, function() {
				    mDirection=='left'? m.current--: m.current++;
				});
			};
			return true;	
		};
		
		initMatrix();
        
    }

    function onEnter() {
       
    }

    function onExit() {
       
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();