SLIDES.slide5 = function() {

    //private
    function onInit(){
    	
			
        TweenMax.killAll();
        

    }

    function onEnter() {
        
    }

    function onExit() {
        TweenMax.killAll();
        //$("#slide5 .reset").removeAttr('style');
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();