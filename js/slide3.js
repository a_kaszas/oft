SLIDES.slide3 = function() {

    //private
    function onInit(){
    
    	training = "off";
    	
    	
        $(".pkw-holder").on("click", function () {
   
        	if(training == ".moto") {
	        	
	        	$(".pkw-holder").animate({marginLeft:"0px"}, function () {
	        	$(".header-subtext3").animate({left:"-22px"});
	        	$(".header-text3").animate({left:"-110px"});
	        	$(".moto .training-menu").css({zIndex:"-1"})
	        		$(".pkw").animate({marginLeft:"0px"}, function () {
		        		
					});
				})
	        	
	        	training = ".auto";
	        	$(".moto-holder").animate({marginLeft:"370px"}, function () {
		        	$(".motorrad").animate({marginLeft:"1030px"}, function () {
			        	$(training +" .training-menu").css({zIndex:"2"})
		        	});
	        	})
	        	
        	}else{
        		
        		
	        	training = ".auto";
	        	$(".moto-holder").animate({marginLeft:"370px"}, function () {
		        	$(".motorrad").animate({marginLeft:"1030px"}, function () {
			        	$(training +" .training-menu").css({zIndex:"2"})
		        	});
	        	})
        	
        	
        	
        	}
        	
        	
        	
	        
        })
        
        $(".moto-holder").on("click", function () {
        
        	if(training == ".auto") {
	        	$(".auto .training-menu").css({zIndex:"-1"})
	        	$(".moto-holder").animate({marginLeft:"0px"}, function () {
	        		$(".motorrad").animate({marginLeft:"514px"}, function () {
		        		
					});
				})
	        	
	        	training = ".moto";
				$(".pkw-holder").animate({marginLeft:"-410px"}, function () {
	        	$(".header-subtext3").animate({left:"-385px"});
	        	$(".header-text3").animate({left:"-374px"});
	        	$(".pkw").animate({marginLeft:"-510px"}, function () {
		        	$(training + " .training-menu").css({zIndex:"2"})
	        	});
        	})
	        	
        	}else{
        
        	training = ".moto";
	        $(".pkw-holder").animate({marginLeft:"-410px"}, function () {
	        	$(".header-subtext3").animate({left:"-385px"});
	        	$(".header-text3").animate({left:"-374px"});
	        	$(".pkw").animate({marginLeft:"-510px"}, function () {
		        	$(training + " .training-menu").css({zIndex:"2"})
	        	});
        	})
        	
        	}

	        
	        
        })
        
        /*$(".next-training").on("click", function () {
	        $(training+" .training-holder.active").removeClass("active").next().addClass("active");
	        $(training+" .trainings-container.active").animate({marginLeft:"-=350px"}, function () {
		        checkSlider ();
	        })
        })
        
         $(".prev-training").on("click", function () {
	         // $(training+" .training-holder.active").removeClass("active");
	          $(training+" .training-holder.active").removeClass("active").prev().addClass("active");
			  $(training+" .trainings-container.active").animate({marginLeft:"+=350px"}, function () {
				  checkSlider ();
			  })
        })
       	*/
        
        
        $(".trainings-container").owlCarousel({
        	nav: false,
                dots: true,
                center: true,
                items:1,
                loop:false,
        });
        
        $(".training-menu div").on("click", function () {
            
            if(!($(this).hasClass("active"))) {
                $(training+" .training-holder.active").each(function () {
                    $(this).removeClass("active");
                })

                category = $(this).attr("class");

                if(category == "community") {
                        $(training+" .training-scroll").addClass("hidden");
                }else{
                        $(training+" .training-scroll").removeClass("hidden");
                }

                $(training+" .trainings-container.active").removeAttr('style').removeClass("active").addClass("hidden");

                $(training+" .trainings-container."+category).addClass("active").removeClass("hidden").find(".training-holder").first().addClass("active");

                    $(this).parent().find(".active").each(function () {
                        $(this).removeClass("active");
                    });

                    $(this).toggleClass("active");
            }
        });
        
        
    }

    function onEnter() {

    }

    function onExit() {
        $("#slide3 .moto-holder, #slide3 .pkw-holder,#slide3 .motorrad, #slide3 .pkw, .training-menu, .header-text3, .header-subtext3").removeAttr("style");
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();

function checkSlider () {
	getActive = $(training+' .training-holder.active').index();
	total = $(training+' .trainings-container.active .training-holder').length;
	
	active = getActive + 1;
	
	if(getActive == 0) {
		$(training+" .prev-training").addClass("hidden");
	}else{
		$(training+" .prev-training").removeClass("hidden");
	}
	
	if(active == total) {
		$(training+" .next-training").addClass("hidden");
	}else {
		$(training+ " .next-training").removeClass("hidden");
	}
	
}

function smallReset () {
	
}