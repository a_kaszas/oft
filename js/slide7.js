SLIDES.slide7 = function() {

    //private
    function onInit(){
        $(".flag").mouseenter(function () {
        	
        	$(".action").each(function(){
	        	$(this).removeClass("action")
        	})
        	 $(this).addClass("action");
        })
        
        $(".flag").on("click",function (){
	         $(".zentrum.active").removeClass("active");
	         $(".zentrum").addClass("hidden");
	       
	        zentrum = $(this).attr("id");
	        $("."+zentrum).removeClass("hidden").addClass("active");
	        
	        
        })
              
        
    }

    function onEnter() {
         $("#menuNav").show().animate({right:"256px"});
          $(".prevNavBtn").animate({left:"256px"});
    }

    function onExit() {
          $(".prevNavBtn").animate({left:"0px"});
          $("#menuNav").animate({right:"0"});
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();