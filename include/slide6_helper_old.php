<?php 

/*
 * Description: sanitize the Json string data, 
 *			 generate items: id, thumb_img, img
 *			 returne themed output
 */
function sanitizeCallback($data,$bildercode){
	$ignore = array("meincallback(", ")",'/srv/www/vhosts/oeamtc1.it-wms.com/htdocs/');
	$output = str_replace($ignore, "", $data);
	$output = str_replace($ignore, "", json_decode($output));
	
	$items = array();
	foreach($output as $key => $item){
		$result_explode = explode("/", $item);
		$data = array('id'=> $result_explode[2],
				   'thumb_img' => $item,
				   'img' => str_replace("tmb_", "", $item)
		);
		$items[]= $data;
	}
	return theme($items,$bildercode);
}

/*
 * Description: theme given items
 * 				return html output
 */
function theme($items,$bildercode){
	
	// toDo!!
	$special_link = "388991";
	

	$imgSchema = getImgSchema();
	
	$output = '';
	$output_tmp ='';
	
	foreach($items as $key => $item){
		
		//if output_tmp equals mod 12, wrapp them with container
		if($key%12==0 && $key!=0){
			$output .= '<div class="matrix_container container_center">'.$output_tmp.'</div>';
			$output_tmp = '';
		}
		
		
		$tempimg = explode(".jpg",$item['img']);
		$tempimg = $tempimg[0];
		
		$tempThumb = "/include/image_wrapper.php?group=".$bildercode."&imgname=".$tempimg."";
		
		
		$output_tmp .='<a href="#" data-img="'.$tempThumb.'" class="matrix_items item_'.$imgSchema[$key%12].'"><span class="matrix_img" style="background:url('.$tempThumb.')" >  <span class="icon_plus"></span> </span></a>';
		
		//check if only one image - special - TO-DO: dynamic!!
		if($bildercode == $special_link){ 
			$output = $tempThumb;
			
		//check for the rest of the items if not mod6 and wrapp them with container
		} else if($key==count($items)-1 && count($items)%12!=0){
			$output .= '<div class="matrix_container container_center">'.$output_tmp.'</div>';
		}
		
		
	}
	
	return $output;
}

/*
 * Description: Image Schema
 *				allowed values: big, big
 *				amout of values 6 Items per page
 */
function getImgSchema(){
	return array('big','big','big','big','big','big','big','big','big','big','big','big','big','big');
}

?>