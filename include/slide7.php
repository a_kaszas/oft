<div class="page2top fg-cond"><img src="img/page1/hoch-gelb-back.png" /></div>

				<div class="mainheadline">In diesen ÖAMTC Fahrtechnik Zentren</div>
				<div class="subheadline">werden Fahrprofis gemacht.</div>


				 <div class="zentrum one active">
					 <div class="headline">Zentrum Teesdorf</div>
					 <div class="bild"><img src="img/page7/zentren/teesdorf.jpg" /></div>
					 <div class="text">
						Zentrum Teesdorf<br />
						Triester Bundesstraße 120<br />
						2524 Teesdorf<br />
						Telefon: 02253 817 00-32100<br />
						<a href="mailto:fahrtechnik@oeamtc.at">fahrtechnik@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/47%C2%B057'35.0%22N+16%C2%B016'20.0%22E/@47.9596354,16.2747128,2044m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>

				  <div class="zentrum two hidden">
					 <div class="headline">Zentrum Melk/Wachauring</div>
					 <div class="bild"><img src="img/page7/zentren/melk.jpg" /></div>
					 <div class="text">
						 Zentrum Melk/Wachauring<br />
						Am Wachauring 2<br />
						3390 Melk<br />
						Telefon: 02752 528 55<br />
						<a href="mailto:fahrtechnik.wachauring@oeamtc.at">fahrtechnik.wachauring@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/48%C2%B012'43.0%22N+15%C2%B019'35.0%22E/@48.2126174,15.3269481,2029m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>

				 <div class="zentrum three hidden">
					 <div class="headline">Offroad Zentrum Stotzing</div>
					 <div class="bild"><img src="img/page7/zentren/stotzing.jpg" /></div>
					 <div class="text">
						Offroad Zentrum Stotzing<br />
						2451 Au am Leithaberge<br />
						Telefon: 02253 817 00-32100<br />
						<a href="mailto:fahrtechnik@oeamtc.at">fahrtechnik@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/47%C2%B054'50.0%22N+16%C2%B032'08.0%22E/@47.914061,16.5354803,1022m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>

				 <div class="zentrum four hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page7/zentren/marchtrenk.jpg" /></div>
					 <div class="text">
						Zentrum Marchtrenk<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon:07243 515 20<br />
						<a href="mailto:fahrtechnik.ooe@oeamtc.at">fahrtechnik.ooe@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/48%C2%B012'42.0%22N+14%C2%B007'03.0%22E/@48.211978,14.1188344,1017m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>

				 <div class="zentrum five hidden">
					 <div class="headline">Zentrum Saalfelden/Brandlhof</div>
					 <div class="bild"><img src="img/page7/zentren/brandlhof.jpg" /></div>
					 <div class="text">
						Zentrum Saalfelden/Brandlhof<br />
						Hohlwegen 4<br />
						5760 Saalfelden<br />
						Telefon: 06582 752 60<br />
						<a href="mailto:fahrtechnik.saalfelden@oeamtc.at">fahrtechnik.saalfelden@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/47%C2%B028'56.0%22N+12%C2%B049'52.0%22E/@47.4843803,12.8303845,2123m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>

				 <div class="zentrum six hidden">
					 <div class="headline">Zentrum Innsbruck</div>
					 <div class="bild"><img src="img/page7/zentren/innsbruck.jpg" /></div>
					 <div class="text">
						Zentrum Innsbruck<br />
						Handlhofweg 81<br />
						6020 Innsbruck<br />
						Telefon: 0512 379 502<br />
						<a href="mailto:fahrtechnik.tirol@oeamtc.at">fahrtechnik.tirol@oeamtc.at</a>

					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/47%C2%B013'37.5%22N+11%C2%B023'33.5%22E/@47.2264736,11.3924135,517m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>

				 <!--<div class="zentrum seven hidden">
					 <div class="headline">Zentrum Röthis</div>
					 <div class="bild"><img src="img/page7/zentren/roethis.jpg" /></div>
					 <div class="text">
						Zentrum Röthis<br />
						Bundesstraße 18<br />
						6832 Röthis<br />
						Telefon: 05522 812 20<br />
						<a href="mailto:fahrtechnik.vorarlberg@oeamtc.at">fahrtechnik.vorarlberg@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/47%C2%B018'13.8%22N+9%C2%B037'10.5%22E/@47.3034838,9.6182103,2068m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>-->

				 <div class="zentrum seven hidden">
					 <div class="headline">Zentrum Lang/Lebring</div>
					 <div class="bild"><img src="img/page7/zentren/lebring.jpg" /></div>
					 <div class="text">
						Zentrum Lang/Lebring<br />
						Jöß, Gewerbegebiet 1<br />
						8403 Lang<br />
						Telefon: 03182 401 65<br />
						<a href="mailto:fahrtechnik.lebring@oeamtc.at">fahrtechnik.lebring@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/46%C2%B050'41.0%22N+15%C2%B031'03.5%22E/@46.8448286,15.5188845,1046m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>

				 <div class="zentrum eight hidden">
					  <div class="headline">Zentrum Kalwang</div>
					 <div class="bild"><img src="img/page7/zentren/kalwang.jpg" /></div>
					 <div class="text">
						Zentrum Kalwang<br />
						Kalwang 71<br />
						8775 Kalwang<br />
						Telefon: 03846 200 90<br />
						<a href="mailto:fahrtechnik.halwang@oeamtc.at">fahrtechnik.halwang@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Kalwang+71/@47.422774,14.7592664,1032m/data=!3m2!1e3!4b1!4m2!3m1!1s0x4771c00aab70cc33:0x1b465ff16417029a" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>

				 <div class="zentrum nine hidden">
					  <div class="headline">Zentrum St. Veit a. d. Glan</div>
					 <div class="bild"><img src="img/page7/zentren/glan.jpg" /></div>
					 <div class="text">
						Zentrum St. Veit a. d. Glan<br />
						Mail 11<br />
						9300 St. Veit. a. d. Glan<br />
						Telefon: 04212 331 70<br />
						<a href="mailto:fahrtechnik.kaernten@oeamtc.at">fahrtechnik.kaernten@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/46%C2%B048'19.0%22N+14%C2%B024'32.0%22E/@46.8063566,14.4093577,2086m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>
<!--
				 <div class="zentrum eleven hidden">
					 <div class="headline">Winterzentrum Göstling</div>
					 <div class="bild"><img src="img/page7/zentren/gostling.jpg" /></div>
					 <div class="text">
						Winterzentrum Göstling<br />
						Telefon: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum@oeamtc.at</a>
				 </div>

				<a target="_blank" href="https://www.google.com/maps/place/47%C2%B049'16.8%22N+14%C2%B058'56.4%22E/@47.8204863,14.9821883,1254m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>


				 </div>

				 <div class="zentrum twelve hidden">
					 <div class="headline">Winterzentrum Hintersee </div>
					 <div class="bild"><img src="img/page7/zentren/fuschlsee.jpg" /></div>
					 <div class="text">
						Winterzentrum Hintersee<br />
						Hintersee 21<br />
						5324 Hintersee<br />
						Telefon: 02752 528 55<br />

						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum@oeamtc.at</a>
				 </div>
				 					 <a target="_blank" href="https://www.google.com/maps/place/47%C2%B044'26.9%22N+13%C2%B015'19.6%22E/@47.7408139,13.2554556,772m/data=!3m2!1e3!4b1!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>

				 </div>


				 <div class="zentrum thirteen hidden">
					 <div class="headline">Winterzentrum Schwarzau</div>
					 <div class="bild"><img src="img/page7/zentren/stuhleck.jpg" /></div>
					 <div class="text">
						Winterzentrum Schwarzau<br />
						Gegend 14<br />
						2662 Schwarzau<br />
						Telefon: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum@oeamtc.at</a>
					 </div>
				 	<a target="_blank" href="https://www.google.com/maps/place/47%C2%B050%2708.9%22N+15%C2%B041%2716.3%22E/@47.835817,15.687869,726m/data=!3m2!1e3!4b1!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>
				 </div>



				 <div class="zentrum fourteen hidden">
					 <div class="headline">Zentrum Spittal a.d.Drau/Stockenboi</div>
					 <div class="bild"><img src="img/page7/zentren/spittal.jpg" /></div>
					 <div class="text">
						Zentrum Spittal a.d.Drau/Stockenboi<br />
						Telefon: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum@oeamtc.at</a>
					 </div>
				 	<a target="_blank" href="https://www.google.com/maps/place/46%C2%B043'25.5%22N+13%C2%B029'30.1%22E/@46.7232182,13.4922868,1052m/data=!3m1!1e3!4m2!3m1!1s0x0:0x0" class="google-map"><img src="img/page7/google-maps.png" /></a>



				 </div>


				  <div class="zentrum fifteen hidden">
					 <div class="headline">Winterzentrum Schladming/Altenmarkt (S)</div>
					 <div class="bild"><img src="img/page7/zentren/spittal.jpg" /></div>
					 <div class="text">
						Winterzentrum Schladming/Altenmarkt (S)<br />

						Speicherkraftwerk Pichl/Mandling, <br />
						Zufahrt über Dschungelweg<br />

						Treffpunkt: Hotel Scheffer<br />
						
						Zauchenseestraße 27, 5541 Altenmarkt<br />
						Telefon: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum@oeamtc.at</a>
					 </div>
				 	<a target="_blank" href="https://www.google.com/maps/place/Zauchenseestra%C3%9Fe+27,+5541,+Austria/@47.3741747,13.4203524,551m/data=!3m2!1e3!4b1!4m2!3m1!1s0x4776d4a801111c79:0x6b8fe81876b26fd" class="google-map"><img src="img/page7/google-maps.png" /></a>

				 </div>-->

				 <div class="karte">
				 	<div id="one" class="flag action"></div>
				 	<div id="two" class="flag"></div>
				 	<div id="three" class="flag"></div>
				 	<div id="four" class="flag"></div>
				 	<div id="five" class="flag"></div>
				 	<div id="six" class="flag"></div>
				 	<div id="seven" class="flag"></div>
				 	<div id="eight" class="flag"></div>
				 	<div id="nine" class="flag"></div>
					 <img src="img/page7/karte_neu.png" />
				 </div>
				 
				 <?php $url1 = "mailto:?subject=Gr&uuml;&szlig;e von meinem Training&body=Hallo!%0D%0A%0D%0AIch habe ein &Ouml;AMTC Fahrtechnik Training gemacht und schicke Dir meine Trainingsnachlese.%0D%0Ahttp://". $_SERVER['HTTP_HOST'] ."/?id=". $url ."%0D%0ADas Training war toll, es hat mir richtig Spa&szlig; gemacht!%0D%0AProbier auch Du es aus.%0D%0A%0D%0ALiebe Gr&uuml;&szlig;e"?>

		         <div class="form-wrapper fg-cond">
		         <div class="form-headline">Mit Freunden teilen</div>
		        <div class="fliesstext">Ihre Freunde könnte das auch interessieren? Einfach weiterempfehlen und gemeinsam wahre Fahrprofis werden.</div>
		        <a class="teilen-button" href="<?php echo $url1;?>">
		        	Empfehlen
		        </a>

<div class="agbs"><a href="http://www.oeamtc.at/impressum" target="_blank">Impressum</a></div>

<div class="agbs kontakt"><a href="http://www.oeamtc.at/portal/fahrtechnik+2500+++10304?id=2500%2C1610078%2C%2C" target="_blank">Info</a></div>

		         </div>
