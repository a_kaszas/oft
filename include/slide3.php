 <div class="page2top"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
			<div class="header-text3"> Trainings</div>
			<div class="header-subtext3"> der ÖAMTC Fahrtechnik. </div>
			
			<div class="pkw-holder">
				<div class="pfeil1"><img src="img/page2/pfeil-gelb.png" /></div>
				<div class="pfeil1-text">PKW Training </div>
				<div class="btn1"><img src="img/page2/plus.png" /></div>
			</div>
			
			<div class="moto-holder">
				<div class="pfeil2"><img src="img/page3/pfeil-grau.png" /></div>
				<div class="pfeil2-text">Motorrad Training </div>
				
			</div>
			
			
			<div class="training auto">
				<div class="training-menu">
					<div class="start active">Start</div><div class="special">Special</div><div class="community">Community</div>
				</div>
				<div class="training-view">
					<div class="trainings-container start active">
						<div class="training-holder active">
							<div class="title">Intensiv Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Technik & Sicherheit</li>
								<li>Slalom-Parcours</li>
								<li>Bremsen & Ausweichen</li>
								<li>Kurven&uuml;bung</li>
								<li>Schleudern & Stabilisieren</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 0.5 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Dynamik Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Technik & Dynamik</li>
								<li>Kurvendynamik</li>
								<li>Spontaner Spurwechsel</li>
								<li>Aquaplaning</li>
								<li>Handlingkurs</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> ab 2 Einheiten zu je 50 min</div>
						</div>
					</div>
					
					<div class="trainings-container special hidden">
						<div class="training-holder active">
							<div class="title">Speed Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Slalom-Parcours</li>
								<li>Kurventechnik</li>
								<li>Aquaplaning</li>
								<li>Notspurwechsel</li>
								<li>Kurvendynamik & Schleudern</li>
								
								<li>Handlingkurs</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Racing Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Rennstreckenverhalten</li>
								<li>Warm-up</li>
								<li>Linienwahl</li>
								<li>Anbremsen</li>
								<li>Kurventechnik</li>
								
								<li>Race Track</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Drift Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Drifttechniken Allrad-Hinterradantrieb</li>
								<li>Einleiten eines Drifts</li>
								<li>Endlos Drifts (Kreisbahn)</li>
								<li>Driften durch Kurvenkombinationen</li>
								<li>Stabilisieren des driftenden Fahrzeuges</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Schnee & Eis Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Sicherheit</li>
								<li>Warm-up</li>
								<li>Richtiges Bremsen</li>
								<li>Notspurwechsel</li>
								<li>Kurvendynamik</li>
								<li>Anfahren</li>
								<li>Handlingkurs</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Snow & Fun Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Eisarena</li>
								<li>Kreisbahn</li>
								<li>Ausweichman&ouml;ver</li>
								<li>Spurwechsel</li>
								<li>Handling-Parcours</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">SUV Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Onroad Theorie</li>
								<li>Slalom</li>
								<li>Ausweichen</li>
								<li>Kurventechnik</li>
								<li>Schleudern</li>
								<li>Offroad Theorie</li>
								
								<li>Bergauf &amp; Bergab</li>
								<li>Schrägfahrten &amp; Spurrillen</li>
								<li>Wasserdurchfahrten</li>


							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Offroad Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Theoretische Einf&uuml;hrung</li>
								<li>Korrekte Sitzposition</li>
								<li>Bergauf &amp;  Bergab</li>
								<li>Spurrillen  &amp;  Schr&auml;gfahrten</li>
								<li>Gef&uuml;hrte Gel&auml;ndefahrt</li>
								<li>Sicherheits-Check</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> ab 2 Einheiten zu je 50 min</div>
						</div>
					</div>
					<div class="trainings-container community hidden">
						<div class="training-holder active">
							<div class="title">Kart Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Rennmodus</li>
								<li>Outdoor-Kart-Strecken</li>
								<li> Modernste Karts</li>
								<li>Top-Technik</li>
								<li>Sicherheitsausstattung</li>
								<li>Zeitnehmung</li>
								<li>Auswertung</li>
								<li>Erstklassige Betreuung</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br />nach Vereinbarung</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="training moto">
				<div class="training-menu">
					<div class="start active">Start</div><div class="special">Special</div><div class="community">Community</div><div class="ausbildung">Ausbildung</div>
				</div>
				<div class="training-view">
					<div class="trainings-container start active">
						<div class="training-holder active">
							<div class="title">Aktiv Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Fahrphysik</li>
								<li>Lenkslalom</li>
								<li>Kreisbahn</li>
								<li>Spezialparcours</li>
								<li>Gefahrenanalyse</li>
								<li>Notbremsen</li>
								<lI>Ausweichen</lI>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Dynamik Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Trial</li>
								<li>Lenktechnik</li>
								<li>Notbremsen</li>
								<li>Kurvenbremsen & Ausweichen</li>
								<li>Handling/Linie</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> ab 2 Einheiten zu je 50 min</div>
						</div>
					</div>
					
					<div class="trainings-container special hidden">
                                                <div class="training-holder">
							<div class="title">Motorrad Warm up</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Gefahren erkennen und richtig reagieren</li>
								<li>Bewegliches Motorrad</li>
								<li>Kurventechnik</li>
								<li>Enge Kurvenkombinationen</li>
								<li>Jeder Bremsmeter z&auml;hlt</li>
								<li>Handlingparcours</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 0.5 Tage</div>
						</div>
						<div class="training-holder active">
							<div class="title">Speed Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Einzelkurven</li>
								<li>Fahrstil Hanging-off</li>
								<li>Kurvenkombinationen</li>
								<li>Sektionstraining</li>
								<li>Ideallinie</li>
								<li>High-Speed Bremsen</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Enduro Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Beweglichkeit mit leichten Gel&auml;nde-Trial-Motorr&auml;dern</li>
								<li>Kurvenstil Dr&uuml;cken</li>
								<li>Hangfahren</li>
								<li>Bremsen auf unbefestigtem Untergrund</li>
								<li>Endurostrecke</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Supermoto Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Lenkimpuls</li>
								<li>Kurvenstil Dr&uuml;cken</li>
								<li>Kurvenkombinationen</li>
								<li>Linie</li>
								<li>Kurvenanbremsen</li>
								<li>Handlingstrecke</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Trial Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Gleichgewicht & Balance</li>
								<li>Feinmotorik bei Gas, Kupplung und Bremse</li>
								<li>Hindernisparcours</li>
								<li>Fahren auf Hang und Steilhang</li>
								<li>Schwieriges Terrain</li>
								<li>Vorderradheben</li>

							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 0.5 Tage</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Training & Ausfahrt</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Einfahren</li>
								<li>Notbremsen</li>
								<li>Tourbriefing</li>
								<li>Ausfahrt</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> ab 2 Einheiten zu je 50 min</div>
						</div>
					</div>
					<div class="trainings-container community hidden">
						<div class="training-holder active">
							<div class="title">Kart Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
							<li>Rennmodus</li>
								<li>Outdoor-Kart-Strecken</li>
								<li> Modernste Karts</li>
								<li>Top-Technik</li>
								<li>Sicherheitsausstattung</li>
								<li>Zeitnehmung</li>
								<li>Auswertung</li>
								<li>Erstklassige Betreuung</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br />nach Vereinbarung</div>
						</div>
					</div>
					<div class="trainings-container ausbildung hidden">
						<div class="training-holder aufstiegspraxis active">
							<div class="title">Perfektionsfahrt <span class="fsg">FSG §4b.</span></div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Gef&uuml;hrte Ausfahrt</li>
								<li>Ausgew&auml;hlte Strecken</li>
								<li>Funkkontakt mit Instruktor</li>
								<li>Leihmotorräder verf&uuml;gbar</li>
								<li>Eintrag Führerscheinregister</li>
							</ul>
							<div class="description">Max. 4 Teilnehmer</div>
							<div class="dauer"><strong>Dauer</strong> <br />0,5 Tag</div>
						</div>
						<div class="training-holder aufstiegspraxis">
							<div class="title">Aufstiegspraxis <span class="fsg">FSG §18a.</span></div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Handling starker Motorr&auml;der</li>
								<li>Fahrtechnik f&uuml;r die Straße</li>
								<li>4 st&uuml;ndige, gef&uuml;hrte Ausfahrt</li>
								<li>ausgew&auml;hlte Strecken</li>
								<li>Risikokompetenz Training</li>
								<li>mehrmaliges Feedback</li>
								<li>inkl. Leihmotorrad</li>
							</ul><br/>
							<div class="description">Max. 2 Teilnehmer</div>
							<div class="dauer"><strong>Dauer</strong> <br />1 Tag</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="pkw"><img src="img/page3/CH_H12131.jpg" /></div>
			<div class="motorrad"><img src="img/page3/back-moto.jpg" /></div>
