<div class="auto-back"><img src="img/page5/back-kundenportal.jpg" /></div>

<div class="page2top"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
			<div class="header-text5">Nutzen Sie alle Vorteile</div>
			<div class="header-subtext5">Registrieren Sie sich in unserem neuen Kundenportal "Meine Fahrtechnik"</div>

<div class="registrieren_wrapper">
	<div class="registrieren_label">Mit Ihrer Registrierung können Sie nicht nur einen eigenen personalisierten Bereich gestalten.<br>Es erwarten Sie viele exklusive Vorteile:</div>
	<div class="registrieren_items first">Einladung zu besonderen Veranstaltungen</div>
	<div class="registrieren_items">Teilnahme an Gewinnspielen</div>
	<div class="registrieren_items">Exklusive Angebote</div>
	<div class="registrieren_items">Insider-Tipps zu Fahrtechnik Themen</div>
</div>

<div id="second" class="registrieren_btn"><a target="_blank" href="https://www.oeamtc.at/fahrtechnik/register">Jetzt registrieren</a></div>