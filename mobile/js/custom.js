var DIGIPAPERMOBILE = function() {

    function init() {
    	
    	    	
    	$(".click-here").on("click", function () {
	    	$(".cover").fadeOut();
    	})

        //eventhandler
        $(".singleSlide .toggle").on("click", function() {
            var $parent = $(this).parents(".singleSlide");
           
            $("video").get(0).pause();
           
            if(!$parent.hasClass("open")) {
                closeSlide($(".singleSlide.open"));
                openSlide($parent);

            } else {
                closeSlide($parent);
            }
        });
        
		//$("#slide7 .panel").show();
		        
        initTrainings ();
        initTrainingstype ();
        startSlideshow ();
        initZentren ();
        


        // if hash in URL then jump to slide
        var hash = window.location.hash;
        UTIL.elementExists($(hash)) && $(hash).find(".toggle").trigger("click");


        var a = 0;
        var b = 0;
        var c = 0;
        var j = 0;

        $("#play-start").on("click", function () {
            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.25 * (this.duration)) {
                    if (a < 1) {
                        console.log("watched 25%");
                        ga('send', 'event', 'Videos', 'Watched 25%', $(this).attr('title'));
                    }
                    a = a + 1;
                }
            });

            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.50 * (this.duration)) {
                    if (b < 1) {
                        console.log("watched 50%");
                        ga('send', 'event', 'Videos', 'Watched 50%', $(this).attr('title'));
                    }
                    b = b + 1;
                }
            });

            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.75 * (this.duration)) {
                    if (c < 1) {
                        console.log("watched 75%");
                        ga('send', 'event', 'Videos', 'Watched 75%', $(this).attr('title'));
                    }
                    c = c + 1;
                }
            });

            /* Video Finished, Thanks */
            $("#59de3905").bind("ended", function() {
                if (j < 1) {
                    console.log("Finished 100%");
                    ga('send', 'event', 'Videos', 'Finished 100%', $(this).attr('title'));
                }
                j = j + 1;
            });
            //ga('send', 'event', 'Video', 'Play', 'Start Video');
            $("#slide1 video").get(0).play();
            $(this).hide();
        })
        
        var vid_play = document.getElementById("59de3905");
        vid_play.onplay = function() {
            /* Video Watched */
            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.25 * (this.duration)) {
                    if (a < 1) {
                        console.log("watched 25%");
                        ga('send', 'event', 'Videos', 'Watched 25%', $(this).attr('title'));
                    }
                    a = a + 1;
                }
            });

            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.50 * (this.duration)) {
                    if (b < 1) {
                        console.log("watched 50%");
                        ga('send', 'event', 'Videos', 'Watched 50%', $(this).attr('title'));
                    }
                    b = b + 1;
                }
            });

            $("#59de3905").bind("timeupdate", function() {
                var currentTime = this.currentTime;
                if (currentTime > 0.75 * (this.duration)) {
                    if (c < 1) {
                        console.log("watched 75%");
                        ga('send', 'event', 'Videos', 'Watched 75%', $(this).attr('title'));
                    }
                    c = c + 1;
                }
            });

            /* Video Finished, Thanks */
            $("#59de3905").bind("ended", function() {
                if (j < 1) {
                    console.log("Finished 100%");
                    ga('send', 'event', 'Videos', 'Finished 100%', $(this).attr('title'));
                }
                j = j + 1;
            });
        };

    }

    function openSlide($slide) {
        $slide.addClass("open").find(".panel").slideDown(300, function() {
            adjustViewport($slide);
        })
    }

    function closeSlide($slide) {
        $slide.removeClass("open").find(".panel").slideUp(100);
    }

    function adjustViewport($slide) {
        $('html, body').animate({scrollTop: $slide.offset().top}, 'slow');
    }

    //public
    return {
        init: init
    }

}();

var UTIL = {
    elementExists: function($element){
        return $element.length;
    }
};

function initTrainingstype () {
	$(".frage-2, .frage-3").hide();
				
    $(".fragen div").on("touchstart click",function () {

	if(!($(this).hasClass("frage"))) {
		
	
	$(this).parent().find(".active").each(function () {
		$(this).removeClass("active");
	});
	
	$(this).toggleClass("active");
	
	}
	
	})
	
	     //start quiz js
       
      frage = 1;
      
      $(".weiter").on("click", function () {
      	  
      	  if($(this).parent().find(".active").length == 1){
	      	  	fragen = $(this).parent().parent().attr("id");
      	  
	      if(frage == 1) {
	      		
	      	  $(".frage-1").hide();
		      $(".frage-2").show();
		      frage = 2;
		      result1 = $(".frage-1").find(".active").attr("data");
		      $(".step1").removeClass("active");
		      $(".step2").addClass("active");
		      
	      }else if(frage == 2) {
	      	  $(".frage-2").hide();
		      $(".frage-3").show();
	      	  result2 = $(".frage-2").find(".active").attr("data");
		      frage = 3;
		      $(".step2").removeClass("active");
		      $(".step3").addClass("active");
		      
	      }else {
	      	  $(".frage-3").hide();
		      result3 = $(".frage-3").find(".active").attr("data");
		      frage = 1;
		      $(".steps").hide();
		      setTimeout(function() {
				 resultsShow ();	 
			  }, 500);
					
	      }

      	  }
      })
      
      //open buchungs overlay & abfrage welche bestell möglichkeiten es gibt
			
			$(".training-suggestions div").on("click", function () {
				trainingId = $(this).text();
				$(".buchung, .headline-keine-zeit, .gutschein-back,.gutschein-bestellen").show();
				$(".buchung").show();
				
				$(".final-training").text(trainingId);
				
				if(trainingId == "Personal Coaching") {
					$(".open-form").hide();
					$(".headline-keine-zeit").attr("src","img/headline-jetzt.png");
				}else if(trainingId == "Racing Experience") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen, .gutschein-bestellen").hide();
				}else if(trainingId == "Supermoto Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Enduro Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Trial Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Training und Ausfahrt") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
					
					
				}else{
					$(".headline-keine-zeit").attr("src","img/headline-keine-zeit.png");
					
				}

				
			})

			// Close  Gutschein
			$(".closer").on("click", function () {
				$('.buchung').hide();
				$(".open-form, .headline-keine-zeit").removeAttr("style");
				console.log('ss');
			})
			
			
			 trainingTaken = $("#mainWrapper").attr("class")
        //console.log(trainingTaken);
        
        $("#"+trainingTaken).hide();
        
        // open buchungs formular with correct selection
        $(".open-form").on("click",function () {
	        ga('send', 'event', 'Formular', 'Button', trainingId);
		switch (trainingId) {
			case "Drift Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=310');
			break;
			case "Schnee & Eis Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=128');
			break;
			case "Racing Experience" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=581');
			break;
			case "Personal Coaching" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=582');
			break;
			case "Dynamik Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=37');
					}else{
						window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=26');
					}
			break;
			case "Speed Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=38');
					}else{
						window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=578');
					}
			break;
			case "Supermoto Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=579');
			break;
			case "Trial Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=129');
			break;
			case "Training und Ausfahrt" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=88');
			break;
			case "Enduro Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=580');
			break;
			case "Aktiv Training" :
				if($(".final-training").attr("id") == "pkw"){
					window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=1029');
				}else{
					window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=28');
				}
			break;
			case "Snow & Fun Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=289');
			break;
			case "Drive & Fly" :
				window.open('https://www.oeamtc.at/fahrtechnik/pkw/drive-fly/11.005.330');
			break;
			/*
			case "Drift Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=310&preOrt=21');
			break;
			case "Schnee & Eis Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=128&preOrt=21');
			break;
			case "Racing Experience" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=581&preOrt=21');
			break;
			case "Personal Coaching" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=582&preOrt=21');
			break;
			case "Dynamik Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=37&preOrt=21');
					}else{
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=26&preOrt=21');
					}
			break;
			case "Speed Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=38&preOrt=21');
					}else{
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=38&preOrt=21');
					}
			break;
			case "Supermoto Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=579&preOrt=21');
			break;
			case "Trial Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=129&preOrt=21');
			break;
			case "Training und Ausfahrt" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=88&preOrt=21');
			break;
			case "Enduro Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=580&preOrt=21');
			break;
			case "Aktiv Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=28&preOrt=21');
			break;
			case "Speed Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=38&preOrt=21');
			break;
			case "Snow & Fun Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=289&preOrt=21');
			break;*/
			
		}	
	})
	
	     // open gutschein iframe seite with correct selection
        $(".gutschein-bestellen").on("click",function () {
	        ga('send', 'event', 'Gutschein', 'Button', trainingId);
		switch (trainingId) {
			case "Drift Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/drift');
			break;
			case "Schnee & Eis Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/schnee');
			break;
			case "Snow & Fun Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/schnee');
			break;
			case "Racing Experience" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Personal Coaching" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/coach');
			break;
			case "Dynamik Training" :
					window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/dynamik');
			break;
			case "Speed Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/speed');
			break;
			case "Supermoto Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Trial Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Training und Ausfahrt" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgg');
			break;
			case "Enduro Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Aktiv Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/aktiv');
			break;
			
		}	
	})
	
	
	
	//steps clicks 
	
	$(".steps div").on("click" , function () {
						
						thisstep = $(this).attr("class");
						//console.log(frage);
						switch(thisstep) {
							case "step1":
								   $(".frage-1").show();
								if(frage == 2 ){																      
								      $(".frage-2").hide();	
								      $(this).toggleClass("active");		
								      $(".step2").removeClass("active");						     
								}else if(frage == 3){																	      
								      $(".frage-3").hide();								      
								      $(this).toggleClass("active");	
								      $(".step3").removeClass("active");
								     
								}
								frage = 1;	
								
							break;
							case "step2" :
								if(frage == 3) {
									 $(".frage-2").show();
								    
								     $(".frage-3").hide();
								    $(this).toggleClass("active");		
								    $(".step3").removeClass("active");	
								    frage = 2;					     

								}
							break;
							
						}
						
						
						
				
			})




}

function initTrainings () {
	training = "off";
	
	$(".pkw-holder").on("click", function () {
		$(".pkw-holder").animate({opacity:"1"});
		$(".moto-holder").animate({opacity:"0.2"});
		$(".pkw-holder").animate({marginTop:"-50px"});
		$(".moto-holder").animate({marginTop:"-50px"});
		
		if(training == ".moto") {
			$(".training.auto").animate({marginLeft:"-10px"})
			$(".training.moto").animate({marginLeft:"-400px"})
			training = ".auto";
		}else{
			$(".training.auto").animate({marginLeft:"-10px"})
			training = ".auto";
		}
		
	})
	
	
	
	$(".moto-holder").on("click", function () {
		$(".moto-holder").animate({opacity:"1"});
		$(".pkw-holder").animate({opacity:"0.2"});
		$(".pkw-holder").animate({marginTop:"-50px"});
		$(".moto-holder").animate({marginTop:"-50px"});
			
		if(training == ".auto") {
			$(".training.moto").animate({marginLeft:"-10px"})
			$(".training.auto").animate({marginLeft:"400px"})
			training = ".moto";
		}else{
			$(".training.moto").animate({marginLeft:"-10px"})
			
			training = ".moto";
		}
		
	})
	
	// TRAININGS CAROUSEL
    $('.owl-carousel-training').owlCarousel({
		nav:false,
		dots: true,
		center: true,
		responsiveClass:true,
		items:1,
		loop:false,
	});
        
        $(".training-menu div").on("click", function () {
	        
        	$(training+" .training-holder.active").each(function () {
				$(this).removeClass("active");
			})
        
        	category = $(this).text().toLowerCase();
        	
        	if(category == "community") {
	        	$(training+" .training-scroll").addClass("hidden");
        	}else{
	        	$(training+" .training-scroll").removeClass("hidden");
        	}
        	
        	$(training+" .trainings-container").removeAttr('style').removeClass("active").addClass("hidden");
        	$(training+" .trainings-container."+category).addClass("active").removeClass("hidden");
        				
	        $(this).parent().find(".active").each(function () {
				$(this).removeClass("active");
			});
			
			$(this).toggleClass("active");
			
			checkSlider ();
			
		})
	
}

function checkSlider () {
	getActive = $(training+' .training-holder.active').index();
	total = $(training+' .trainings-container.active .training-holder').length;
	
	active = getActive + 1;
	
	if(getActive == 0) {
		$(training+" .prev-training").addClass("hidden");
	}else{
		$(training+" .prev-training").removeClass("hidden");
	}
	
	if(active == total) {
		$(training+" .next-training").addClass("hidden");
	}else {
		$(training+ " .next-training").removeClass("hidden");
	}
	
}

function resultsShow () {
	total = new Array();
	 	total = [];
	 	total[0] = result1;
	 	total[1] = result2;
	 	total[2] = result3;
	 	
	 	blau = 0;
	 	lila = 0;
	 	gruen = 0;
	 	
	 	for(var i = 0; i < total.length; ++i){
		 	if(total[i] == "blau") { 
		 		blau ++;
		 	} else if (total[i] == "gruen") {
			 	gruen ++;
		 	} else if (total[i] == "lila") {
			 	lila ++; 
		 	}
		 }		 
		 if (blau > 1 ) {
		 	 $("#antwort-a").fadeIn();
		 	 

		 	 } else if (gruen > 1) {
		 	
		 	 $("#antwort-b").fadeIn();
			
			 
			 
		 } else if (lila > 1) {
		 	 
		 	 $("#antwort-c").fadeIn();
		 	 
		 	 		 	 
		 } else {
		 	 
		 	$("#antwort-b").fadeIn();
		 	
		 }
}

function startSlideshow () {
	
	$(".slideshow-holder").load("/mobile/php/slide-6.php", function () {
		//setTimeout(function() {
		$('.slider').glide({
        	autoplay: "8000",
			arrows: false,
			navigation: true,
		});
		
		$("#owl-example").owlCarousel({
			nav:true,
			items: 1,
			navText: [
	      "<img src='../img/global/pfeil_links.svg'>",
	      "<img src='../img/global/pfeil_rechts.svg'>"
	      ],
		});
			
			
			$(".download").on("click", function () {
				currentImg = $(this).parent().parent().find(".shareurl").attr("src");
				//console.log(currentImg);
				//window.open(currentImg);
				$(this).find('a').attr('href',currentImg);
			})
			
			$("#slide6 .email").on("click", function () {
				currentImg = $(this).parent().parent().find(".shareurl").attr("src");
				//console.log(currentImg);
				//window.open("mailto:?body="+currentImg);
				window.location='mailto:?subject='+$('#slide6 .headline').html()+'&body='+encodeURIComponent(currentImg); return false;
			})
			
			baseurl = $(".cover").attr("id");
    	
			$(".facebook").on("click", function (){
		 	shareimg = $(this).parent().parent().find(".shareurl").attr("src");
		 	console.log($(this).parent().parent().find(".shareurl"));
		 	if(shareimg.indexOf('mp4') == -1) {
			 	imglink = baseurl+shareimg;
		 	} else {
			 	imglink = shareimg;
		 	}
			facebook_link = 'https://www.facebook.com/sharer/sharer.php?u='+imglink+'&t='+$('#slide6 .headline').html();
		    window.open(facebook_link,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=650,height=600');
		 	
		 	/*FB.ui({
			 	method: 'feed',
			 	href : baseurl,
			 	picture: imglink,
			 	
			}, function(response){});*/
	 	})
	 	
	 	
			
		//}, 15000);
		
		 //open Overlay image
        $(".slider-img").on("click", function() {
	
	       $('.'+$(this).attr('data-img')).show();
	      
			console.log($(this).attr('data-img'));
        });
        
        // close Overlay image
		$(".closeOverlaymobile").on("click", function() {
			$(this).parent('.slider-img-big').hide();
        });
	})
	
}

function initZentren() {
	$(".zentren-select li").on("click", function () {
		zentrum = $(this).attr("id");
		
		$("." + zentrum).removeClass("hidden");
		//console.log(zentrum);	
	})
	
	$(".zentrum .closer").on("click", function () {
		$(this).parent().addClass("hidden");
	})
	
}

