<?php
session_start();
include '../include/data.php';
//include '../include/slide5_helper.php';
?>
<!DOCTYPE html>

<html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>ÖAMTC Fahrtechnik</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />

    <link rel="stylesheet" href="css/owl.carousel.css"/>
    <link rel="stylesheet" href="css/owl.theme.css"/>
    <link rel="stylesheet" href="css/custom.css"/>
    <script src="../js/vendor/jquery-2.1.0.min.js"></script>
    <script src="js/jquery.glide.min.js"></script>
    <script src="js/form.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>

    <script>
        $(document).ready(function() {
            DIGIPAPERMOBILE.init();
        });
    </script>

</head>
<body>
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55682482-1', 'auto');
  ga('send', 'pageview');
</script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '301308040057333',
      xfbml      : true,
      version    : 'v2.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<div id="mainWrapper">
	
	<div id="<?php echo "http://" . $_SERVER['SERVER_NAME'];?>" class="cover">
		
		<div class="header">
        	<img src="img/header.png" class="header"/>
		</div>
		
		<?php if($anrede != "") { ?>
		<div class="headline"><?php print $anrede . " " . $vorname . " " . $nachname;?>,</div>
		<?php }?>
		
		<div class="subheadline">Fahrprofis werden nicht geboren, sondern gemacht.</div>
		
		<?php 
				if(in_array($trainincode ,array("MPX","MIT","M2T","MMF","MOX", "SMT", "MTT", "MWU", "MWU", "MPF"))) { ?>
					<div class="page1-background moto"><img src="img/cover-moto.jpg" /></div>
				<?php }else if ($trainincode == "OF1") {?>
					<div class="page1-background"><img src="img/cover-suv.jpg" /></div>
				<?php }else{?>
					<div class="page1-background"><img src="img/cover-pkw.jpg" /></div>
				<?php }
				
				
			?>		
			
			
			<div class="click-here">
				
				Hier klicken.
				
				
			</div>
		
	</div>

    <div class="header">
        <img src="img/header.png" class="header"/>
    </div>
    <div class="slidesWrapper">

        <div id="slide2" class="singleSlide">
            <div class="toggle">Das war Ihr Fahrtraining</div>
            <div class="panel"><div class="headline">Das war Ihr Training.</div>
	            <div class="video-wrapper reset">		
	<?php 
	/*switch($trainincode) {
			case "PIT" : ?>
				<video id="59de3905" controls="controls" preload="auto" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/01pkwaktivtrainingcrm.ipad.mp4" />
  <source src="/videos/01pkwaktivtrainingcrm.ipad.webmsd.webm" />
</video>

			<?php break;
			case "P2T": ?>
			
				<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/02pkwdynamiktrainingcrm.ipad.mp4" />
  <source src="/videos/02pkwdynamiktrainingcrm.ipad.webmsd.webm" />
</video>		
<?php break;
			case "PIN": ?>
			
				<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Intensiv Training" data-uid="59de3905">
  <source src="/videos/PKW_IntensivTraining_CRM_converted.mp4" />
  <source src="/videos/PKW_IntensivTraining_CRM_converted.webm" />
</video>	
<?php break;
			case "P3T": ?>
			
				<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Speed Training" data-uid="59de3905">
  <source src="/videos/PKW_Speed_Training_CRM_converted.mp4" />
  <source src="/videos/PKW_Speed_Training_CRM_converted.webm" />
</video>	
		
				
			<?php break;
			case "DR1": ?>
			
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/03pkwdrifttrainingcrm.ipad.mp4" />
  <source src="/videos/03pkwdrifttrainingcrm.ipad.webmsd.webm" />
</video>
			<?php break;
			case "PMF": ?>
			
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/04pkwmehrphasecrm.ipad.mp4" />
  <source src="/videos/04pkwmehrphasecrm.ipad.webmsd.webm" />
</video>
			<?php break;
			case "OF1": ?>
			<video id="59de3905" controls="controls" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/05pkwoffroadtrainingcrm.ipad.mp4" />
  <source src="/videos/05pkwoffroadtrainingcrm.ipad.webmsd.webm" />
</video>
			

			<?php break;
			case "PRE": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/06pkwracingexperiencecrm.ipad.mp4" />
  <source src="/videos/06pkwracingexperiencecrm.ipad.webmsd.webm" />
</video>


			
			<?php break;
			case "SET": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />
</video>

<?php break;
			case "SFN": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />
</video>

<?php break;
			case "SF1": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />
</video>

<?php break;
			case "SF2": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />
</video>


				
			<?php break;
			case "SUV": ?>
			<video  preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/08pkwsuvtrainingcrm.ipad.mp4" />
  <source src="/videos/08pkwsuvtrainingcrm.ipad.webmsd.webm" />
</video>



				
			<?php break;
			case "MPX": ?>
			
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/09mot125ertrainingcrm.ipad.mp4" />
  <source src="/videos/09mot125ertrainingcrm.ipad.webmsd.webm" />
</video>



				
			<?php break;
			case "MIT": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/10motaktivtrainingcrm.ipad.mp4" />
  <source src="/videos/10motaktivtrainingcrm.ipad.webmsd.webm" />
</video>

				
			<?php break;
			case "M2T": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/11motdynamiktrainingcrm.ipad.mp4" />
  <source src="/videos/11motdynamiktrainingcrm.ipad.webmsd.webm" />
</video>



				
			<?php break;
			case "MMF": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/12motmehrphasecrm.ipad.mp4" />
  <source src="/videos/12motmehrphasecrm.ipad.webmsd.webm" />
</video>
			<?php break;
			case "MOX": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/13motmopedtrainingcrm.ipad.mp4" />
  <source src="/videos/13motmopedtrainingcrm.ipad.webmsd.webm" />
</video>

				
			<?php break;
			case "SMT": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905" >
  <source src="/videos/14motspeedtrainingcrm.ipad.mp4" />
  <source src="/videos/14motspeedtrainingcrm.ipad.webmsd.webm" />
</video>



				
			<?php break;
			case "MTT": ?>
			
			<video preload="auto"  controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905" >
  <source src="/videos/15mottrialtrainingcrm.ipad.mp4" />
  <source src="/videos/15mottrialtrainingcrm.ipad.webmsd.webm" />
</video>

				
			<?php break;
			case "MWU": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905" >
  <source src="/videos/16motwarmupcrm.ipad.mp4" />
  <source src="/videos/16motwarmupcrm.ipad.webmsd.webm" />
</video>

<?php break;
			case "MAX": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Aufstiegspraxis Motorrad" data-uid="59de3905" >
  <source src="/videos/DIGPAPER_AUFSTIEGSPRAXIS_V7.mp4" />
  <source src="/videos/DIGPAPER_AUFSTIEGSPRAXIS_V7.webm" />
</video>

<?php break;
			case "MPF": ?>
			<video preload="auto" controls="controls" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Perfektionsfahrt Motorrad" data-uid="59de3905" >
  <source src="/videos/DIGIPAPER_Perfektionsfahrt_V2_h264.mp4" />
  <source src="/videos/DIGIPAPER_Perfektionsfahrt_V2_h264.webm" />
</video>

			<?php break; 
			default : ?>
<video id="59de3905" preload="auto" controls="controls" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
	<source src="/videos/DIGIPAPER_PKW_AKTIV_TRAINING.mp4" />
	<source src="/videos/DIGIPAPER_PKW_AKTIV_TRAINING.webm" />
</video>


			<?php break;
		}*/	

		switch($trainincode) {
            case "PIT" : ?>
                <video id="59de3905" controls="true" preload="auto" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
                    <!--<source src="/videos/01pkwaktivtrainingcrm.ipad.mp4" />
                    <source src="/videos/01pkwaktivtrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_AKTIV_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_AKTIV_TRAINING.webm" />
                </video>
            <?php break;
        
            case "P2T": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Dynamik Training" data-uid="59de3905">
                    <!--<source src="/videos/02pkwdynamiktrainingcrm.ipad.mp4" />
                    <source src="/videos/02pkwdynamiktrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_DYNAMIK_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_DYNAMIK_TRAINING.webm" />
                </video>	
            <?php break;
        
            case "PIN": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Intensiv Training" data-uid="59de3905">
                    <!--<source src="/videos/PKW_IntensivTraining_CRM_converted.mp4" />
                    <source src="/videos/PKW_IntensivTraining_CRM_converted.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_INTENSIV_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_INTENSIV_TRAINING.webm" />
                </video>
            <?php break;
        
            case "P3T": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Speed Training" data-uid="59de3905">
                    <!--<source src="/videos/PKW_Speed_Training_CRM_converted.mp4" />
                    <source src="/videos/PKW_Speed_Training_CRM_converted.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_SPEED_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_SPEED_TRAINING.webm" />
                </video>					
            <?php break;
        
            case "DR1": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Drift Training" data-uid="59de3905">
                    <!--<source src="/videos/03pkwdrifttrainingcrm.ipad.mp4" />
                    <source src="/videos/03pkwdrifttrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_DRIFT_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_DRIFT_TRAINING.webm" />
                </video>
            <?php break;
        
            case "PMF": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Mehrphasen Training" data-uid="59de3905">
                    <!--<source src="/videos/04pkwmehrphasecrm.ipad.mp4" />
                    <source src="/videos/04pkwmehrphasecrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_MEHRPHASEN_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_MEHRPHASEN_TRAINING.webm" />
                </video>
            <?php break;
        
            case "OF1": ?>
                <video id="59de3905" controls="true" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW offroad Training" data-uid="59de3905">
                    <source src="/videos/05pkwoffroadtrainingcrm.ipad.mp4" />
                    <source src="/videos/05pkwoffroadtrainingcrm.ipad.webmsd.webm" />
                </video>
            <?php break;
        
            case "PRE": ?>
                <video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Racing experience" data-uid="59de3905">
                    <!--<source src="/videos/06pkwracingexperiencecrm.ipad.mp4" />
                    <source src="/videos/06pkwracingexperiencecrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_Racing_Experience.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_Racing_Experience.webm" />
                </video>
            <?php break;
        
            case "SET": ?>
                <video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Schneeundeis Training" data-uid="59de3905">
                    <!--<source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
                    <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_Schnee_und_Eis_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_Schnee_und_Eis_TRAINING.webm" />
                </video>
            <?php break;
        
            case "SFN": ?>
                <video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Schnee und eis Training" data-uid="59de3905">
                    <!--<source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
                    <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_Schnee_und_Eis_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_Schnee_und_Eis_TRAINING.webm" />
                </video>
            <?php break;

            case "SF1": ?>
                <video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
                    <!--<source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
                    <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_Schnee_und_Eis_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_Schnee_und_Eis_TRAINING.webm" />
                </video>
            <?php break;

            case "SF2": ?>
                    <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
                        <!--<source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
                        <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />-->
                        <source src="/videos/DIGIPAPER_PKW_Schnee_und_Eis_TRAINING.mp4" />
                        <source src="/videos/DIGIPAPER_PKW_Schnee_und_Eis_TRAINING.webm" />
                    </video>
            <?php break;
        
            case "SUV": ?>
                <video  preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW SUV Training" data-uid="59de3905">
                    <!--<source src="/videos/08pkwsuvtrainingcrm.ipad.mp4" />
                    <source src="/videos/08pkwsuvtrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_SUV.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_SUV.webm" />
                </video>
            <?php break;
        
            case "MPX": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="125 Training" data-uid="59de3905">
                    <!--<source src="/videos/09mot125ertrainingcrm.ipad.mp4" />
                    <source src="/videos/09mot125ertrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/digipaper_mot_125_B111.mp4" />
                    <source src="/videos/digipaper_mot_125_B111.webm" />
                </video>
            <?php break;
        
            case "MIT": ?>
                <video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Motorrad Aktiv Training" data-uid="59de3905">
                    <!--<source src="/videos/10motaktivtrainingcrm.ipad.mp4" />
                    <source src="/videos/10motaktivtrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/digipaper_mot_Aktiv_Training.mp4" />
                    <source src="/videos/digipaper_mot_Aktiv_Training.webm" />
                </video>
            <?php break;
        
            case "M2T": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Moto Dynamik Training" data-uid="59de3905">
                    <!--<source src="/videos/11motdynamiktrainingcrm.ipad.mp4" />
                    <source src="/videos/11motdynamiktrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/digipaper_mot_Dynamik_Training.mp4" />
                    <source src="/videos/digipaper_mot_Dynamik_Training.webm" />
                </video>  
            <?php break;
        
            case "MMF": ?>
                    <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="125 mehrphasen Training" data-uid="59de3905">
                        <!--<source src="/videos/12motmehrphasecrm.ipad.mp4" />
                        <source src="/videos/12motmehrphasecrm.ipad.webmsd.webm" />-->
                        <source src="/videos/digipaper_mot_Fahrsicherheits_Training.mp4" />
                        <source src="/videos/digipaper_mot_Fahrsicherheits_Training.webm" />
                    </video>
            <?php break;
        
            case "MOX": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Moped Training" data-uid="59de3905">
                    <!--<source src="/videos/13motmopedtrainingcrm.ipad.mp4" />
                    <source src="/videos/13motmopedtrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/digipaper_mot_50ccm.mp4" />
                    <source src="/videos/digipaper_mot_50ccm.webm" />
                </video>
            <?php break;
        
            case "SMT": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Moto Speed Training" data-uid="59de3905" >
                    <!--<source src="/videos/14motspeedtrainingcrm.ipad.mp4" />
                    <source src="/videos/14motspeedtrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/digipaper_mot_Speed_Training.mp4" />
                    <source src="/videos/digipaper_mot_Speed_Training.webm" />
                </video>
            <?php break;
        
            case "MTT": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Moto trial Training" data-uid="59de3905" >
                    <!--<source src="/videos/15mottrialtrainingcrm.ipad.mp4" />
                    <source src="/videos/15mottrialtrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/digipaper_mot_Trial_Training.mp4" />
                    <source src="/videos/digipaper_mot_Trial_Training.webm" />
                </video>
            <?php break;
        
            case "MWU": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Moto warm up Training" data-uid="59de3905" >
                    <!--<source src="/videos/16motwarmupcrm.ipad.mp4" />
                    <source src="/videos/16motwarmupcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/digipaper_mot_Warm_Up_Training.mp4" />
                    <source src="/videos/digipaper_mot_Warm_Up_Training.webm" />
                </video>
            <?php break;
        
            case "MAX": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Aufstiegspraxis Motorrad" data-uid="59de3905" >
                    <!--<source src="/videos/DIGIPAPER_Aufstiegspraxis_V3-2.mp4" />
                    <source src="/videos/DIGIPAPER_Aufstiegspraxis_V3-2.webm" />-->
                    <source src="/videos/digipaper_mot_Aufstiegspraxis.mp4" />
                    <source src="/videos/digipaper_mot_Aufstiegspraxis.webm" />
                </video>
            <?php break;
        
            case "MPF": ?>
                <video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="Perfektionsfahrt Motorrad" data-uid="59de3905" >
                    <!--<source src="/videos/DIGIPAPER_Perfektionsfahrt_V3.mp4" />
                    <source src="/videos/DIGIPAPER_Perfektionsfahrt_V3.webm" />-->
                    <source src="/videos/digipaper_mot_Perfektionsfahrt.mp4" />
                    <source src="/videos/digipaper_mot_Perfektionsfahrt.webm" />
                </video>
            <?php break;
        
            default : ?>
                <video id="59de3905" controls="true" preload="auto"  class="sublime" poster="/videos/startbild.jpg" width="100%" height="auto" title="PKW Aktiv Training" data-uid="59de3905">
                    <!--<source src="/videos/01pkwaktivtrainingcrm.ipad.mp4" />
                    <source src="/videos/01pkwaktivtrainingcrm.ipad.webmsd.webm" />-->
                    <source src="/videos/DIGIPAPER_PKW_AKTIV_TRAINING.mp4" />
                    <source src="/videos/DIGIPAPER_PKW_AKTIV_TRAINING.webm" />
                </video>
            <?php break;
        }

		?>
	

</div>

</div>
        </div>

        <div id="slide3" class="singleSlide">
            <div class="toggle">Trainings der ÖAMTC Fahrtechnik</div>
            <div class="panel">
            <div class="training auto" style="opacity: 1;">
				<div class="training-menu" style="z-index: 2;">
					<div class="start active">Start</div><div class="special">Special</div><div class="community">Community</div>
				</div>
				<div class="training-view">
					<div class="trainings-container start active owl-carousel-training">
						<div class="training-holder active">
							<div class="title">Intensiv Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Technik &amp; Sicherheit</li>
								<li>Slalom-Parcours</li>
								<li>Bremsen &amp; Ausweichen</li>
								<li>Kurvenübung</li>
								<li>Schleudern &amp; Stabilisieren</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 0.5 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Dynamik Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Technik &amp; Dynamik</li>
								<li>Kurvendynamik</li>
								<li>Spontaner Spurwechsel</li>
								<li>Aquaplaning</li>
								<li>Handlingkurs</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> ab 2 Einheiten zu je 50 min</div>
						</div>
					</div>
					
					<div class="trainings-container special hidden owl-carousel-training">
						<div class="training-holder active">
							<div class="title">Speed Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Slalom-Parcours</li>
								<li>Kurventechnik</li>
								<li>Aquaplaning</li>
								<li>Notspurwechsel</li>
								<li>Kurvendynamik &amp; Schleudern</li>
								
								<li>Handlingkurs</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Racing Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Rennstreckenverhalten</li>
								<li>Warm-up</li>
								<li>Linienwahl</li>
								<li>Anbremsen</li>
								<li>Kurventechnik</li>
								
								<li>Race Track</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Drift Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Drifttechniken Allrad-Hinterradantrieb</li>
								<li>Einleiten eines Drifts</li>
								<li>Endlos Drifts (Kreisbahn)</li>
								<li>Driften durch Kurvenkombinationen</li>
								<li>Stabilisieren des driftenden Fahrzeuges</li>
								
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Schnee &amp; Eis Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Sicherheit</li>
								<li>Warm-up</li>
								<li>Richtiges Bremsen</li>
								<li>Notspurwechsel</li>
								<li>Kurvendynamik</li>
								<li>Anfahren</li>
								<li>Handlingkurs</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Snow &amp; Fun Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Eisarena</li>
								<li>Kreisbahn</li>
								<li>Ausweichmanöver</li>
								<li>Spurwechsel</li>
								<li>Handling-Parcours</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">SUV Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Onroad Theorie</li>
								<li>Slalom</li>
								<li>Ausweichen</li>
								<li>Kurventechnik</li>
								<li>Schleudern</li>
								<li>Offroad Theorie</li>
								<li>Bergauf & Bergab</li>
								<li>Schrägfahrten & Spurrillen</li>
								<li>Wasserdurchfahrten</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Offroad Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Theoretische Einführung</li>
								<li>Korrekte Sitzposition</li>
								<li>Bergauf &amp; Bergab</li>
								<li>Spurrillen &amp; Schrägfahrten</li>
								<li>Geführte Geländefahrt</li>
								<li>Sicherheits-Check</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br> ab 2 Einheiten zu je 50 min</div>
						</div>
					</div>
					<div class="trainings-container community hidden">
						<div class="training-holder active">
							<div class="title">Kart Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Rennmodus</li>
								<li>Outdoor-Kart-Strecken</li>
								<li>Modernste Karts</li>
								<li>Top-Technik</li>
								<li>Sicherheitsausstattung</li>
								<li>Zeitnehmung</li>
								<li>Auswertung</li>
								<li>Erstklassige Betreuung</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br>nach Vereinbarung</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="training moto">
				<div class="training-menu">
					<div class="start active">Start</div><div class="special">Special</div><div class="community">Community</div><div class="ausbildung">Ausbildung</div>
				</div>
				<div class="training-view">
					<div class="trainings-container start active owl-carousel-training">
						<div class="training-holder active">
							<div class="title">Aktiv Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Fahrphysik</li>
								<li>Lenkslalom</li>
								<li>Kreisbahn</li>
								<li>Spezialparcours</li>
								<li>Gefahrenanalyse</li>
								<li>Notbremsen</li>
								<lI>Ausweichen</lI>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Dynamik Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Trial</li>
								<li>Lenktechnik</li>
								<li>Notbremsen</li>
								<li>Kurvenbremsen & Ausweichen</li>
								<li>Handling/Linie</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> ab 2 Einheiten zu je 50 min</div>
						</div>
					</div>
					
					<div class="trainings-container special hidden owl-carousel-training">
						<div class="training-holder active">
							<div class="title">Speed Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Einzelkurven</li>
								<li>Fahrstil Hanging-off</li>
								<li>Kurvenkombinationen</li>
								<li>Sektionstraining</li>
								<li>Ideallinie</li>
								<li>High-Speed Bremsen</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Enduro Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Beweglichkeit mit leichten Gel&auml;nde-Trial-Motorr&auml;dern</li>
								<li>Kurvenstil Dr&uuml;cken</li>
								<li>Hangfahren</li>
								<li>Bremsen auf unbefestigtem Untergrund</li>
								<li>Endurostrecke</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Supermoto Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Lenkimpuls</li>
								<li>Kurvenstil Dr&uuml;cken</li>
								<li>Kurvenkombinationen</li>
								<li>Linie</li>
								<li>Kurvenanbremsen</li>
								<li>Handlingstrecke</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Trial Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Gleichgewicht & Balance</li>
								<li>Feinmotorik bei Gas, Kupplung und Bremse</li>
								<li>Hindernisparcours</li>
								<li>Fahren auf Hang und Steilhang</li>
								<li>Schwieriges Terrain</li>
								<li>Vorderradheben</li>

							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 0.5 Tage</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Training & Ausfahrt</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Einfahren</li>
								<li>Notbremsen</li>
								<li>Tourbriefing</li>
								<li>Ausfahrt</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Motorrad Warm up</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Gefahren erkennen und richtig reagieren</li>
								<li>Bewegliches Motorrad</li>
								<li>Kurventechnik</li>
								<li>Enge Kurvenkombinationen</li>
								<li>Jeder Bremsmeter z&auml;hlt</li>
								<li>Handlingparcours</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> 0.5 Tage</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br /> ab 2 Einheiten zu je 50 min</div>
						</div>
					</div>
					<div class="trainings-container community hidden">
						<div class="training-holder active">
							<div class="title">Kart Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
							<li>Rennmodus</li>
								<li>Outdoor-Kart-Strecken</li>
								<li> Modernste Karts</li>
								<li>Top-Technik</li>
								<li>Sicherheitsausstattung</li>
								<li>Zeitnehmung</li>
								<li>Auswertung</li>
								<li>Erstklassige Betreuung</li>
							</ul>
							<div class="dauer"><strong>Dauer</strong> <br />nach Vereinbarung</div>
						</div>
					</div>
					<div class="trainings-container ausbildung hidden owl-carousel-training">
						<div class="training-holder active">
							<div class="title">Perfektionsfahrt <span class="fsg">FSG §4b.</span></div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
							<li>Gef&uuml;hrte Ausfahrt</li>
								<li>Ausgew&auml;hlte Strecken</li>
								<li>Funkkontakt mit Instruktor</li>
								<li>Leihmotorr&auml;der verf&uuml;gbar</li>
								<li>Eintrag F&uuml;hrerscheinregister</li>
							</ul><br><br>
							Max. 4 Teilnehmer
							<div class="dauer"><strong>Dauer</strong> <br />0,5 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Aufstiegspraxis <span class="fsg">FSG §18a.</span></div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Handling starker Motorr&auml;der</li>
								<li>Fahrtechnik f&uuml;r die Straße</li>
								<li>4 st&uuml;ndige, gef&uuml;hrte Ausfahrt</li>
								<li>ausgew&auml;hlte Strecken</li>
								<li>Risikokompetenz Training</li>
								<li>mehrmaliges Feedback</li>
								<li>inkl. Leihmotorrad</li>
							</ul><br><br>
							Max. 2 Teilnehmer
							<div class="dauer"><strong>Dauer</strong> <br />1 Tag</div>
						</div>
					</div>
				</div>
			</div>


	            <div class="header-text3"> Trainings</div>
	            <div class="header-subtext3"> der ÖAMTC Fahrtechnik. </div>
			<div class="moto-holder">
				<div class="pfeil2"><img src="img/btn-moto-training.png"></div>
			</div>
			<div class="pkw-holder">
				<div class="pfeil2"><img src="img/btn-pkw-training.png"></div>
			</div>
			
						
			
            </div>
        </div>
		
		<div id="slide4" class="singleSlide">
            <div class="toggle">Kundenportal "Meine Fahrtechnik"</div>
            <div class="panel">
	            <div class="headline">Nutzen Sie alle Vorteile</div>
	            <div class="subheadline">Registrieren Sie sich in unserem neuen Kundenportal "Meine Fahrtechnik"</div>

				<div class="registrieren_label">Mit Ihrer Registrierung können Sie nicht nur einen eigenen personalisierten Bereich gestalten.<br>Es erwarten Sie viele exklusive Vorteile:</div>
				<div class="registrieren_wrapper">
					<div class="registrieren_items first">Einladung zu besonderen Veranstaltungen</div>
					<div class="registrieren_items">Teilnahme an Gewinnspielen</div>
					<div class="registrieren_items">Exklusive Angebote</div>
					<div class="registrieren_items">Insider-Tipps zu Fahrtechnik Themen</div>
				</div>
				
				<div id="second" class="registrieren_btn"><a target="_blank" href="https://www.oeamtc.at/fahrtechnik/register">Jetzt registrieren</a></div>
            </div>
        </div>
		
		<div id="slide5" class="singleSlide">
            <div class="toggle">Welcher Trainingstyp sind Sie?</div>
            <div class="panel">
	            <div class="headline">Welcher Trainingstyp sind Sie? </div>
	            <div class="subheadline">3 Klicks zum Ergebnis.</div>
	            <?php 
				if(in_array($trainincode ,array("MPX","MIT","M2T","MMF","MOX", "SMT", "MTT", "MWU", "MAX", "MPF"))) {
					include "php/slide5-moto.php";
					$type = "moto";
				}else if ($trainincode == "OF1") {
					include "php/slide5-suv.php";
					$type = "offroad";
				}else{
					include "php/slide5-auto.php";
					$type = "pkw";
				}
			?>		
	            <div class="steps fg-cond">
					<div class="step1 active"></div>
					<div class="step2"></div>
					<div class="step3"></div>
				</div>
            </div>
        </div>
		
        <div id="slide6" class="singleSlide">
	        
	        <?php $special_link = '388991'; $videoimg_link = "224455"; ?>
	        
            <div class="toggle">
				<?php 
						$getLength = file_get_contents('http://oeamtc1.it-wms.com/archive/years/group_ajax.php?callback=meincallback&group='.$bildercode);

						$getLength_new = file_get_contents('http://oeamtc.it-wms.com/ftzapi.php?memberId='.$bildercode, true);
						$decodedmedia = json_decode($getLength_new, true);

						if ($bildercode == $special_link) { ?>
					Ihr persönliches Trainingsfoto
					<?php } else { ?>
					Ihre Fahrtechnik Fotogalerie
					<?php } ?>
            </div>
            <div class="panel">
				<?php if ($bildercode == $special_link) { ?>
				<div class="headline">Sie sind unser Held!</div>
				<?php } else if($decodedmedia["STATUS"] == 'OK'){ ?>
				<div class="headline">Unsere Fahrtechnik Trainings in Medien</div>
				<?php } else if($getLength != "meincallback(empty)"){ ?>
				<div class="headline">Ihr Fahrtechnik Training in Bildern</div>
				<?php } else { ?>
				<div class="headline">Unsere Fahrtechnik Trainings in Bildern</div>
				<?php } ?>
				
            	<div class="slideshow-holder"></div>
            	
            	<div class="images_label_mobile">
	            	<?php if ($bildercode == $special_link) { ?>
					<span><?php print $anrede . " " . $vorname . " " . $nachname;?><br/>
					Wir gratulieren Ihnen zum absolvierten Training! Sie haben es geschafft! Die Schleuderplatte haben Sie gemeistert wie ein Profi. Wenn Sie all Ihre Fotos sehen möchten, registrieren Sie sich ganz einfach auf unserem Kundenportal „Meine Fahrtechnik“. In Ihrem personalisierten Bereich stehen Ihnen alle Fotos zur Verfügung. Außerdem erwarten Sie noch viele andere Vorteile.</span>
					<?php } else if($decodedmedia["STATUS"] == 'OK'){ ?>
					<span>Registrieren Sie sich jetzt auf unserem Kundenportal „Meine Fahrtechnik“. Dort finden Sie alle Fotos und Videos, die wir bei Ihrem Traning gemacht haben.</span>
					<?php } else if($getLength != "meincallback(empty)"){ ?>
					<span>Registrieren Sie sich jetzt auf unserem Kundenportal „Meine Fahrtechnik“. Dort finden Sie alle Fotos, die wir bei Ihrem Traning gemacht haben.</span>
					<?php } else { ?>
					<span>Registrieren Sie sich jetzt auf unserem Kundenportal „Meine Fahrtechnik“.<br/>Dort erwarten Sie viele Vorteile und zus&auml;tzliche Informationen.</span>
					<?php } ?>
            	</div>
            	
				<div id="second" class="individual_btn registrieren_btn"><a href="https://www.oeamtc.at/fahrtechnik/register" target="_blank">Jetzt registrieren</a></div>
            </div>
        </div>

        <div id="slide7" class="singleSlide">
            <div class="toggle">ÖAMTC Fahrtechnik Zentren</div>
            <div class="panel">
	            <div class="headline">In diesen ÖAMTC Fahrtechnik Zentren</div>
	            <div class="subheadline">werden Fahrprofis gemacht.</div>
	            <div class="clear"></div>
	            
	            <div class="zentren-info">
	            	 <div class="zentrum z1 hidden">
	            	 <div class="closer"></div>
					 <div class="headline">Zentrum Teesdorf</div>
					  
					 <div class="text">
						Kontakt:<br />
						Triester Bundesstraße 120<br />
						2524 Teesdorf<br />
						Telefon: 02253 817 00-32100<br />
						<a href="mailto:fahrtechnik@oeamtc.at">fahrtechnik@oeamtc.at</a>
					 </div>
					 
				 </div>
				 
				  <div class="zentrum z2 hidden">
				  	<div class="closer"></div>
					 <div class="headline">Zentrum Melk/Wachauring</div>
					 
					 <div class="text">
						Kontakt:<br />
						Am Wachauring 2<br />
						3390 Melk<br />
						Telefon: 02752 528 55<br />
						<a href="mailto:fahrtechnik.wachauring@oeamtc.at">fahrtechnik.wachauring<br />@oeamtc.at</a>
					 </div>
					
				 </div>
				 
				 <div class="zentrum z3 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Offroad Zentrum Stotzing</div>
					
					 <div class="text">
						Kontakt:<br />
						2451 Au am Leithaberge<br />
						Telefon: 02253 817 00-32100<br />
						<a href="mailto:fahrtechnik@oeamtc.at">fahrtechnik@oeamtc.at</a>
					 </div>
					
				 </div>
				 
				 <div class="zentrum z4 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Marchtrenk</div>
					 
					 <div class="text">
						Kontakt:<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon:07243 515 20<br />
						<a href="mailto:fahrtechnik.ooe@oeamtc.at">fahrtechnik.ooe@oeamtc.at</a>
					 </div>
					
				 </div>
				 
				 <div class="zentrum z5 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Saalfelden/Brandlhof</div>
					
					 <div class="text">
						Kontakt:<br />
						Hohlwegen 4<br />
						1950 Saalfelden<br />
						Telefon: 06582 752 60<br />
						<a href="mailto:fahrtechnik.saalfelden@oeamtc.at">fahrtechnik.saalfelden<br />@oeamtc.at</a>
					 </div>
					 				 </div>
				 
				 <div class="zentrum z6 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Innsbruck</div>
					
					 <div class="text">
						Kontakt:<br />
						Handlhofweg 81<br />
						6020 Innsbruck<br />
						Telefon: 0512 379 502<br />
						<a href="mailto:fahrtechnik.tirol@oeamtc.at">fahrtechnik.tirol@oeamtc.at</a>
						
					 </div>
					
				 </div>
				 
				 <div class="zentrum z7 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Lang/Lebring</div>
					
					 <div class="text">
						Kontakt:<br />
						Jöß, Gewerbegebiet 1<br />
						8403 Lang<br />
						Telefon: 03182 401 65<br />
						<a href="mailto:fahrtechnik.lebring@oeamtc.at">fahrtechnik.lebring@oeamtc.at</a>
					 </div>
					 
				 </div>
				 
				 <div class="zentrum z8 hidden">
				 	<div class="closer"></div>
					  <div class="headline">Zentrum Kalwang</div>
					 <div class="text">
						Kontakt:<br />
						Kalwang 71<br />
						8775 Kalwang<br />
						Telefon: 03846 200 90<br />
						<a href="mailto:fahrtechnik.halwang@oeamtc.at">fahrtechnik.halwang@oeamtc.at</a>
					 </div>
					 				 </div>
				 
				 <div class="zentrum z9 hidden">
				 	<div class="closer"></div>
					  <div class="headline">Zentrum St. Veit a. d. Glan</div>
					
					 <div class="text">
						Kontakt:<br />
						Mölbling / Mail<br />
						Mail 11<br />
						Telefon: 04212 331 70<br />
						<a href="mailto:fahrtechnik.kaernten@oeamtc.at">fahrtechnik.kaernten@oeamtc.at</a>
					 </div>
					
				 </div>
				
	            
	            </div>
	            
	            <div class="zentren-select">
					<ul>
						<li class="li-headline">ÖAMTC Fahrtechnik Zentren</li>
						<li id="z1">    1 Zentrum Teesdorf (NÖ)							   </li>
						<li id="z2">    2 Zentrum Melk/Wachauring (NÖ)					   </li>
						<li id="z3">    3 Offroad Zentrum Stotzing (B)					   </li>
						<li id="z4">    4 Zentrum Marchtrenk (OÖ)						   </li>
						<li id="z5">    5 Zentrum Saalfelden/Brandlhof (S)				   </li>
						<li id="z6">    6 Zentrum Innsbruck (T)							   </li>
						<li id="z7">    7 Zentrum Lang/Lebring (STMK)					   </li>
						<li id="z8">    8 Zentrum Kalwang (STMK)						   </li>
						<li id="z9">    9 Zentrum St. Veit a. d. Glan (K)				   </li>
					</ul>	            
	            </div>
	            
	            
	            
	            
            </div>
        </div>
        <div id="slide8" class="singleSlide">
            <div class="toggle">Mit Freunden teilen</div>
            <div class="panel">
	            
	            <?php include "php/slide-8.php";?>
	            
            </div>
        </div>

    </div>
 </div>

</body>
</html>