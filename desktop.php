<?php
session_start();

include 'include/data.php';
//include 'include/slide6_helper.php';
header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>ÖAMTC Fahrtechnik</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    
    
    <link rel="stylesheet" href="css/global.css"/>
    <link rel="stylesheet" href="css/slide1.css"/>
    <link rel="stylesheet" href="css/slide2.css"/>
    <link rel="stylesheet" href="css/slide3.css"/>
    <link rel="stylesheet" href="css/slide4.css"/>
    <link rel="stylesheet" href="css/slide5.css"/>
    <link rel="stylesheet" href="css/slide6.css"/>
    <link rel="stylesheet" href="css/slide7.css"/>
    <!-- <link rel="stylesheet" href="css/vendor/jquery.kyco.preloader.css"/> -->
	
    <script src="js/vendor/jquery-2.1.0.min.js"></script> 
    <script src="js/vendor/jquery.touchSwipe.min.js"></script>
    <script src="js/vendor/jquery.carouFredSel-6.2.1.js"></script>
    <script src="js/vendor/jquery.kyco.preloader.js"></script>
    <script src="js/vendor/TweenMax.min.js"></script>
    <script src="js/vendor/modernizr.custom.89522.js"></script>
    <script src="js/vendor/jQuery.print.js"></script>
    <script src="js/vendor/dragdealer.js"></script>
    <script src="js/vendor/owl.carousel.min.js"></script>
    <!--<script type="text/javascript" src="//cdn.sublimevideo.net/js/eztprl7t.js"></script> -->
    <script src="js/form.js"></script>
    <script src="js/global.js"></script>
    <script src="js/slide1.js"></script>
    <script src="js/slide2.js"></script>
    <script src="js/slide3.js"></script>
    <script src="js/slide4.js"></script>
    <script src="js/slide5.js"></script> 
    <script src="js/slide6.js"></script>
    <script src="js/slide7.js"></script>
    <script> 
		
        $(document).ready(function() {
			//start();
			//$('body').kycoPreload();
           DIGIPAPER.init();
			
        });
    </script>
</head>
<body>
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55682482-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '301308040057333',
      xfbml      : true,
      version    : 'v2.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/de_DE/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<div id="mainWrapper" class="<?php echo $trainincode;?>">
    <div id="<?php echo "http://" . $_SERVER['SERVER_NAME'];?>" class="slidesWrapper">
        <div id="slide1" class="singleSlide">
            <div class="single-holder">
                <?php include "include/slide1.php" ?>
            </div>				
        </div> <!--ENDE Slide1 -->
        <div id="slide2" class="singleSlide">
            <div class="single-holder">
                 <?php include "include/slide2.php";?>
            </div>
        </div>  <!-- ENDE Slide2  -->
        <div id="slide3" class="singleSlide">
            <div class="single-holder fg-cond">
                <?php include "include/slide3.php";?> 
            </div>
        </div>  <!-- ENDE Slide3  -->
        <div id="slide4" class="singleSlide">
            <div class="single-holder fg-cond">
                <!--  <img class="simageHolder" src="img/vorlage/page4.jpg" />  -->
                <div class="page2top"><img src="img/page2/hoch-gelb-back2.png" /></div>
                <div class="header-text4 fg-cond">Welcher Trainingstyp sind Sie?</div>
                <div class="header-subtext4 fg-cond">3 Klicks zum Ergebnis.</div>
                <div class="steps fg-cond">
                    <div class="step1 active">1</div>
                    <div class="step2">2</div>
                    <div class="step3">3</div>
                </div>
                <?php 
                    if(in_array($trainincode ,array("MPX","MIT","M2T","MMF","MOX", "SMT", "MTT", "MWU", "MAX","MPF"))) {
                            include "include/slide4-moto.php";
                            $type = "moto";
                    }else if ($trainincode == "OF1") {
                            include "include/slide4-suv.php";
                            $type = "offroad";
                    }else{
                            include "include/slide4-auto.php";
                            $type = "pkw";
                    }
                ?>			
            </div>
        </div>  <!-- ENDE Slide4  -->
        <div id="slide5" class="singleSlide">
            <div class="single-holder">
                <?php include "include/slide5.php";?>			
            </div>
        </div>  <!-- ENDE Slide5  -->
        <div id="slide6" class="singleSlide">      
            <div class="single-holder itc-book">
                <?php include "include/slide6.php";?>      
            </div>
        </div>  <!-- ENDE Slide6  -->
        <div id="slide7" class="singleSlide">      
            <div class="single-holder itc-book">
                <?php include "include/slide7.php";?>      
            </div>
        </div>  <!-- ENDE Slide7  -->        
    </div>
    <div class="prevNavBtn"><img src="img/global/pfeil_links.svg" /></div>
    <div class="nextNavBtn"><img src="img/global/pfeil_rechts.svg" /></div>
    <ul id="menuNav">
    	<li class="openThumbs">
            <img src="img/musterseite/icon_home.png" />
            <div class="hover">Home</div>
    	</li>
        <li>
            <a class="facebookgoogle" href="https://www.facebook.com/fahrtechnik" target="_blank"><img src="img/musterseite/icon_facebook.png" /></a>
            <div class="hover">Facebook</div>	
        </li>
        <li>
        <?php $url1 = "mailto:?subject=Gr&uuml;&szlig;e von meinem Training&body=Hallo!%0D%0A%0D%0AIch habe ein &Ouml;AMTC Fahrtechnik Training gemacht und schicke Dir meine Trainingsnachlese.%0D%0Ahttp://". $_SERVER['HTTP_HOST'] ."/?id=". $url ."%0D%0ADas Training war toll, es hat mir richtig Spa&szlig; gemacht!%0D%0AProbier auch Du es aus.%0D%0A%0D%0ALiebe Gr&uuml;&szlig;e"?>
            <a href="<?php echo $url1;?>" class="caroufredsel"><img src="img/musterseite/icon_add.png" /></a>
            <div class="hover">Mit Freunden teilen</div>
        </li>
    </ul>
</div>
<div id="navigationOverlay">
    <ul>
        <li><a href="#slide1" class="caroufredsel"><img src="<?php echo $coverThumb;?>" /></a></li>
        <li><a href="#slide2" class="caroufredsel"><img src="<?php echo $videoThumb;?>" /></a></li>
        <li><a href="#slide3" class="caroufredsel"><img src="img/global/_0003_3.png" /></a></li>				
        <li><a href="#slide4" class="caroufredsel"><img src="<?php echo $selektorThumb;?>" /></a></li>				
        <li><a href="#slide5" class="caroufredsel"><img src="img/global/05.png" /></a></li>				
        <li><a href="#slide6" class="caroufredsel"><img src="img/global/06.png" /></a></li>  
        <li><a href="#slide7" class="caroufredsel"><img src="img/global/_0000_6.jpg" /></a></li>  
    </ul>
</div>

</body>
</html>
