SLIDES.slide4 = function() {

    //private
    function onInit(){
    	
			
        TweenMax.killAll();
        $("#slide4 .reset").removeAttr('style').hide();
        
        setQuestionpos();
        
        trainingTaken = $("#mainWrapper").attr("class")
        //console.log(trainingTaken);
        
        $("#"+trainingTaken).hide();
        
        // open buchungs formular with correct selection
        $(".open-form").on("click",function () {
		switch (trainingId) {
			case "Drift Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=PKW&styp=310');
			break;
			case "Schnee & Eis Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=PKW&styp=289');
			break;
			case "Racing Experience" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=PKW&styp=581');
			break;
			case "Personal Coaching" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm');
			break;
			case "Dynamik Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=PKW&styp=37');
					}else{
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=KRAD&styp=26');
					}
			break;
			case "Speed Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=PKW&styp=38');
					}else{
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=KRAD&styp=578');
					}
			break;
			case "Supermoto Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=KRAD&styp=579');
			break;
			case "Trial Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=KRAD&styp=129');
			break;
			case "Training und Ausfahrt" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=KRAD&styp=88');
			break;
			case "Enduro Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=KRAD&styp=580');
			break;
			case "Aktiv Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&sbasic=PKW&styp=2');
			break;
			
		}	
	})
	
	     // open gutschein iframe seite with correct selection
        $(".gutschein-bestellen").on("click",function () {
		switch (trainingId) {
			case "Drift Training" :
				window.open('http://oeft.digitalschmiede.com/gutschein/drift.html');
			break;
			case "Schnee & Eis Training" :
				window.open('http://oeft.digitalschmiede.com/gutschein/schneeundeis.html');
			break;
			case "Racing Experience" :
				window.open('http://oeft.digitalschmiede.com/gutschein/wertgutschein.html');
			break;
			case "Personal Coaching" :
				window.open('http://oeft.digitalschmiede.com/gutschein/personal-coaching.html');
			break;
			case "Dynamik Training" :
					window.open('http://oeft.digitalschmiede.com/gutschein/dynamik.html');
			break;
			case "Speed Training" :
				window.open('http://oeft.digitalschmiede.com/gutschein/speed.html');
			break;
			case "Supermoto Training" :
				window.open('http://oeft.digitalschmiede.com/gutschein/wertgutschein.html');
			break;
			case "Trial Training" :
				window.open('http://oeft.digitalschmiede.com/gutschein/wertgutschein.html');
			break;
			case "Training und Ausfahrt" :
				window.open('http://oeft.digitalschmiede.com/gutschein/wertgutschein.html');
			break;
			case "Enduro Training" :
				window.open('http://oeft.digitalschmiede.com/gutschein/wertgutschein.html');
			break;
			case "Aktiv Training" :
				window.open('http://oeft.digitalschmiede.com/gutschein/aktiv.html');
			break;
			
		}	
	})

        
       //start quiz js
       
      frage = 1;
      
      $(".weiter").on("click", function () {
      	  
      	  if($(this).parent().find(".active").length == 1){
	      	  	fragen = $(this).parent().parent().attr("id");
      	  
	      if(frage == 1) {
		      TweenMax.to($("#"+fragen+" .frage-1 .frage"), 0.2, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-1 .first"), 0.3, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-1 .second"), 0.4, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-1 .third"), 0.5, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-1 .weiter"), 0.6, {left: "-1000" ,delay:0});
		      
		      TweenMax.to($("#"+fragen+" .frage-2 .frage"), 0.2, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .frage").show()}});
		      TweenMax.to($("#"+fragen+" .frage-2 .first"), 0.3, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .first").show()}});
		      TweenMax.to($("#"+fragen+" .frage-2 .second"), 0.4, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .second").show()}});
		      TweenMax.to($("#"+fragen+" .frage-2 .third"), 0.5, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .third").show()}});
		      TweenMax.to($("#"+fragen+" .frage-2 .weiter"), 0.6, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .weiter").show()}});
		      frage = 2;
		      result1 = $(".frage-1").find(".active").attr("data");
		      $(".step1").removeClass("active");
		      $(".step2").addClass("active");
		      
	      }else if(frage == 2) {
	      	  TweenMax.to($("#"+fragen+" .frage-2 .frage"), 0.2, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-2 .first"), 0.3, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-2 .second"), 0.4, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-2 .third"), 0.5, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-2 .weiter"), 0.6, {left: "-1000" ,delay:0});
		      
		      TweenMax.to($("#"+fragen+" .frage-3 .frage"), 0.2, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .frage").show()}});
		      TweenMax.to($("#"+fragen+" .frage-3 .first"), 0.3, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .first").show()}});
		      TweenMax.to($("#"+fragen+" .frage-3 .second"), 0.4, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .second").show()}});
		      TweenMax.to($("#"+fragen+" .frage-3 .third"), 0.5, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .third").show()}});
		      TweenMax.to($("#"+fragen+" .frage-3 .weiter"), 0.6, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .weiter").show()}});
		      result2 = $(".frage-2").find(".active").attr("data");
		      frage = 3;
		      $(".step2").removeClass("active");
		      $(".step3").addClass("active");
		      
	      }else {
		      TweenMax.to($("#"+fragen+" .frage-3 .frage"), 0.2, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-3 .first"), 0.3, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-3 .second"), 0.4, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-3 .third"), 0.5, {left: "-1000" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-3 .weiter"), 0.6, {left: "-1000" ,delay:0});
		      result3 = $(".frage-3").find(".active").attr("data");
		      frage = 1;
		      
		      setTimeout(function() {
		      	$(".steps").hide();
				 resultsShow ();	 
			  }, 500);
					
	      }

      	  }
      })
       
        $(".fragen div").on("touchstart click",function () {
        
			if(!($(this).hasClass("frage"))) {
				
			
			$(this).parent().find(".active").each(function () {
				$(this).removeClass("active");
			});
			
			$(this).toggleClass("active");
			
			}
			
			})
			
			
			
			$(".steps div").on("click" , function () {
						
						thisstep = $(this).attr("class");
						//console.log(frage);
						switch(thisstep) {
							case "step1":
								  TweenMax.to($("#"+fragen+" .frage-1 .frage"), 0.2, {left: "0" ,delay:0.5});
							      TweenMax.to($("#"+fragen+" .frage-1 .first"), 0.3, {left: "0" ,delay:0.5});
							      TweenMax.to($("#"+fragen+" .frage-1 .second"), 0.4, {left: "0" ,delay:0.5});
							      TweenMax.to($("#"+fragen+" .frage-1 .third"), 0.5, {left: "0" ,delay:0.5});
							      TweenMax.to($("#"+fragen+" .frage-1 .weiter"), 0.6, {left: "0" ,delay:0.5});
								if(frage == 2 ){																      
								      TweenMax.to($("#"+fragen+" .frage-2 .frage"), 0.2, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-2 .first"), 0.3, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-2 .second"), 0.4, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-2 .third"), 0.5, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-2 .weiter"), 0.6, {left: "1000" ,delay:0,ease:Circ.easeIn});	
								      $(this).toggleClass("active");		
								      $(".step2").removeClass("active");						     
								}else if(frage == 3){																	      
								      TweenMax.to($("#"+fragen+" .frage-3 .frage"), 0.2, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-3 .first"), 0.3, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-3 .second"), 0.4, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-3 .third"), 0.5, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-3 .weiter"), 0.6, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      $(this).toggleClass("active");	
								      $(".step3").removeClass("active");
								      $(".frage-2 .reset").each(function() {
									      $(this).css({left:"1000px"});
								      })
								}
								frage = 1;	
								
							break;
							case "step2" :
								if(frage == 3) {
									TweenMax.to($("#"+fragen+" .frage-2 .frage"), 0.2, {left: "0" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-2 .first"), 0.3, {left: "0" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-2 .second"), 0.4, {left: "0" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-2 .third"), 0.5, {left: "0" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-2 .weiter"), 0.6, {left: "0" ,delay:0,ease:Circ.easeIn});	
								    
								    TweenMax.to($("#"+fragen+" .frage-3 .frage"), 0.2, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-3 .first"), 0.3, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-3 .second"), 0.4, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-3 .third"), 0.5, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-3 .weiter"), 0.6, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    $(this).toggleClass("active");		
								    $(".step3").removeClass("active");	
								    frage = 2;					     

								}
							break;
							
						}
						
						
						
				
			})
			
			//open buchungs overlay & abfrage welche bestell möglichkeiten es gibt
			
			$(".training-suggestions div").on("click", function () {
				trainingId = $(this).text();
				
				$(".buchung").show();
				
				$(".final-training").text(trainingId);
				
				if(trainingId == "Personal Coaching") {
					$(".open-form, .headline-keine-zeit").hide();
				}
				
			})
			
			$(".closer").on("click", function () {
				$('.buchung').hide();
				$(".open-form, .headline-keine-zeit").removeAttr("style");
			})
       
    }

    function onEnter() {
        
    }

    function onExit() {
        TweenMax.killAll();
        $("#slide4 .reset").removeAttr('style');
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();


function resultsShow () {
	total = new Array();
	 	total = [];
	 	total[0] = result1;
	 	total[1] = result2;
	 	total[2] = result3;
	 	
	 	blau = 0;
	 	lila = 0;
	 	gruen = 0;
	 	
	 	for(var i = 0; i < total.length; ++i){
		 	if(total[i] == "blau") { 
		 		blau ++;
		 	} else if (total[i] == "gruen") {
			 	gruen ++;
		 	} else if (total[i] == "lila") {
			 	lila ++; 
		 	}
		 }


         TweenMax.to($(".filme-text"), 0.5, {top:490, delay:3});
         
         TweenMax.to($(".again"), 0.5, {top:530, delay:6, ease:Bounce.easeInOut});
		 
		 
		 if (blau > 1 ) {
		 	 $("#antwort-a").fadeIn();
		 	 

		 	 } else if (gruen > 1) {
		 	
		 	 $("#antwort-b").fadeIn();
			
			 
			 
		 } else if (lila > 1) {
		 	 
		 	 $("#antwort-c").fadeIn();
		 	 
		 	 		 	 
		 } else {
		 	 
		 	$("#antwort-b").fadeIn();
		 	
		 }
}


function setQuestionpos() {
	$(".frage-2 .reset").each(function () {
				$(this).css({left:"1000px"})
		})
			
		$(".frage-3 .reset").each(function () {
			$(this).css({left:"1000px"})
		})

}

