var SLIDES = {};
var DIGIPAPER = function() {


    function init() {
        initSlides();
        //start first slide
        var startSlideID = SLIDES[$("#mainWrapper").find(".singleSlide:first-child").attr("id")];
        (startSlideID.onEnter) && startSlideID.onEnter();
        initCarousel();
        initNavigation();
        $('img').each(function(){ $(this).attr('src', $(this).attr('lsrc')); });
    }

    function initSlides() {
        $('#mainWrapper').find(".singleSlide").each(function(){
            var slideID = SLIDES[$(this).attr("id")];
            (slideID.onInit) && slideID.onInit();
        })
    }

    function initCarousel() {
        $('#mainWrapper').find(".slidesWrapper").carouFredSel({
            auto        : false,
            responsive  : false,
            circular    : false,
            infinite    : false,
            items : { visible: 1, start: true },
            prev: '.prevNavBtn',
            next: '.nextNavBtn',
            swipe: {onTouch: true },
            scroll: {
                onAfter:function(data) {
                    var oldSlideID = SLIDES[data.items.old.attr("id")];
                    var newSlideID = SLIDES[data.items.visible.attr("id")];
                    urlChange();
                    (oldSlideID.onExit) && oldSlideID.onExit();
                    (newSlideID.onEnter) && newSlideID.onEnter();
                }
            }
        });
    }

    function initNavigation() {
        var $menuNav = $("#menuNav");
        var $navigationOverlay = $("#navigationOverlay");

        $menuNav.find(".openThumbs").on("click", function(){
            showThumbNav();
        });
        function showThumbNav() {
            $navigationOverlay.css("visibility", "visible").stop().animate({ opacity: 1 }).one("click", function () {
                hiddeMainNav();
                //console.log(event.target)
            })
        }
        function hiddeMainNav() {
            $navigationOverlay.stop().animate({ opacity: 0 }, function () {
                $(this).css("visibility", "hidden");
            })
        }
    }

    return {
        init: init
    }

}();


function urlChange() {
	url = location.href;
	url = url.split("#");
	url = url[1];
	first = $(".singleSlide:first").attr("id");
	if(url != first) {
		location.href = "#" + first;
	}
}



