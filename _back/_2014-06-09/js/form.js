$(function() {
	//console.log("in");

		$(".teilnahme-loader").on("touchstart click",function () {
			$(".hide-overflow").show();
			$("#gewinnspiel-form .x2").addClass("teil")
			$(".teilnahme-loader, #form").hide();

			
		});	
	
	//send a friend formular
	
	$("#send").on("click",function() {
		
		$("#send").hide();
  	
		// validate and process form
		// first hide any error messages
    $('.error').hide();
	
	var anrede1 = $( '.form-box.first input[name=sex]:checked' ).val();	
	
	var titel1 = $("input#titel").val();
	
	var vorname1 = $("input#vorname").val();
	if (vorname1 == "") {
      $(".first .vorname").css("color","red");
      $("#send").show();
      return false;
    }
	
	var nachname1 = $("input#nachname").val();
	if (nachname1 == "") {
      $(".first .nachname").css("color","red");
      $("#send").show();
      return false;
    }

	var re = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
	
	var email1 = $("input#email").val();
	if (email1 == "" || !(re.test(email1))) {
      $(".first .email").css("color","red");
      $("#send").show();
      return false;
    }
	
	var anrede2 = $( '.form-box.second input[name=sex2]:checked' ).val();	
		    
    var titel2= $("input#titel2").val();
    
    var vorname2 = $("input#vorname2").val();
    if (vorname2 == "") {
      $(".second .vorname").css("color","red");
      $("#send").show();
      return false;
    }
	
	var nachname2 = $("input#nachname2").val();
	if (nachname2 == "") {
      $(".second .nachname").css("color","red");
      $("#send").show();
      return false;
    }

    
    var email2 = $("input#email2").val();
    if (email2 == "" || !(re.test(email2))) {
      $(".second .email").css("color","red");
      $("#send").show();
      return false;
    }
    
    if($('#agb').prop("checked") == false){
	    $(".agbs").css("color","red");
		$("#send").show();
		return false;
    }
	
	var text = $("textarea").val();
   
		
		var dataString = 'anrede1=' + anrede1  + '&titel1=' + titel1 + '&vorname1=' + vorname1 + '&nachname1=' + nachname1 + "&email1=" + email1 + '&anrede2=' + anrede2 + '&titel2=' + titel2 + '&vorname2=' + vorname2 + '&nachname2=' + nachname2 + '&email2=' + email2 + '&text=' + text;
	
		//alert(dataString);
		
	$.ajax({
			type: "POST",
			url: "include/sendtofriend.php",
			data: dataString, 
			success: function() {
      	
      	
        $('#form, .daten, .form-headline, .hide-overflow').fadeOut('fast'); //code der ausgeführt wird wenn das formular abgeschickt wurde
		$('#danke').fadeIn('slow');
		$(".box").animate({height:"150px",marginTop:"200px"});
		
        
      }
     });
        //alert (dataString);return false;	
		
    

		
		
				 return false;
		

   
	});

});
