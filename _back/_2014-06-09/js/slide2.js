SLIDES.slide2 = function() {

    //private
    function onInit(){
    
    	 $("#slide2 .reset").removeAttr('style');
    
    	header = $(".header-subtext2");
    	menuBox = $(".index-menu");
    	videoWrapper = $(".video-wrapper");
    	topB = $(".page2top");
        
        
        $(".page2-background").on("click", function () {
	      	pos = $(".index-menu").css("marginLeft");
	      	//console.log(pos); 
	      	
	      	if(pos == "741px") {
		      	 TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
	      	}else{
		      	TweenMax.to($(".index-menu"), 1, {marginLeft: "741" ,delay:0, ease:Elastic.easeIn});
	      	}
        })
        
        $("#play").on("click", function () {
	        videoId = $(this).attr("class");
	        //console.log(videoId);
	        $("#"+videoId).get(0).play();
	        $(this).hide();
	        TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
	        TweenMax.to($(".header-subtext2"), 1, {marginLeft: "-380" ,delay:0, ease:Elastic.easeOut});
	        
        })
        
        
    }

    function onEnter() {
        //TweenMax.from(header, 2, {left: "-400" ,delay:1, ease:Bounce.Elastic,onStart: function() {header.show()}});
        //TweenMax.from(menuBox, 1, {marginLeft: "1000" ,delay:2, ease:Bounce.Elastic,onStart: function() {menuBox.show()}});
        //TweenMax.from(videoWrapper, 3, {opacity: "0" ,delay:1, ease:Bounce.Elastic,onStart: function() {videoWrapper.show()}});
        //TweenMax.from(topB, 1, {top: "-292" ,delay:1, ease:Bounce.Elastic,onStart: function() {topB.show()}});
    }

    function onExit() {
    	 $('#play').show();
    	 //console.log(videoId);
    	 $("video").get(0).pause(); 
       TweenMax.killAll();
        $("#slide2 .reset").removeAttr('style');
    }

	

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();

function videoend () {
		 $('#play').show();
	     TweenMax.to($(".index-menu"), 1, {marginLeft: "741" ,delay:0, ease:Elastic.easeIn});
	     TweenMax.to($(".header-subtext2"), 1, {marginLeft: "0" ,delay:0, ease:Elastic.easeIn});
	}