SLIDES.slide6 = function() {

    //private
    function onInit(){
        $(".flag").mouseenter(function () {
	        zentrum = $(this).attr("id");
	        $("."+zentrum).removeClass("hidden").addClass("active");
        })
        
        $(".zentrum").mouseleave(function () {
        	//console.log("in");
	        $(".zentrum.active").removeClass("active").addClass("hidden");
        })
        
        $("#menuNav").hide();
        
        
        
    }

    function onEnter() {
         $("#menuNav").hide();
          $(".prevNavBtn").animate({left:"256px"});
    }

    function onExit() {
         $("#menuNav").show();
          $(".prevNavBtn").animate({left:"0px"});
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();