SLIDES.slide5 = function() {

    //private
    function onInit(){
        fotoid = $(".foto-holder").attr("id");
        
        $('.printImg').click(function(){
			$(this).parents().find(".large_img").print();
	 	});
	 	
	 	$(".emailImg").on("click",function () {
		 	shareimg = $(this).parents().find(".large_img img").attr("src");
		 	//console.log(shareimg);
		 	$(".emailImg").attr("href","mailto:?body="+shareimg)
		 	
	 	})

		var initMatrix = function(){
		
			Options= new Array ({
				Width: 1024,
				InnerWrapper: $('.matrix_inner_wraper'),
				ContainerLength: $('.matrix_inner_wraper .matrix_container').length,
				Draggalbe:$('.matrix_draggalbe'),
				Scroller:$('.matrix_scroller'),
				Items: $('.matrix_items'),
				Overlay: $('.matrix_layer_wrapper'),
				OverlayImg: $('.content_right .large_img'),
				BtLeft: $('.matrix_controller .bt_left'),
				BtRight: $('.matrix_controller .bt_right'),
				BtLeftOverlay: $('.bt_leftOverlay'),
				BtRightOverlay: $('.bt_rightOverlay'),
				DownloadImg: $('.downloadImg'),
				
				
				
				closeOverlay: $('.closeOverlay'),
				current: 0,
			});
		
			var m = Options[0];
			
			m.InnerWrapper.width(m.Width*m.ContainerLength);
			
			m.BtLeft.click(function(){
				if(m.current==0){
					return false;
				}else{
					move('right');
					moveDragg('left');
				}
			});
			
			m.BtRight.click(function(){
				if(m.current==m.ContainerLength-1){
					return false;
				}else{
					move('left');
					moveDragg('right');
				}
			});
			
			m.BtLeftOverlay.click(function(){
				if($('.matrix_items.current').prev().length){
					img = $('.matrix_items.current').prev().addClass('current_tmp').attr('data-img');
					m.Items.removeClass('current');
					$('.matrix_items.current_tmp').removeClass('current_tmp').addClass('current');
					m.OverlayImg.html('<img src="'+img+'" />');	
					setDownload();
				}else{
					closeLayer();
				}
			});
			
			m.BtRightOverlay.click(function(){
				if($('.matrix_items.current').next().length){
					img = $('.matrix_items.current').next().addClass('current_tmp').attr('data-img');
					m.Items.removeClass('current');
					$('.matrix_items.current_tmp').removeClass('current_tmp').addClass('current');
					m.OverlayImg.html('<img src="'+img+'" />');	
					setDownload();
				}else{
					closeLayer();
				}
			});
			
			
			
			
		m.Items.click(function(e){
			e.preventDefault();
			openLayer($(this).attr('data-img'));
			$(this).addClass('current');
			setDownload();
		});
		
		m.closeOverlay.click(function(e){
			e.preventDefault();
			closeLayer($(this).attr('data-img'));
		});
		
		openLayer = function(img){
			m.OverlayImg.html('<img src="'+img+'" />');
			m.Overlay.show();
		};
		
		closeLayer = function(){
			m.Overlay.hide();
			m.Items.removeClass('current');
		};
		setDownload = function(){
			m.DownloadImg.attr('href',$('.matrix_items.current').attr('data-img'));
		}
		
	/*
			$('._scroller').click(function(e){
				mOffset = e.offsetX;
				console.log(parseInt($('._draggalbe').css('left')));
				if( parseInt($('._draggalbe').css('left')) < mOffset){
					moveDragg('right');
				}else{
					moveDragg('left');
				}
			});
	*/
			
			m.Draggalbe.width(parseInt(m.Scroller.width()) / (m.ContainerLength));
			
			
			move = function(mDirection){
				m.InnerWrapper.animate({
				    left: mDirection=='left' ? parseInt(m.InnerWrapper.css('left'))-m.Width : parseInt(m.InnerWrapper.css('left'))+m.Width,
				  }, 1000, function() {
				    // Animation complete.
				});
			}
			
			moveDragg = function(mDirection){
				m.Draggalbe.animate({
					left: mDirection=='left' ?  parseInt(m.Draggalbe.css('left'))-parseInt(m.Scroller.width()) / (m.ContainerLength) : parseInt(m.Draggalbe.css('left'))+parseInt(m.Scroller.width()) / (m.ContainerLength)}, 1000, function() {
				    mDirection=='left'? m.current--: m.current++;
				});
			};
			return true;	
		};
		
		initMatrix();
        
    }

    function onEnter() {
       
    }

    function onExit() {
       
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();