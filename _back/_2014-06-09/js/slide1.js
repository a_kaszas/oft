SLIDES.slide1 = function() {

    //private
    function onInit(){
        $("#menuNav").hide();
        $(".nextNavBtn").css({right:"251px"});
    }

    function onEnter() {
      $("#menuNav").hide();
	  $(".nextNavBtn").animate({right:"251px"});	
	  moveImage ();
		
    } 

    function onExit() {
       	
       	 TweenMax.killAll();
		$("#menuNav").show();
		$(".nextNavBtn").animate({right:"0px"});

    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();

function moveImage () {
	 TweenMax.to($(".page1-background img"), 3, {width:"600px", height:"813px" ,delay:1,repeatDelay:4, repeat:10, yoyo:true });
}
