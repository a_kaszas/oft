<!DOCTYPE html>

<?php
session_start();
include 'include/data.php';
include 'include/slide5_helper.php';
?>

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Österreichische Fahrtechnik</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9">
    
    <link rel="stylesheet" href="css/global.css"/>
    <link rel="stylesheet" href="css/slide1.css"/>
    <link rel="stylesheet" href="css/slide2.css"/>
    <link rel="stylesheet" href="css/slide3.css"/>
    <link rel="stylesheet" href="css/slide4.css"/>
	<link rel="stylesheet" href="css/slide5.css"/>
	<link rel="stylesheet" href="css/slide6.css"/>
	<link rel="stylesheet" href="css/slide6.css"/>
	<link rel="stylesheet" href="css/vendor/jquery.kyco.preloader.css"/>
	
	<script type="text/javascript" src="http://fast.fonts.net/jsapi/0c381e6a-25d3-4567-977d-f551785884e0.js"></script>
	
    <script src="js/vendor/jquery-2.1.0.min.js"></script> 
    <script src="js/vendor/jquery.touchSwipe.min.js"></script>
    <script src="js/vendor/jquery.carouFredSel-6.2.1.js"></script>
	<script src="js/vendor/jquery.kyco.preloader.js"></script>
	<script src="js/vendor/TweenMax.min.js"></script>
	<script src="js/vendor/modernizr.custom.89522.js"></script>
	<script src="js/vendor/jQuery.print.js"></script>
	
	<script src="js/form.js"></script>
    <script src="js/global.js"></script>
    <script src="js/slide1.js"></script>
    <script src="js/slide2.js"></script>
    <script src="js/slide3.js"></script>
    <script src="js/slide4.js"></script>
    <script src="js/slide5.js"></script> 
    <script src="js/slide6.js"></script>
	
    <script> 
		
        $(document).ready(function() {
			start();
			$('body').kycoPreload();
           DIGIPAPER.init();
			
        });
    </script>

</head>
<body>


<div id="mainWrapper" class="<?php echo $trainincode;?>">

	
    <div class="slidesWrapper">
    
        <div id="slide1" class="singleSlide">
					
		<div class="single-holder">
		
			<?php include "include/slide1.php" ?>
				
		</div>				

			
		</div> <!--ENDE Slide1 -->
		
		
        <div id="slide2" class="singleSlide">
        <div class="single-holder">
        
         <?php include "include/slide2.php";?>
         
		</div>
		
        </div>  <!-- ENDE Slide2  -->
		 
		 <div id="slide3" class="singleSlide">
		 <div class="single-holder fg-cond">
        
        	<?php include "include/slide3.php";?> 
        
		</div>
        	
         
        
         </div>  <!-- ENDE Slide3  -->
		 
		 <div id="slide4" class="singleSlide">
        <div class="single-holder fg-cond">
        
        <!--  <img class="simageHolder" src="img/vorlage/page4.jpg" />  -->
      
         
         <div class="page2top"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
			<div class="header-text4 fg-cond">Welcher Trainingstyp sind Sie?</div>
			<div class="header-subtext4 fg-cond">3 Klicks zum Ergebnis.</div>
			
			<div class="steps fg-cond">
				
				<div class="step1 active">1</div>
				<div class="step2">2</div>
				<div class="step3">3</div>
				
				
			</div>
			
			<?php 
				if(in_array($trainincode ,array("MPX","MIT","M2T","MMF","MOX", "SMT", "MTT", "MWU"))) {
					include "include/slide4-moto.php";
					$type = "moto";
				}else if ($trainincode == "SUV") {
					include "include/slide4-suv.php";
					$type = "offroad";
				}else{
					include "include/slide4-auto.php";
					$type = "pkw";
				}
				
				
			?>			

        
   </div>
        	
                 
         </div>  <!-- ENDE Slide4  -->
		 
		 <div id="slide5" class="singleSlide">
         <div class="single-holder">
     	
        	<?php include "include/slide5.php";?>			
         
         </div>
        
         </div>  <!-- ENDE Slide5  -->
		 
		 
		 <div id="slide6" class="singleSlide">      
         	<div class="single-holder itc-book">
                
				 <?php include "include/slide6.php";?>      
        
			 </div>
         </div>  <!-- ENDE Slide6  -->
        
        
               
        
    </div>
    <div class="prevNavBtn"><img src="img/global/pfeil_links.svg" /></div>
    <div class="nextNavBtn"><img src="img/global/pfeil_rechts.svg" /></div>
    <ul id="menuNav">
    <li class="openThumbs"><img src="img/musterseite/icon_home.png" /></li>
        <li><a href="https://www.facebook.com/fahrtechnik" target="_blank"><img src="img/musterseite/icon_facebook.png" /></a></li>
      
        <li><a href="#slide6" class="caroufredsel"><img src="img/musterseite/icon_add.png" /></a></li>
    </ul>
</div>
<div id="navigationOverlay">
    <ul>
        <li><a href="#slide1" class="caroufredsel"><img src="img/global/_0005_1.png" /></a></li>
        <li><a href="#slide2" class="caroufredsel"><img src="img/global/_0004_2.png" /></a></li>
		<li><a href="#slide3" class="caroufredsel"><img src="img/global/_0003_3.png" /></a></li>				<li><a href="#slide4" class="caroufredsel"><img src="img/global/_0002_4.png" /></a></li>				<li><a href="#slide5" class="caroufredsel"><img src="img/global/_0001_5.png" /></a></li>				<li><a href="#slide6" class="caroufredsel"><img src="img/global/_0000_6.png" /></a></li>    </ul>
</div>


</body>
</html>