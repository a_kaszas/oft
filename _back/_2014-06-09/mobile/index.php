<?php
session_start();
include '../include/data.php';
include '../include/slide5_helper.php';
?>
<!DOCTYPE html>

<html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Österreichische Fahrtechnik</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;" />

    <link rel="stylesheet" href="css/custom.css"/>
    <script src="../js/vendor/jquery-2.1.0.min.js"></script>
    <script src="js/jquery.glide.min.js"></script>
    <script src="js/form.js"></script>
    <script src="js/custom.js"></script>

    <script>
        $(document).ready(function() {
            DIGIPAPERMOBILE.init();
        });
    </script>

</head>
<body>
<div id="mainWrapper">
	
	<div class="cover">
		
		<div class="header">
        	<img src="img/header.png" class="header"/>
		</div>
		
		<div class="headline"><?php print $anrede . " " . $vorname . " " . $nachname;?>,</div>
		<div class="subheadline">Fahrprofis werden nicht geboren, sondern gemacht.</div>
		
		<?php 
				if(in_array($trainincode ,array("MPX","MIT","M2T","MMF","MOX", "SMT", "MTT", "MWU"))) { ?>
					<div class="page1-background"><img src="/img/page1/start-back-moto.jpg" /></div>
				<?php }else if ($trainincode == "SUV") {?>
					<div class="page1-background"><img src="/img/page1/start-back-suv.jpg" /></div>
				<?php }else{?>
					<div class="page1-background"><img src="/img/page1/start-back-auto.jpg" /></div>
				<?php }
				
				
			?>		
			
			
			<div class="click-here">
				
				Hier klicken.
				
				
			</div>
		
	</div>

    <div class="header">
        <img src="img/header.png" class="header"/>
    </div>
    <div class="slidesWrapper">

        <div id="slide2" class="singleSlide">
            <div class="toggle">Das war ihr Fahrtraining</div>
            <div class="panel"><div class="headline">Das war Ihr Training.</div>
	            <div class="video-wrapper reset">		
	<?php 
	switch($trainincode) {
			case "PIT" : ?>
				<video id="video1" controls preload="none" poster="/videos/01pkwaktivtrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/01pkwaktivtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/01pkwaktivtrainingcrm.webm" type="video/webm" />
				<source src="/videos/01pkwaktivtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "P2T": ?>
				<video id="video1" controls preload="none" poster="/videos/02pkwdynamiktrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/02pkwdynamiktrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/02pkwdynamiktrainingcrm.webm" type="video/webm" />
				<source src="/videos/02pkwdynamiktrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "DRI": ?>
				<video id="video1" controls preload="none" poster="/videos/03pkwdrifttrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/03pkwdrifttrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/03pkwdrifttrainingcrm.webm" type="video/webm" />
				<source src="/videos/03pkwdrifttrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "PMF": ?>
				<video id="video1" controls preload="none" poster="/videos/04pkwmehrphasecrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/04pkwmehrphasecrm.mp4" type="video/mp4" />
				<source src="/videos/04pkwmehrphasecrm.webm" type="video/webm" />
				<source src="/videos/04pkwmehrphasecrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "OF1": ?>
				<video id="video1" controls preload="none" poster="/videos/05pkwoffroadtrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/05pkwoffroadtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/05pkwoffroadtrainingcrm.webm" type="video/webm" />
				<source src="/videos/05pkwoffroadtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "PRE": ?>
				<video id="video1" controls preload="none" poster="/videos/06pkwracingexperiencecrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/06pkwracingexperiencecrm.mp4" type="video/mp4" />
				<source src="/videos/06pkwracingexperiencecrm.webm" type="video/webm" />
				<source src="/videos/06pkwracingexperiencecrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "SET": ?>
				<video id="video1" controls preload="none" poster="/videos/07pkwschneeeistrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/07pkwschneeeistrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/07pkwschneeeistrainingcrm.webm" type="video/webm" />
				<source src="/videos/07pkwschneeeistrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "SUV": ?>
				<video id="video1" controls preload="none" poster="/videos/08pkwsuvtrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/08pkwsuvtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/08pkwsuvtrainingcrm.webm" type="video/webm" />
				<source src="/videos/08pkwsuvtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MPX": ?>
				<video id="video1" controls preload="none" poster="/videos/09mot125ertrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/09mot125ertrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/09mot125ertrainingcrm.webm" type="video/webm" />
				<source src="/videos/09mot125ertrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MIT": ?>
				<video id="video1" controls preload="none" poster="/videos/10motaktivtrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/10motaktivtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/10motaktivtrainingcrm.webm" type="video/webm" />
				<source src="/videos/10motaktivtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "M2T": ?>
				<video id="video1" controls preload="none" poster="/videos/11motdynamiktrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/11motdynamiktrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/11motdynamiktrainingcrm.webm" type="video/webm" />
				<source src="/videos/11motdynamiktrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MMF": ?>
				<video id="video1" controls preload="none" poster="/videos/12motmehrphasecrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/12motmehrphasecrm.mp4" type="video/mp4" />
				<source src="/videos/12motmehrphasecrm.webm" type="video/webm" />
				<source src="/videos/12motmehrphasecrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MOX": ?>
				<video id="video1" controls preload="none" poster="/videos/13motmopedtrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/13motmopedtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/13motmopedtrainingcrm.webm" type="video/webm" />
				<source src="/videos/13motmopedtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "SMT": ?>
				<video id="video1" controls preload="none" poster="/videos/14motspeedtrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/14motspeedtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/14motspeedtrainingcrm.webm" type="video/webm" />
				<source src="/videos/14motspeedtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MTT": ?>
				<video id="video1" controls preload="none" poster="/videos/15mottrialtrainingcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/15mottrialtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/15mottrialtrainingcrm.webm" type="video/webm" />
				<source src="/videos/15mottrialtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MWU": ?>
				<video id="video1" controls preload="none" poster="/videos/16motwarmupcrm.jpg" onended="videoend()" width="300" height="195">
				<source src="/videos/16motwarmupcrm.mp4" type="video/mp4" />
				<source src="/videos/16motwarmupcrm.webm" type="video/webm" />
				<source src="/videos/16motwarmupcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
		}
	?>
	

</div>

</div>
        </div>

        <div id="slide3" class="singleSlide">
            <div class="toggle">Die Trainings</div>
            <div class="panel">
            <div class="training auto" style="opacity: 1;">
				<div class="training-menu" style="z-index: 2;">
					<div class="start active">Start</div><div class="special">Special</div><div class="community">Community</div>
				</div>
				<div class="training-view">
					<div class="trainings-container start active">
						<div class="training-holder active">
							<div class="title">Aktiv Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Technik &amp; Sicherheit</li>
								<li>Slalom-Parcours</li>
								<li>Bremsen &amp; Ausweichen</li>
								<li>Kurvenübung</li>
								<li>Schleudern &amp; Stabilisieren</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Dynamik Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Technik &amp; Dynamik</li>
								<li>Kurvendynamik</li>
								<li>Spontaner Spurwechsel</li>
								<li>Aquaplaning</li>
								<li>Handlingkurs</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
					</div>
					
					<div class="trainings-container special hidden">
						<div class="training-holder active">
							<div class="title">Speed Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Slalom-Parcours</li>
								<li>Kurventechnik</li>
								<li>Aquaplaning</li>
								<li>Notspurwechsel</li>
								<li>Kurvendynamik &amp; Schleudern</li>
								
								<li>Handlingkurs</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Racing Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Rennstreckenverhalten</li>
								<li>Warm-up</li>
								<li>Linienwahl</li>
								<li>Anbremsen</li>
								<li>Kurventechnik</li>
								
								<li>Race Track</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Drift Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Techniken</li>
								<li>Gegenlenken</li>
								<li>Auslösen</li>
								<li>Halten</li>
								<li>Umsetzen</li>
								<li>Beenden</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Schnee &amp; Eis Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Sicherheit</li>
								<li>Warm-up</li>
								<li>Richtiges Bremsen</li>
								<li>Notspurwechsel</li>
								<li>Kurvendynamik</li>
								<li>Anfahren</li>
								<li>Handlingkurs</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Snow &amp; Fun Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Eisarena</li>
								<li>Kreisbahn</li>
								<li>Ausweichmanöver</li>
								<li>Spurwechsel</li>
								<li>Handling-Parcours</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">SUV Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Onroad Theorie</li>
								<li>Slalom</li>
								<li>Ausweichen</li>
								<li>Kurventechnik</li>
								<li>Schleudern</li>
								<li>Offroad Theorie</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Offroad Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Theoretische Einführung</li>
								<li>Korrekte Sitzposition</li>
								<li>Bergauf &amp; Bergab</li>
								<li>Spurrillen &amp; Schrägfahrten</li>
								<li>Geführte Geländefahrt</li>
								<li>Sicherheits-Check</li>
								<li>Feedback</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
					</div>
					<div class="trainings-container community hidden">
						<div class="training-holder active">
							<div class="title">Kart Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Rennmodus</li>
								<li>Outdoor-Kart-Strecken</li>
								<li>Modernste Karts</li>
								<li>Top-Technik</li>
								<li>Sicherheitsausstattung</li>
								<li>Zeitnehmung</li>
								<li>Auswertung</li>
								<li>Erstklassige Betreuung</li>
							</ul>
							<div class="dauer">Dauer <br> 1 Tag</div>
						</div>
					</div>
				</div>
				<div class="training-scroll">
					<img class="prev-training hidden" src="/img/page3/arrow-left.png" style="opacity: 1;">
					<img class="next-training" src="/img/page3/arrow-right.png" style="opacity: 1;">
				</div>
			</div>
			
			<div class="training moto">
				<div class="training-menu">
					<div class="start active">Start</div><div class="special">Special</div><div class="community">Community</div>
				</div>
				<div class="training-view">
					<div class="trainings-container start active">
						<div class="training-holder active">
							<div class="title">Aktiv Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Fahrphysik</li>
								<li>Lenkslalom</li>
								<li>Kreisbahn</li>
								<li>Spezialpacours</li>
								<li>Gefahrenanalyse</li>
								<li>Notbremsen</li>
								<lI>Ausweichen</lI>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Dynamik Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Trial</li>
								<li>Lenktechnik</li>
								<li>Notbremsen</li>
								<li>Kurvenbremsen & Ausweichen</li>
								<li>Handling/Linie</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
					</div>
					
					<div class="trainings-container special hidden">
						<div class="training-holder active">
							<div class="title">Speed Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Einzelkurven</li>
								<li>Fahrstil Hanging-off</li>
								<li>Kurvenkombinationen</li>
								<li>Sektionstraining</li>
								<li>Ideallinie</li>
								<li>High-Speed Bremsen</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
						<div class="training-holder">
							<div class="title">Enduro Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Beweglichkeit mit leichten Gel&auml;nde-Trial-Motorr&auml;dern</li>
								<li>Kurvenstil Dr&uuml;cken</li>
								<li>Hangfahren</li>
								<li>Bremsen auf unbefestigtem Untergrund</li>
								<li>Endurostrecke</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Supermoto Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Lenkimpuls</li>
								<li>Kurvenstil Dr&uuml;cken</li>
								<li>Kurvenkombinationen</li>
								<li>Linie</li>
								<li>Kurvenanbremsen</li>
								<li>Handlingstrecke</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Trial Training</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Gleichgewicht & Balance</li>
								<li>Feinmotorik bei Gas, Kupplung und Bremse</li>
								<li>Hindernisparcours</li>
								<li>Fahren auf Hang und Steilhang</li>
								<li>Schwieriges Terrain</li>
								<li>Vorderradheben</li>

							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Training & Ausfahrt</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Einfahren</li>
								<li>Notbremsen</li>
								<li>Tourbriefing</li>
								<li>Ausfahrt</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Motorrad Warm up</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Gefahren erkennen und richtig reagieren</li>
								<li>Bewegliches Motorrad</li>
								<li>Kurventechnik</li>
								<li>Enge Kurvenkombinationen</li>
								<li>Jeder Bremsmeter z&auml;hlt</li>
								<li>Handlingparcours</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
						
						<div class="training-holder">
							<div class="title">Personal Coaching</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
								<li>Individuelles Programm</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
					</div>
					<div class="trainings-container community hidden">
						<div class="training-holder active">
							<div class="title">Kart Experience</div>
							<div class="subtitle">INHALTE</div>
							<ul class="itc-book">
							<li>Rennmodus</li>
								<li>Outdoor-Kart-Strecken</li>
								<li> Modernste Karts</li>
								<li>Top-Technik</li>
								<li>Sicherheitsausstattung</li>
								<li>Zeitnehmung</li>
								<li>Auswertung</li>
								<li>Erstklassige Betreuung</li>
							</ul>
							<div class="dauer">Dauer <br /> 1 Tag</div>
						</div>
					</div>
				</div>
				<div class="training-scroll">
					<img class="prev-training hidden"  src="/img/page3/arrow-left.png" />
					<img class="next-training"  src="/img/page3/arrow-right.png" />
				</div>
			</div>


	            <div class="header-text3"> Die Trainings</div>
	            <div class="header-subtext3"> der ÖAMTC Fahrtechnik. </div>
			<div class="moto-holder">
				<div class="pfeil2"><img src="img/btn-moto-training.png"></div>
			</div>
			<div class="pkw-holder">
				<div class="pfeil2"><img src="img/btn-pkw-training.png"></div>
			</div>
			
						
			
            </div>
        </div>

        <div id="slide4" class="singleSlide">
            <div class="toggle">Welcher Trainingstyp sind Sie?</div>
            <div class="panel">
	            <div class="headline">Welcher Trainingstyp sind Sie? </div>
	            <div class="subheadline">3 Klicks zum Ergebnis.</div>
	            <?php 
				if(in_array($trainincode ,array("MPX","MIT","M2T","MMF","MOX", "SMT", "MTT", "MWU"))) {
					include "php/slide4-moto.php";
					$type = "moto";
				}else if ($trainincode == "SUV") {
					include "php/slide4-suv.php";
					$type = "offroad";
				}else{
					include "php/slide4-auto.php";
					$type = "pkw";
				}
			?>		
	            <div class="steps fg-cond">
					<div class="step1 active"></div>
					<div class="step2"></div>
					<div class="step3"></div>
				</div>
            </div>
        </div>

        <div id="slide5" class="singleSlide">
            <div class="toggle">Fotos der Fahrtechnik-Trainings</div>
            <div class="panel">
            	<?php if($bildercode != ""){?>
            	<div class="headline">Die Fotos von <?php echo $traininglong; ?></div>
            	<?php }else{?>
	            <div class="headline">Die Fotos von Ihrem Mehrphasen Training</div>
	            <?php }?>
	            	<div class="slideshow-holder"></div>
	            
            </div>
        </div>

        <div id="slide6" class="singleSlide">
            <div class="toggle">ÖAMTC Fahrtechnik Zentren</div>
            <div class="panel">
	            <div class="headline">In diesen ÖAMTC Fahrtechnik Zentren</div>
	            <div class="subheadline">werden Fahrprofis gemacht.</div>
	            <div class="clear"></div>
	            
	            <div class="zentren-info">
	            	 <div class="zentrum z1 hidden">
	            	 <div class="closer"></div>
					 <div class="headline">Zentrum Teesdorf</div>
					  
					 <div class="text">
						Kontakt:<br />
						Triester Bundesstraße 120<br />
						2524 Teesdorf<br />
						Telefon: 02253 817 00-32100<br />
						<a href="mailto:fahrtechnik@oeamtc.at">fahrtechnik@oeamtc.at</a>
					 </div>
					 
				 </div>
				 
				  <div class="zentrum z2 hidden">
				  	<div class="closer"></div>
					 <div class="headline">Zentrum Melk/Wachauring</div>
					 
					 <div class="text">
						Kontakt:<br />
						Am Wachauring 2<br />
						3390 Melk<br />
						Telefon: 02752 528 55<br />
						<a href="mailto:fahrtechnik.wachauring@oeamtc.at">fahrtechnik.wachauring<br />@oeamtc.at</a>
					 </div>
					
				 </div>
				 
				 <div class="zentrum z3 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Offroad Zentrum Stotzing</div>
					
					 <div class="text">
						Kontakt:<br />
						2451 Au am Leithaberge<br />
						Telefon: 02253 817 00-32100<br />
						<a href="mailto:fahrtechnik@oeamtc.at">fahrtechnik@oeamtc.at</a>
					 </div>
					
				 </div>
				 
				 <div class="zentrum z4 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Marchtrenk</div>
					 
					 <div class="text">
						Kontakt:<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon:07243 515 20<br />
						<a href="mailto:fahrtechnik.ooe@oeamtc.at">fahrtechnik.ooe@oeamtc.at</a>
					 </div>
					
				 </div>
				 
				 <div class="zentrum z5 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Saalfelden/Brandlhof</div>
					
					 <div class="text">
						Kontakt:<br />
						Hohlwegen 4<br />
						5760 Saalfelden<br />
						Telefon: 06582 752 60<br />
						<a href="mailto:fahrtechnik.saalfelden@oeamtc.at">fahrtechnik.saalfelden<br />@oeamtc.at</a>
					 </div>
					 				 </div>
				 
				 <div class="zentrum z6 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Innsbruck</div>
					
					 <div class="text">
						Kontakt:<br />
						Zenzenhof<br />
						6020 Innsbruck<br />
						Telefon: 0512 379 502<br />
						<a href="mailto:fahrtechnik.tirol@oeamtc.at">fahrtechnik.tirol@oeamtc.at</a>
						
					 </div>
					
				 </div>
				 
				 <div class="zentrum z7 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Röthis</div>
					 
					 <div class="text">
						Kontakt:<br />
						Bundesstraße 18<br />
						6832 Röthis<br />
						Telefon: 05522 812 20<br />
						<a href="mailto:fahrtechnik.vorarlberg@oeamtc.at">fahrtechnik.vorarlberg<br />@oeamtc.at</a>
					 </div>
				 </div>
				 
				 <div class="zentrum z8 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Zentrum Lang/Lebring</div>
					
					 <div class="text">
						Kontakt:<br />
						Jöß, Gewerbegebiet 1<br />
						8403 Lang<br />
						Telefon: 03182 401 65<br />
						<a href="mailto:fahrtechnik.lebring@oeamtc.at">fahrtechnik.lebring@oeamtc.at</a>
					 </div>
					 
				 </div>
				 
				 <div class="zentrum z9 hidden">
				 	<div class="closer"></div>
					  <div class="headline">Zentrum Kalwang</div>
					 <div class="text">
						Kontakt:<br />
						Kalwang 71<br />
						8775 Kalwang<br />
						Telefon: 03846 200 90<br />
						<a href="mailto:fahrtechnik.halwang@oeamtc.at">fahrtechnik.halwang@oeamtc.at</a>
					 </div>
					 				 </div>
				 
				 <div class="zentrum z10 hidden">
				 	<div class="closer"></div>
					  <div class="headline">Zentrum St. Veit a. d. Glan</div>
					
					 <div class="text">
						Kontakt:<br />
						Mölbling /Mail<br />
						9300 St. Veit a. d. Glan<br />
						Telefon: 04212 331 70<br />
						<a href="mailto:fahrtechnik.kaernten@oeamtc.at">fahrtechnik.kaernten@oeamtc.at</a>
					 </div>
					
				 </div>
				 
				 <div class="zentrum z11 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Lackenhof Ötscher</div>
					 
					 <div class="text">
						Kontakt:<br />
						Tel: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum<br />@oeamtc.at</a>
				 </div>
				 </div>
				 
				 <div class="zentrum z12 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Winterzentrum Faistenau/Fuschlsee</div>
					
					 <div class="text">
						Kontakt:<br />
						Tel: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum<br />@oeamtc.at</a>
				 </div>
				 </div>
				  
				 <div class="zentrum z13 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Winterzentrum Semmering/Stuhleck </div>
					 
					 <div class="text">
						Kontakt:<br />
						Tel: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum<br />@oeamtc.at</a>
					 </div>
					 
				 </div>
				 
				 
				 <div class="zentrum z14 hidden">
				 	<div class="closer"></div>
					 <div class="headline">Winterzentrum Spittal a. d. Drau</div>
					 <div class="text">
						Kontakt:<br />
						Tel: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum<br />@oeamtc.at</a>
					 </div>
				 </div>
	            
	            </div>
	            
	            <div class="zentren-select">
					<ul>
						<li class="li-headline">ÖAMTC Fahrtechnik Zentren</li>
						<li id="z1">    1 Zentrum Teesdorf (NÖ)							   </li>
						<li id="z2">    2 Zentrum Melk/Wachauring (NÖ)					   </li>
						<li id="z3">    3 Offroad Zentrum Stotzing (B)					   </li>
						<li id="z4">    4 Zentrum Marchtrenk (OÖ)						   </li>
						<li id="z5">    5 Zentrum Saalfelden/Brandlhof (S)				   </li>
						<li id="z6">    6 Zentrum Innsbruck (T)							   </li>
						<li id="z7">    7 Zentrum Röthis (V)							   </li>
						<li id="z8">    8 Zentrum Lang/Lebring (STMK)					   </li>
						<li id="z9">    9 Zentrum Kalwang (STMK)						   </li>
						<li id="z10">    10 Zentrum St. Veit a. d. Glan (K)				   </li>
						<li class="li-headline">ÖAMTC Fahrtechnik Winterzentren</li>
						<li id="z11">	11 Winterzentrum Göstling (NÖ)					   </li>
						<li id="z12">	12 Winterzentrum Faistenau/Fuschlsee (S)		   </li>
						<li id="z13">	13 Winterzentrum Semmering/Stuhleck (Stmk)		   </li>
						<li id="z14">	14 Winterzentrum Spittal a. d. Drau/Stockenboi (K) </li>
					</ul>	            
	            </div>
	            
	            
	            
	            
            </div>
        </div>
        <div id="slide7" class="singleSlide">
            <div class="toggle">Mit Freunden teilen</div>
            <div class="panel">
	            
	            <?php include "php/slide-7.php";?>
	            
            </div>
        </div>

    </div>
 </div>
</body>
</html>