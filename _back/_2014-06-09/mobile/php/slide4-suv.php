<div id="auto-fragen" class="auto-fragen itc-book suv">
				<div class="frage-1 fragen">
					<div class="frage fg-cond">Bei Offroad-Trips …</div>
					<div class="first" data="blau" style="left: 0px;">
						blühen Sie auf.
					</div>
					<div class="second" data="gruen" style="left: 0px;">
						waren Sie bisher zurückhaltend.
					</div>
					<div class="third" data="lila" style="left: 0px;">
						wollen Sie noch einiges perfektionieren.
					</div>
					<span id="first" class="weiter">Weiter</span>
				</div>
				
				<div class="frage-2 fragen">
					<div class="frage reset fg-cond">Wenn es über Stock und Stein geht…</div>
					<div class="first reset" data="blau" style="left: 0px;">
						sind Sie in Ihrem Element.
					</div>
					<div class="second reset" data="gruen" style="left: 0px;">
						fahren Sie besonders aufmerksam.
					</div>
					<div class="third reset" data="lila" style="left: 0px;">
						nehmen Sie die Herausforderung sportlich.
					</div>
					<span id="second" class="weiter reset">Weiter</span>
				</div>
				
				<div class="frage-3 fragen">
					<div class="frage reset fg-cond">Steile Hänge …</div>
					<div class="first reset" data="gruen" style="left: 0px;">
						liefern Ihnen einen richtigen Adrenalin-Kick.
					</div>
					<div class="second reset" data="blau" style="left: 0px;">
						vermeiden Sie, wenn es geht.
					</div>
					<div class="third reset" data="lila" style="left: 0px;">
						wecken Ihren Sportsgeist.
					</div>
					<span id="third" class="weiter reset">Weiter</span>
				</div>
			
			</div>
			
				
			<div class="antworten suv">
			
			<div id="antwort-a" class="antwort">
				<div class="antwort-headline">Sie sind ein Abenteurer.</div>
				<div class="antwort-subheadline itc-book"> Schwierige Fahrmanöver, bei denen Sie zeigen <br />
können, was in Ihnen steckt, sind ganz <br />
nach Ihrem Geschmack.</div>
				<div class="training-suggestions">
					<div class="training-1">Speed Training</div>
					<div class="training-2">Personal Coaching</div>
					<div class="training-3">Drive & Fly</div>
					<div id="SET" class="training-4">Schnee & Eis / Snow & Fun Training</div>
				</div>
			</div>
			
			<div id="antwort-b" class="antwort">
				<div class="antwort-headline">Sie sind der Verantwortungsvolle.</div>
				<div class="antwort-subheadline itc-book"> Eine ruhige, sichere und vorausschauende<br>Fahrweise zeichnet Sie aus. </div>
				<div class="training-suggestions">
					<div class="training-1">Aktiv Training</div>
					<div id="P2T" class="training-2">Dynamik Training</div>
					<div id="SET" class="training-3">Schnee & Eis / Snow & Fun Training</div>
					<div class="training-4">Personal Coaching</div>
				</div>
			</div>
			
			<div id="antwort-c" class="antwort">
				<div class="antwort-headline">Sie sind der Sportlich Gefühlvolle.</div>
				<div class="antwort-subheadline itc-book">Beim Fahren vertrauen Sie auf Ihr Gefüh<br>und ihre Erfahrung.</div>
				<div class="training-suggestions">
					<div id="P2T" class="training-1">Dynamik Training</div>
					<div class="training-2">Speed Training</div>
					<div id="SET" class="training-3">Schnee & Eis / Snow & Fun Training</div>
					<div class="training-4">Personal Coaching</div>
				</div>
			</div>			
			</div>
			
			<div class="buchung reset">
				<div class="closer"></div>
				<div id="suv" class="final-training">Speed Training</div>
				<div class="open-form"><img src="img/btn-buchen.png" /></div>
				<div class="final-anrede"><?php print $anrede . " " . $vorname . " " . $nachname;?>, Ihr Gutscheincode lautet:</div>
				<div class="gutscheincode"><?php echo $gutscheincode;?></div>
				
				<div class="gutschein-wrapper">
					<img class="headline-keine-zeit" src="img/headline-keine-zeit.png" />
					<img class="gutschein-back" src="img/btn-gutschein.png" />
					<div class="gutschein-bestellen"></div>
				</div>
			</div>
