<div id="auto-fragen" class="auto-fragen itc-book">
				<div class="frage-1 fragen">
					<div class="frage fg-cond">Wenn die Ampel auf Gelb steht... </div>
					<div class="first" data="blau" style="left: 0px;">
						steigen Sie noch aufs Gas. 
					</div>
					<div class="second" data="gruen" style="left: 0px;">
						bleiben Sie stehen.
					</div>
					<div class="third" data="lila" style="left: 0px;">
						entscheiden Sie jedes Mal nach Gefühl. 
					</div>
					<span id="first" class="weiter">Weiter</span>
				</div>
				
				<div class="frage-2 fragen">
					<div class="frage reset fg-cond">Beim Einparken... </div>
					<div class="first reset" data="blau" style="left: 0px;">
						macht Ihnen keiner was vor, Sie sind Spezialist.
					</div>
					<div class="second reset" data="gruen" style="left: 0px;">
						lassen Sie sich Zeit und konzentrieren sich.
					</div>
					<div class="third reset" data="lila" style="left: 0px;">
						vertrauen Sie ganz Erfahrung und Gefühl.
					</div>
					<span id="second" class="weiter reset">Weiter</span>
				</div>
				
				<div class="frage-3 fragen">
					<div class="frage reset fg-cond">Auf der Autobahn... </div>
					<div class="first reset" data="blau" style="left: 0px;">
						stellt Überholen für Sie kein Problem dar.
					</div>
					<div class="second reset" data="gruen" style="left: 0px;">
						bleiben Sie meistens auf der rechten Spur.
					</div>
					<div class="third reset" data="lila" style="left: 0px;">
						ist Ihr Fahrstil immer souverän.
					</div>
					<span id="third" class="weiter reset">Weiter</span>
				</div>
			
			</div>
			
				
			<div class="antworten">
			
			<div id="antwort-a" class="antwort">
				<div class="antwort-headline"> Sie sind ein Action-Liebhaber. </div>
				<div class="antwort-subheadline itc-book"> Geschwindigkeit, Spaß und zügige Fahrmanöver<br>sind ganz nach Ihrem Geschmack. </div>
				<div class="training-suggestions">
					<div id="DRI" class="training-1">Drift Training</div>
					<div id="SET" class="training-2">Schnee & Eis Training</div>
					<div class="training-3">Racing Experience</div>
					<div class="training-4">Personal Coaching</div>
				</div>
			</div>
			
			<div id="antwort-b" class="antwort">
				<div class="antwort-headline"> Sie sind der Sicherheitsbewusste </div>
				<div class="antwort-subheadline itc-book"> Eine ruhige, sichere und vorausschauende<br>Fahrweise zeichnet Sie aus. </div>
				<div class="training-suggestions">
					<div id="P2T" class="training-1">Dynamik Training</div>
					<div id="SET" class="training-2">Schnee & Eis Training</div>
					<div class="training-3">Personal Coaching</div>
					<div class="training-4">Speed Training</div>
				</div>
			</div>
			
			<div id="antwort-c" class="antwort">
				<div class="antwort-headline"> Sie sind der Sportlich-Dynamische. </div>
				<div class="antwort-subheadline itc-book"> Sie fahren sicher und souverän und passen<br>Ihren Fahrstil der Verkehrssituation an. </div>
				<div class="training-suggestions">
					<div class="training-1">Speed Training</div>
					<div id="SET" class="training-2">Schnee & Eis Training</div>
					<div id="PRE" class="training-3">Racing Experience</div>
					<div id="DRI" class="training-4">Drift Training</div>
				</div>
			</div>			
			</div>
			
			<div class="buchung reset">
				<div class="closer"></div>
				<div id="pkw" class="final-training">Speed Training</div>
				<div class="open-form"><img src="img/btn-buchen.png" /></div>
				<div class="final-anrede"><?php print $anrede . " " . $vorname . " " . $nachname;?>, Ihr Gutscheincode lautet:</div>
				<div class="gutscheincode"><?php echo $gutscheincode;?></div>
				
				<div class="gutschein-wrapper">
					<img class="headline-keine-zeit" src="img/headline-keine-zeit.png" />
					<img class="gutschein-back" src="img/btn-gutschein.png" />
					<div class="gutschein-bestellen"></div>
				</div>
			</div>
