<?php 
	
	session_start();
	$kunde =  $_SERVER["REQUEST_URI"];
	$kunde = explode("=", $kunde);
	$kunde = $kunde[1];
	if (strpos($kunde, '&') !== FALSE) {
		$kunde = explode("=", $kunde);
		$kunde = $kunde[0];
	}
	$_SESSION['id'] = $kunde;
	require_once 'include/vendor/Mobile_Detect.php';
	$detect = new Mobile_Detect;
	
	$browser = $_SERVER['HTTP_USER_AGENT'];
	
if($detect->isTablet()){
 	header('Location:desktop.php');
}else if($detect->isMobile()){
	if(strpos($browser, 'Android') !== FALSE){
		$browser = explode("Android",$browser);
		$browser = explode(";",$browser[1]);
		$browser = $browser[0];
		$string_number = $browser;
		
		if($browser < 4.0) {
			header('Location:include/mob/fallback.php');
		} else {
			header('Location:mobile/index.php');
		}
		
		
	}else {
		header('Location:mobile/index.php');
	}
 	
}else{
	if (strpos($browser, 'MSIE 8') !== FALSE){
		header('Location:include/desktop/fallback.php');
	}else if (strpos($browser, 'MSIE 7') !== FALSE) {
 		header('Location:include/desktop/fallback.php');
 	} else {
	 	header('Location:desktop.php');

 	}
}
	
?>