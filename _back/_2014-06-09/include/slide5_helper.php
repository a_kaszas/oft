<?php 

/*
 * Description: sanitize the Json string data, 
 *			 generate items: id, thumb_img, img
 *			 returne themed output
 */
function sanitizeCallback($data,$bildercode){
	$ignore = array("meincallback(", ")",'/srv/www/vhosts/oeamtc1.it-wms.com/htdocs/');
	$output = str_replace($ignore, "", $data);
	$output = str_replace($ignore, "", json_decode($output));
	
	$items = array();
	foreach($output as $key => $item){
		$result_explode = explode("/", $item);
		$data = array('id'=> $result_explode[2],
				   'thumb_img' => $item,
				   'img' => str_replace("tmb_", "", $item)
		);
		$items[]= $data;
	}
	return theme($items,$bildercode);
}

/*
 * Description: theme given items
 * 				return html output
 */
function theme($items,$bildercode){
	$imgSchema = getImgSchema();
	
	$output = '';
	$output_tmp ='';
	
	foreach($items as $key => $item){
		//if output_tmp equals mod 15, wrapp them with container
		if($key%15==0 && $key!=0){
			$output .= '<div class="matrix_container">'.$output_tmp.'</div>';
			$output_tmp = '';
		}
		
		
		$output_tmp .='<a href="#" data-img="http://oeamtc1.it-wms.com/archive/years/'.$bildercode."/".$item['img'].'" class="matrix_items item_'.$imgSchema[$key%15].'"><span class="matrix_img" style="background:url(http://oeamtc1.it-wms.com/archive/years/'.$bildercode."/".$item['img'].')" >  <span class="icon_plus"></span> </span></a>';
		
		//check for the rest of the items if not mod15 and wrapp them with container
		if($key==count($items)-1 && count($items)%15!=0){
			$output .= '<div class="matrix_container">'.$output_tmp.'</div>';
		}
	}
	return $output;
}

/*
 * Description: Image Schema
 *				allowed values: big, small
 *				amout of values 15 Items per page
 */
function getImgSchema(){
	return array('big','small','big','small','small','small','big','small','big','small','small','small','big','small','big');
}

?>