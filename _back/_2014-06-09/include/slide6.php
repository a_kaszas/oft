<div class="page2top fg-cond"><img src="img/page2/hoch-gelb-back2.png" /></div>
				
				<div class="mainheadline">In diesen ÖAMTC Fahrtechnik Zentren</div>
				<div class="subheadline">werden Fahrprofis gemacht.</div>
				
				 
				 <div class="zentrum one hidden">
					 <div class="headline">Zentrum Teesdorf</div>
					 <div class="bild"><img src="img/page6/zentren/teesdorf.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Triester Bundesstraße 120<br />
						2524 Teesdorf<br />
						Telefon: 02253 817 00-32100<br />
						<a href="mailto:fahrtechnik@oeamtc.at">fahrtechnik@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/%C3%96AMTC+Fahrtechnikzentrum/@47.960661,16.275947,1021m/data=!3m2!1e3!4b1!4m2!3m1!1s0x476db15a244a34eb:0xdb200f969ce39106" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				  <div class="zentrum two hidden">
					 <div class="headline">Zentrum Melk/Wachauring</div>
					 <div class="bild"><img src="img/page6/zentren/melk.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Am Wachauring 2<br />
						3390 Melk<br />
						Telefon: 02752 528 55<br />
						<a href="mailto:fahrtechnik.wachauring@oeamtc.at">fahrtechnik.wachauring<br />@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Am+Wachauring+2/@48.2124991,15.3278159,1016m/data=!3m2!1e3!4b1!4m2!3m1!1s0x477265b5010f692f:0xbe17e1c0bc3ba4c2" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum three hidden">
					 <div class="headline">Offroad Zentrum Stotzing</div>
					 <div class="bild"><img src="img/page6/zentren/stotzing.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						2451 Au am Leithaberge<br />
						Telefon: 02253 817 00-32100<br />
						<a href="mailto:fahrtechnik@oeamtc.at">fahrtechnik@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Au+am+Leithaberge/@47.9198186,16.5510152,4333m/data=!3m1!1e3!4m2!3m1!1s0x476c4f29cc0d4417:0xc6f99f42e7ad153a" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum four hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentren/marchtrenk.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon:07243 515 20<br />
						<a href="mailto:fahrtechnik.ooe@oeamtc.at">fahrtechnik.ooe@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2114736,14.1183743,1017m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum five hidden">
					 <div class="headline">Zentrum Saalfelden/Brandlhof</div>
					 <div class="bild"><img src="img/page6/zentren/brandlhof.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Hohlwegen 4<br />
						5760 Saalfelden<br />
						Telefon: 06582 752 60<br />
						<a href="mailto:fahrtechnik.saalfelden@oeamtc.at">fahrtechnik.saalfelden<br />@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Hohlwegen+4/@47.4829616,12.8308315,1026m/data=!3m1!1e3!4m2!3m1!1s0x4776fad9aa898bff:0xd646a7248d946e43" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum six hidden">
					 <div class="headline">Zentrum Innsbruck</div>
					 <div class="bild"><img src="img/page6/zentren/innsbruck.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Zenzenhof<br />
						6020 Innsbruck<br />
						Telefon: 0512 379 502<br />
						<a href="mailto:fahrtechnik.tirol@oeamtc.at">fahrtechnik.tirol@oeamtc.at</a>
						
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Handlhofweg+81/@47.2261189,11.3912407,1035m/data=!3m2!1e3!4b1!4m2!3m1!1s0x479d698607b4aed5:0xf3932f1c703427d" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum seven hidden">
					 <div class="headline">Zentrum Röthis</div>
					 <div class="bild"><img src="img/page6/zentren/roethis.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Bundesstraße 18<br />
						6832 Röthis<br />
						Telefon: 05522 812 20<br />
						<a href="mailto:fahrtechnik.vorarlberg@oeamtc.at">fahrtechnik.vorarlberg<br />@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Bundesstra%C3%9Fe+18/@47.3045088,9.6164888,1034m/data=!3m2!1e3!4b1!4m2!3m1!1s0x479b3e96dff4e1e1:0xc3e18719f614c77c" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum eight hidden">
					 <div class="headline">Zentrum Lang/Lebring</div>
					 <div class="bild"><img src="img/page6/zentren/lebring.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Jöß, Gewerbegebiet 1<br />
						8403 Lang<br />
						Telefon: 03182 401 65<br />
						<a href="mailto:fahrtechnik.lebring@oeamtc.at">fahrtechnik.lebring@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/J%C3%B6%C3%9F-Gewerbegebiet+1/@46.8454827,15.519713,1043m/data=!3m2!1e3!4b1!4m2!3m1!1s0x476fbab94a5e4e05:0x91e06409bffb487c" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum nine hidden">
					  <div class="headline">Zentrum Kalwang</div>
					 <div class="bild"><img src="img/page6/zentren/kalwang.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Kalwang 71<br />
						8775 Kalwang<br />
						Telefon: 03846 200 90<br />
						<a href="mailto:fahrtechnik.halwang@oeamtc.at">fahrtechnik.halwang@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Kalwang+71/@47.422774,14.7592664,1032m/data=!3m2!1e3!4b1!4m2!3m1!1s0x4771c00aab70cc33:0x1b465ff16417029a" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum ten hidden">
					  <div class="headline">Zentrum St. Veit a. d. Glan</div>
					 <div class="bild"><img src="img/page6/zentren/glan.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Mölbling /Mail<br />
						9300 St. Veit a. d. Glan<br />
						Telefon: 04212 331 70<br />
						<a href="mailto:fahrtechnik.kaernten@oeamtc.at">fahrtechnik.kaernten@oeamtc.at</a>
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Mail/@46.8046404,14.4073763,1055m/data=!3m1!1e3!4m2!3m1!1s0x47706b1bd5c14cfd:0x91812e9aef9fb415" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum eleven hidden">
					 <div class="headline">Lackenhof Ötscher</div>
					 <div class="bild"><img src="img/page6/zentren/gostling.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Tel: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum<br />@oeamtc.at</a>
				 </div>
				 </div>
				 
				 <div class="zentrum twelve hidden">
					 <div class="headline">Winterzentrum Faistenau/Fuschlsee</div>
					 <div class="bild"><img src="img/page6/zentren/fuschlsee.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Tel: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum<br />@oeamtc.at</a>
				 </div>
				 </div>
				  
				 <div class="zentrum thirteen hidden">
					 <div class="headline">Winterzentrum Semmering/Stuhleck </div>
					 <div class="bild"><img src="img/page6/zentren/stuhleck.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Tel: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum<br />@oeamtc.at</a>
					 </div>
					 
				 </div>
				 
				 
				 <div class="zentrum fourteen hidden">
					 <div class="headline">Winterzentrum Spittal a. d. Drau</div>
					 <div class="bild"><img src="img/page6/zentren/spittal.jpg" /></div>
					 <div class="text">
						Kontakt:<br />
						Tel: 02752 528 55<br />
						<a href="mailto:fahrtechnik.winterzentrum@oeamtc.at">fahrtechnik.winterzentrum<br />@oeamtc.at</a>
					 </div>
				 </div>
				 
				 <div class="karte">
				 	<div id="one" class="flag"></div>
				 	<div id="two" class="flag"></div>
				 	<div id="three" class="flag"></div>
				 	<div id="four" class="flag"></div>
				 	<div id="five" class="flag"></div>
				 	<div id="six" class="flag"></div>
				 	<div id="seven" class="flag"></div>
				 	<div id="eight" class="flag"></div>
				 	<div id="nine" class="flag"></div>
				 	<div id="ten" class="flag"></div>
				 	<div id="eleven" class="flag"></div>
				 	<div id="twelve" class="flag"></div>
				 	<div id="thirteen" class="flag"></div>
				 	<div id="fourteen" class="flag"></div>
					 <img src="img/page6/karte.png" />
				 </div>
				 
				 
		         <div class="form-wrapper fg-cond">
		         <div class="form-headline">Mit Freunden teilen</div>
		        
					 <form id="form" action="" method="post">
<div class="form-box first">
<div class="form-sub">Bitte Felder mit Ihren Daten ausfüllen.</div>
<div class="anrede"><input checked type="radio" name="sex" value="male"><span>Herr</span></div>
<div class="anrede"><input type="radio" name="sex" value="female"><span>Frau</span></div>
<div class="titel formfield"><input type="text" name="titel" value="" tabindex="3" id="titel"><br />Titel</div>
<div class="clear"></div>
<div class="vorname formfield"><input type="text" name="vorname" value="" tabindex="1" id="vorname"><br />Vorname*</div>
<div class="nachname formfield"><input type="text" name="nachname" value="" tabindex="2" id="nachname"><br />Nachname*</div>
<div class="email formfield"><input type="text" name="email" value="" tabindex="7" id="email"><br />E-Mail*</div>
</div>

<div class="form-box second">
<div class="form-sub">Bitte Felder mit den Daten Ihres Freundes ausfüllen.</div>
<div class="anrede"><input checked type="radio" name="sex2" value="male"><span>Herr</span></div>
<div class="anrede"><input type="radio" name="sex2" value="female"><span>Frau</span></div>
<div class="titel formfield"><input type="text" name="titel2" value="" tabindex="3" id="titel2"><br />Titel</div>
<div class="clear"></div>
<div class="vorname formfield"><input type="text" name="vorname2" value="" tabindex="1" id="vorname2"><br />Vorname*</div>
<div class="nachname formfield"><input type="text" name="nachname2" value="" tabindex="2" id="nachname2"><br />Nachname*</div>
<div class="email formfield"><input type="text" name="email2" value="" tabindex="7" id="email2"><br />E-Mail*</div>
</div>

<textarea rows="4" cols="50">
Hallo!
Ich habe bei den ÖAMTC Fahrtechnik Trainings mitgemacht und hatte richtig viel Spaß. 
Probier’s aus!
</textarea>

<div class="agbs"><input type="checkbox" id="agb" name="agb" value="agb"> AGB*</div>
<input type="submit" value="Senden" id="send">


</form>

<div id="danke">Ihre Nachricht wurde erfolgreich versendet! <br />
	Ihr ÖFT Team
</div>

		         </div>
