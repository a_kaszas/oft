 <div class="page2top reset"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
			
			
			<div class="header-subtext2 reset fg-cond"> Das war Ihr Training. </div>
			
			<div class="index-menu reset fg-cond">
				<div class="page2-background"><img src="img/page2/hintergrund-2.png" /></div>
				
				<div class="link1"><a href="#slide6" class="caroufredsel"><img src="img/page2/fahrtechnik-2.png" /></a></div>
	
				<div class="link2"><a href="#slide4" class="caroufredsel"><img src="img/page2/trainingstyp.png" /></a></div>
				
				<div class="link3"><a href="#slide5" class="caroufredsel"><img src="img/page2/fotogalerie.png" /></a></div>
				
				<div class="link4"><a href="#slide3" class="caroufredsel"><img src="img/page2/produkte.png" /></a></div>
	
				<div class="text-link4">Die Produkte<br>der ÖAMTC<br>Fahrtechnik </div>
				
				<div class="text-link2">Welcher<br>Trainingstyp<br>sind Sie </div>
				
				<div class="text-link3">Ihre<br>Fotogalerie </div>
	
				<div class="text-link1">Fahrtechnik<br>Zentren </div>
			
			</div>

<div class="video-wrapper reset">		
<img src="img/page2/btn-play.png" id="play" class="video1" />	
	<?php 
	//echo $trainincode;
	switch($trainincode) {
			case "PIT" : ?>
				<video id="video1" poster="/videos/01pkwaktivtrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/01pkwaktivtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/01pkwaktivtrainingcrm.webm" type="video/webm" />
				<source src="/videos/01pkwaktivtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "P2T": ?>
				<video id="video1" poster="/videos/02pkwdynamiktrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/02pkwdynamiktrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/02pkwdynamiktrainingcrm.webm" type="video/webm" />
				<source src="/videos/02pkwdynamiktrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "DRI": ?>
				<video id="video1" poster="/videos/03pkwdrifttrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/03pkwdrifttrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/03pkwdrifttrainingcrm.webm" type="video/webm" />
				<source src="/videos/03pkwdrifttrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "PMF": ?>
				<video id="video1" poster="/videos/04pkwmehrphasecrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/04pkwmehrphasecrm.mp4" type="video/mp4" />
				<source src="/videos/04pkwmehrphasecrm.webm" type="video/webm" />
				<source src="/videos/04pkwmehrphasecrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "OF1": ?>
				<video id="video1" poster="/videos/05pkwoffroadtrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/05pkwoffroadtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/05pkwoffroadtrainingcrm.webm" type="video/webm" />
				<source src="/videos/05pkwoffroadtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "PRE": ?>
				<video id="video1" poster="/videos/06pkwracingexperiencecrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/06pkwracingexperiencecrm.mp4" type="video/mp4" />
				<source src="/videos/06pkwracingexperiencecrm.webm" type="video/webm" />
				<source src="/videos/06pkwracingexperiencecrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "SET": ?>
				<video id="video1" poster="/videos/07pkwschneeeistrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/07pkwschneeeistrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/07pkwschneeeistrainingcrm.webm" type="video/webm" />
				<source src="/videos/07pkwschneeeistrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "SUV": ?>
				<video id="video1" poster="/videos/08pkwsuvtrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/08pkwsuvtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/08pkwsuvtrainingcrm.webm" type="video/webm" />
				<source src="/videos/08pkwsuvtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MPX": ?>
				<video id="video1" poster="/videos/09mot125ertrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/09mot125ertrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/09mot125ertrainingcrm.webm" type="video/webm" />
				<source src="/videos/09mot125ertrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MIT": ?>
				<video id="video1" poster="/videos/10motaktivtrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/10motaktivtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/10motaktivtrainingcrm.webm" type="video/webm" />
				<source src="/videos/10motaktivtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "M2T": ?>
				<video id="video1" poster="/videos/11motdynamiktrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/11motdynamiktrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/11motdynamiktrainingcrm.webm" type="video/webm" />
				<source src="/videos/11motdynamiktrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MMF": ?>
				<video id="video1" poster="/videos/12motmehrphasecrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/12motmehrphasecrm.mp4" type="video/mp4" />
				<source src="/videos/12motmehrphasecrm.webm" type="video/webm" />
				<source src="/videos/12motmehrphasecrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MOX": ?>
				<video id="video1" poster="/videos/13motmopedtrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/13motmopedtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/13motmopedtrainingcrm.webm" type="video/webm" />
				<source src="/videos/13motmopedtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "SMT": ?>
				<video id="video1" poster="/videos/14motspeedtrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/14motspeedtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/14motspeedtrainingcrm.webm" type="video/webm" />
				<source src="/videos/14motspeedtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MTT": ?>
				<video id="video1" poster="/videos/15mottrialtrainingcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/15mottrialtrainingcrm.mp4" type="video/mp4" />
				<source src="/videos/15mottrialtrainingcrm.webm" type="video/webm" />
				<source src="/videos/15mottrialtrainingcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
			case "MWU": ?>
				<video id="video1" poster="/videos/16motwarmupcrm.jpg" onended="videoend()" width="1024" height="668">
				<source src="/videos/16motwarmupcrm.mp4" type="video/mp4" />
				<source src="/videos/16motwarmupcrm.webm" type="video/webm" />
				<source src="/videos/16motwarmupcrm.ogv" type="video/ogg" />
				</video>
			<?php break;
		}
	?>
	

</div>
