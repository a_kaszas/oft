<div class="auto-back"><img src="img/page4/back-moto.jpg" /></div>
<div id="auto-fragen" class="auto-fragen itc-book moto">
				<div class="frage-1 fragen">
					<div class="frage fg-cond">Sie laufen auf eine Gruppe<br />
Motorradfahrer auf – was machen Sie?</div>
					<div class="first" data="blau" style="left: 0px;">
						“Herbrennen“ – Sie überholen möglichst schnell
					</div>
					<div class="second" data="gruen" style="left: 0px;">
						Sie schließen sich der Gruppe an 
					</div>
					<div class="third" data="lila" style="left: 0px;">
						Sie suchen sich eine ruhigere Straße
					</div>
					<span id="first" class="weiter">Weiter</span>
				</div>
				
				<div class="frage-2 fragen ">
					<div class="frage reset fg-cond">Sie sind auf einer<br />
Bergstraße unterwegs …</div>
					<div class="first reset" data="blau" style="left: 0px;">
						und reizen jede Kurve aus.
					</div>
					<div class="second reset" data="gruen" style="left: 0px;">
						und versuchen Ihre Blicktechnik zu perfektionieren.
					</div>
					<div class="third reset" data="lila" style="left: 0px;">
						und fahren Ihren Kumpels hinterher.
					</div>
					<span id="second" class="weiter reset">Weiter</span>
				</div>
				
				<div class="frage-3 fragen">
					<div class="frage reset fg-cond">Im Ortsgebiet...</div>
					<div class="first reset" data="gruen" style="left: 0px;">
						bewegen Sie sich zwischen den Autoschlangen.
					</div>
					<div class="second reset" data="blau" style="left: 0px;">
						bleiben Sie auf Ihrem Fahrstreifen. 
					</div>
					<div class="third reset" data="lila" style="left: 0px;">
						achten Sie darauf, nicht übersehen zu werden.
					</div>
					<span id="third" class="weiter reset">Weiter</span>
				</div>
			
			</div>
			
				
			<div class="antworten moto">
			
			<div id="antwort-a" class="antwort">
				<div class="antwort-headline"> Sie sind ein Action-Liebhaber. </div>
				<div class="antwort-subheadline itc-book"> Geschwindigkeit, Spaß und zügige Fahrmanöver <br>lassen Ihren Puls in die Höhe schnellen.</div>
				<div class="training-suggestions">
					<div id="M2T" class="training-1">Dynamik Training</div>
					<div id="SMT" class="training-2">Speed Training</div>
					<div class="training-3">Supermoto Training</div>
					<div id="MTT" class="training-4">Trial Training</div>
				</div>
			</div>
			
			<div id="antwort-b" class="antwort">
				<div class="antwort-headline">Sie sind der Analytiker.</div>
				<div class="antwort-subheadline itc-book"> Eine ruhige, sichere und vorausschauende<br>Fahrweise zeichnet Sie aus. </div>
				<div class="training-suggestions">
					<div id="M2T" class="training-1">Dynamik Training</div>
					<div class="training-2">Training und Ausfahrt</div>
					<div id="SMT" class="training-3">Speed Training</div>
					<div class="training-4">Personal Coaching</div>
				</div>
			</div>
			
			<div id="antwort-c" class="antwort">
				<div class="antwort-headline">Sie sind der Teamplayer.</div>
				<div class="antwort-subheadline itc-book">Beim Fahren vertrauen Sie auf Ihr Gefüh<br>und ihre Erfahrung.</div>
				<div class="training-suggestions">
					<div class="training-1">Enduro Training</div>
					<div class="training-2">Training & Ausfahrt</div>
					<div id="M2T" class="training-3">Dynamik Training</div>
					<div class="training-4">Personal Coaching</div>
				</div>
			</div>			
			</div>
			
			<div class="buchung reset">
				<div class="closer"></div>
				<div id="moto" class="final-training">Speed Training</div>
				<div class="open-form"><img src="img/page4/btn-jetzt-buchen.png" /></div>
				<div class="final-anrede"><?php print $anrede . " " . $vorname . " " . $nachname;?>, Ihr Gutscheincode lautet:</div>
				<div class="gutscheincode"><?php echo $gutscheincode;?></div>
				
				<div class="gutschein-wrapper">
					<img class="headline-keine-zeit" src="img/page4/headline-keine-zeit.png" />
					<img class="gutschein-back" src="img/page4/btn-gutschein.png" />
					<div class="gutschein-bestellen"></div>
				</div>
			</div>
