<!DOCTYPE html>

<?php

include 'include/data.php';

include 'php/vendor/Mobile_Detect.php';
$detect = new Mobile_Detect;
if( $detect->isMobile() && !$detect->isTablet() ){
    header('Location:mobile/');
}


?>

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Österreichische Fahrtechnik</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9">
	
	<!-- <link rel="stylesheet" href="css/vendor/queryLoader.css" type="text/css" /> -->

    <link rel="stylesheet" href="css/global.css"/>
    <link rel="stylesheet" href="css/slide1.css"/>
    <link rel="stylesheet" href="css/slide2.css"/>
    <link rel="stylesheet" href="css/slide3.css"/>
    <link rel="stylesheet" href="css/slide4.css"/>
	<link rel="stylesheet" href="css/slide5.css"/>
	<link rel="stylesheet" href="css/slide6.css"/>
	<link rel="stylesheet" href="css/slide6.css"/>
	<link rel="stylesheet" href="css/vendor/jquery.kyco.preloader.css"/>
	
	<script type="text/javascript" src="http://fast.fonts.net/jsapi/0c381e6a-25d3-4567-977d-f551785884e0.js"></script>
	
    <script src="js/vendor/jquery-2.1.0.min.js"></script> 
    <script src="js/vendor/jquery.touchSwipe.min.js"></script>
    <script src="js/vendor/jquery.carouFredSel-6.2.1.js"></script>
	<script src="js/vendor/jquery.kyco.preloader.js"></script>
	<script src="js/vendor/TweenMax.min.js"></script>
	<script src="js/vendor/modernizr.custom.89522.js"></script>

    <script src="js/global.js"></script>
    <script src="js/slide1.js"></script>
    <script src="js/slide2.js"></script>
    <script src="js/slide3.js"></script>
    <script src="js/slide4.js"></script>
    <script src="js/slide5.js"></script> 
    <script src="js/slide6.js"></script>
	
    <script> 
		
        $(document).ready(function() {
			start();
			$('body').kycoPreload();
           DIGIPAPER.init();
			
        });
    </script>

</head>
<body>


<div id="mainWrapper">


	
	<div  class="musik-main hidden">

	
	<audio class="test-audio">
	<source src="music/test.mp3">
	</audio>
	
</div>
	
    <div class="slidesWrapper">
    
        <div id="slide1" class="singleSlide">
					
		<div class="single-holder">
		
		<div class="hochformat">
						
			<img class="simageHolder" src="img/vorlage/page1.jpg" />
			
			<div class="page1top"><img src="img/page1/hoch-gelb-back.png" /></div>
					<div class="header-text1"> <?php print $anrede . " " . $vorname . " " . $nachname;?>, </div>
			
			<div class="header-subtext1"> Fahrprofis werden nicht geboren, sondern gemacht. </div>
			
			<div class="page1-background"><img src="img/page1/hintergrundbild-1.png" /></div>

		</div>
		
		</div>				

			
		</div> <!--ENDE Slide1 -->
		
		
        <div id="slide2" class="singleSlide">
        <div class="single-holder">
        
         <!-- <img class="simageHolder" src="img/vorlage/page2.jpg" />  -->
         
         <div class="page2top reset"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
			
			
			<div class="header-subtext2 reset"> Das war Ihr Training. </div>
			
			<div class="index-menu reset">
				<div class="page2-background"><img src="img/page2/hintergrund-2.png" /></div>
				
				<div class="link1"><a href="#slide6" class="caroufredsel"><img src="img/page2/fahrtechnik-2.png" /></a></div>
	
				<div class="link2"><a href="#slide4" class="caroufredsel"><img src="img/page2/trainingstyp.png" /></a></div>
				
				<div class="link3"><a href="#slide5" class="caroufredsel"><img src="img/page2/fotogalerie.png" /></a></div>
				
				<div class="link4"><a href="#slide3" class="caroufredsel"><img src="img/page2/produkte.png" /></a></div>
	
				<div class="text-link4">Die Produkte<br>der ÖAMTC<br>Fahrtechnik </div>
				
				<div class="text-link2">Welcher<br>Trainingstyp<br>sind Sie. </div>
				
				<div class="text-link3">Ihre<br>Fotogalerie </div>
	
				<div class="text-link1">Fahrtechnik<br>Zentren </div>
			
			</div>

<div class="video-wrapper reset">		
<img src="img/page2/btn-play.svg" id="play" class="video1" />	
	<video id="video1" poster="/videos/city.jpg" onended="videoend()" width="1024" height="668">
		<source src="/videos/city.mp4" type="video/mp4" />
		<source src="/videos/01_gruppenruf.webm" type="video/webm" />
		<source src="/videos/01_gruppenruf.ogv" type="video/ogg" />
	</video>
</div>

			
		</div>
		
		
			
        
         </div>  <!-- ENDE Slide2  -->
		 
		 <div id="slide3" class="singleSlide">
		 <div class="single-holder">
        
         <!--<img class="simageHolder" src="img/vorlage/page3.jpg" /> -->
         
         <div class="page2top"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
			<div class="header-text3"> Die Trainings</div>
			<div class="header-subtext3"> der ÖAMTC Fahrtechnik. </div>
			
			<div class="pkw-holder">
				<div class="pfeil1"><img src="img/page2/pfeil-gelb.png" /></div>
				<div class="pfeil1-text">PKW Training </div>
				<div class="btn1"><img src="img/page2/plus.png" /></div>
			</div>
			
			<div class="moto-holder">
				<div class="pfeil2"><img src="img/page2/pfeil-grau.png" /></div>
				<div class="pfeil2-text">Motorrad Training </div>
				<div class="btn2"><img src="img/page2/plus.png" /></div>
			</div>
			
			
			<div class="pkw"><img src="img/page3/CH_H12131.png" /></div>
			<div class="motorrad"><img src="img/page3/moto.jpg" /></div>

		</div>
        	
         
        
         </div>  <!-- ENDE Slide3  -->
		 
		 <div id="slide4" class="singleSlide">
        <div class="single-holder">
        
        <!--  <img class="simageHolder" src="img/vorlage/page4.jpg" />  -->
      
         
         <div class="page2top"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
			<div class="header-text4"> Welcher Trainingstyp sind Sie? </div>
			<div class="header-subtext4"> 3 Klicks zum Ergebnis. </div>
			<div class="subtext-gelb"> Wenn die Ampel auf Gelb steht... </div>
			<div class="subtext-parken"> Beim Einparken... </div>
			<div class="subtext-autobahn"> Auf der Autobahn... </div>


			<div class="btn-aktiv"><img src="img/page3/btn-voll.png" /></div>
			<div class="btn-leer"><img src="img/page3/btn-leer.png" /></div>
			<div class="btn-leer1"><img src="img/page3/btn-leer.png" /></div>
			<div class="btn1-text">steigen Sie noch aufs Gas. </div>
			<div class="btn2-text">bremsen Sie vorsichtshalber. </div>
			<div class="btn3-text">entscheiden Sie jedes Mal nach Gefühl. </div>
			<div class="btn-number-leer"><img src="img/page3/btn-number-leer.svg" /></div>
			<div class="btn-number-voll"><img src="img/page3/btn-number-voll.svg" /></div>
			<div class="btn-strich"><img src="img/page3/btn-strich.svg" /></div>
			
			<div class="btn1-textparken">macht Ihnen keiner was vor, Sie sind Spezialist. </div>
			<div class="btn2-textparken">lassen Sie sich Zeit und konzentrieren sich. </div>
			<div class="btn3-textparken">vertrauen Sie ganz Erfahrung und Gefühl. </div>
			
			<div class="btn1-textautobahn">stellt Überholen für Sie kein Problem dar. </div>
			<div class="btn2-textautobahn">bleiben Sie meistens auf der rechten Spur. </div>
			<div class="btn3-textautobahn">ist Ihr Fahrstil immer souverän. </div>

			
		
		
			<div class="header-text5">Fahrprofis werden nicht geboren, </div>
			<div class="header-subtext5">sondern gemacht. </div>
			<div class="subtext-5"> Sie sind ein Action-Liebhaber. </div>
			<div class="subtext-5a"> Geschwindigkeit, Spaß und zügige Fahrmanöver<br>sind ganz nach Ihrem Geschmack. </div>
			
			<div class="subtext-6"> Sie sind der Sicherheitsbewusste. </div>
			<div class="subtext-6a"> Eine ruhige, sichere und vorausschauende<br>Fahrweise zeichnet Sie aus. </div>

			<div class="subtext-7"> Sie sind der Sportlich-Dynamische. </div>
			<div class="subtext-7a"> Sie fahren sicher und souverän und passen<br>Ihren Fahrstil der Verkehrssituation an. </div>
						
			
			<div class="btn-rabatt"><img src="img/page5/rabatt.svg" /></div>

        
   </div>
        	
                 
         </div>  <!-- ENDE Slide4  -->
		 
		 <div id="slide5" class="singleSlide">
         <div class="single-holder">
     	
         <!-- <img class="simageHolder" src="img/vorlage/page5a.jpg" />  -->
         
         <div class="page2top"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
		
		<div id="<?php echo $bildercode;?>" class="foto-holder">
			
			<img src="http://oeamtc1.it-wms.com/archive/years/388991/2014-03-20-143340_388991.jpg" />
			
			
		</div>	
			
         
         </div>
        
         </div>  <!-- ENDE Slide5  -->
		 
		 
		 <div id="slide6" class="singleSlide">      
         	<div class="single-holder">
         
        		 <img class="simageHolder" src="img/vorlage/page6.jpg" /> 
                
				 <div class="page2top"><img src="img/page2/hoch-gelb-back2.png" /></div>
				 
				 <div class="zentrum one hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				  <div class="zentrum two hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum three hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum four hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum five hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum six hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum seven hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum eight hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum nine hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum ten hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum eleven hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum twelve hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum thirteen hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="zentrum fourteen hidden">
					 <div class="headline">Zentrum Marchtrenk</div>
					 <div class="bild"><img src="img/page6/zentrum-1.jpg" /></div>
					 <div class="text">
						Kontakt<br />
						Neufahrner Straße 100<br />
						4614 Marchtrenk<br />
						Telefon: (07243) 51 520 - 32400<br />
						Fax: (07243) 51 520 - 32420<br />
					 </div>
					 <a target="_blank" href="https://www.google.com/maps/place/Neufahrner+Stra%C3%9Fe+100/@48.2119573,14.1193078,722m/data=!3m1!1e3!4m2!3m1!1s0x477392558a912b9b:0x34634ae0dc3f1cd3" class="google-map"><img src="img/page6/google-maps.png" /></a>
				 </div>
				 
				 <div class="karte">
				 	<div id="one" class="flag"></div>
				 	<div id="two" class="flag"></div>
				 	<div id="three" class="flag"></div>
				 	<div id="four" class="flag"></div>
				 	<div id="five" class="flag"></div>
				 	<div id="six" class="flag"></div>
				 	<div id="seven" class="flag"></div>
				 	<div id="eight" class="flag"></div>
				 	<div id="nine" class="flag"></div>
				 	<div id="ten" class="flag"></div>
				 	<div id="eleven" class="flag"></div>
				 	<div id="twelve" class="flag"></div>
				 	<div id="thirteen" class="flag"></div>
				 	<div id="fourteen" class="flag"></div>
					 <img src="img/page6/karte.png" />
				 </div>
				 
				 
		         <div class="form-wrapper">
			         
		         </div>
      
        
			 </div>
         </div>  <!-- ENDE Slide6  -->
        
        
               
        
    </div>
    <div class="prevNavBtn"><img src="img/global/pfeil_links.svg" /></div>
    <div class="nextNavBtn"><img src="img/global/pfeil_rechts.svg" /></div>
    <ul id="menuNav">
    <li class="openThumbs"><img src="img/musterseite/icon_dialogschmiede.png" /></li>
        <li><img src="img/musterseite/icon_facebook.png" /></li>
        <li><img src="img/musterseite/icon_like.png" /></li>
        <li><img src="img/musterseite/icon_add.png" /></li>
    </ul>
</div>
<div id="navigationOverlay">
    <ul>
        <li><a href="#slide1" class="caroufredsel">1</a></li>
        <li><a href="#slide2" class="caroufredsel">2</a></li>
		<li><a href="#slide3" class="caroufredsel">3</a></li>
		<li><a href="#slide4" class="caroufredsel">4</a></li>
		<li><a href="#slide5" class="caroufredsel">5</a></li>
		<li><a href="#slide6" class="caroufredsel">6</a></li>
    </ul>
</div>


</body>
</html>