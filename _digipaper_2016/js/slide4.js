SLIDES.slide4 = function() {

    //private
    function onInit(){
    	
			
        TweenMax.killAll();
        $("#slide4 .reset").removeAttr('style').hide();
        
        setQuestionpos();
        
        $(".again").on("click", function () {
	        resetQuiz();
        })
        
        trainingTaken = $("#mainWrapper").attr("class")
        //console.log(trainingTaken);
        
        
        //hide trainings already taken
        if(trainingTaken != ""){
        	$("#antwort-a #"+trainingTaken).hide();
        	$("#antwort-b #"+trainingTaken).hide();
        	$("#antwort-c #"+trainingTaken).hide();
        }
        
        // open buchungs formular with correct selection
        $(".open-form").on("click",function () {
	        
	    ga('send', 'event', 'Formular', 'Button', trainingId);	    
	        
		switch (trainingId) {
			case "Drift Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=310');
			break;
			case "Schnee & Eis Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=128');
			break;
			case "Racing Experience" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=581');
			break;
			case "Personal Coaching" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=582');
			break;
			case "Dynamik Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=37');
					}else{
						window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=26');
					}
			break;
			case "Speed Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=38');
					}else{
						window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=578');
					}
			break;
			case "Supermoto Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=579');
			break;
			case "Trial Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=129');
			break;
			case "Training und Ausfahrt" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=88');
			break;
			case "Enduro Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=580');
			break;
			case "Aktiv Training" :
				if($(".final-training").attr("id") == "pkw"){
					window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=1029');
				}else{
					window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=3&courseType=28');
				}
			break;
			case "Snow & Fun Training" :
				window.open('https://www.oeamtc.at/fahrtechnik/training/booking?classification=2&courseType=289');
			break;
			case "Drive & Fly" :
				window.open('https://www.oeamtc.at/fahrtechnik/pkw/drive-fly/11.005.330');
			break;
		}	
	})
	
	     // open gutschein iframe seite with correct selection
        $(".gutschein-bestellen").on("click",function () {
	        
	    ga('send', 'event', 'Gutschein', 'Button', trainingId);
	        
		switch (trainingId) {
			case "Drift Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/drift');
			break;
			case "Schnee & Eis Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/schnee');
			break;
			case "Snow & Fun Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/schnee');
			break;
			case "Racing Experience" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Personal Coaching" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/coach');
			break;
			case "Dynamik Training" :
					window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/dynamik');
			break;
			case "Speed Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/speed');
			break;
			case "Supermoto Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Trial Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Training und Ausfahrt" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Enduro Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Aktiv Training" :
				if($(".final-training").attr("id") == "pkw"){
					window.open('https://www.fahrtechnik-gutschein.at/shopinshop/l/personalisierung/intensiv');
				}else{
					window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/aktiv');
				}
			break;
			
		}	
	})
		
       //start quiz js
       
      frage = 1;
      
      $(".weiter").on("click", function () {
      	  
      	  if($(this).parent().find(".active").length == 1){
	      	  	fragen = $(this).parent().parent().attr("id");
      	  
	      if(frage == 1) {
		      TweenMax.to($("#"+fragen+" .frage-1 .frage"), 0.2, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-1 .first"), 0.3, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-1 .second"), 0.4, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-1 .third"), 0.5, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-1 .weiter"), 0.6, {left: "-1300" ,delay:0});
		      
		      TweenMax.to($("#"+fragen+" .frage-2 .frage"), 0.2, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .frage").show()}});
		      TweenMax.to($("#"+fragen+" .frage-2 .first"), 0.3, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .first").show()}});
		      TweenMax.to($("#"+fragen+" .frage-2 .second"), 0.4, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .second").show()}});
		      TweenMax.to($("#"+fragen+" .frage-2 .third"), 0.5, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .third").show()}});
		      TweenMax.to($("#"+fragen+" .frage-2 .weiter"), 0.6, {left: "0" ,delay:0.5,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-2 .weiter").show()}});
		      frage = 2;
		      result1 = $(".frage-1").find(".active").attr("data");
		      $(".step1").removeClass("active");
		      $(".step2").addClass("active");
		      
	      }else if(frage == 2) {
	      	  TweenMax.to($("#"+fragen+" .frage-2 .frage"), 0.2, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-2 .first"), 0.3, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-2 .second"), 0.4, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-2 .third"), 0.5, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-2 .weiter"), 0.6, {left: "-1300" ,delay:0});
		      
		      TweenMax.to($("#"+fragen+" .frage-3 .frage"), 0.2, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .frage").show()}});
		      TweenMax.to($("#"+fragen+" .frage-3 .first"), 0.3, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .first").show()}});
		      TweenMax.to($("#"+fragen+" .frage-3 .second"), 0.4, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .second").show()}});
		      TweenMax.to($("#"+fragen+" .frage-3 .third"), 0.5, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .third").show()}});
		      TweenMax.to($("#"+fragen+" .frage-3 .weiter"), 0.6, {left: "0" ,delay:1,ease:Circ.easeIn,onStart: function() {$("#"+fragen+" .frage-3 .weiter").show()}});
		      result2 = $(".frage-2").find(".active").attr("data");
		      frage = 3;
		      $(".step2").removeClass("active");
		      $(".step3").addClass("active");
		      
	      }else {
		      TweenMax.to($("#"+fragen+" .frage-3 .frage"), 0.2, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-3 .first"), 0.3, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-3 .second"), 0.4, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-3 .third"), 0.5, {left: "-1300" ,delay:0});
		      TweenMax.to($("#"+fragen+" .frage-3 .weiter"), 0.6, {left: "-1300" ,delay:0});
		      result3 = $(".frage-3").find(".active").attr("data");
		      frage = 1;
		      $(".active").removeClass("active");
		      setTimeout(function() {
		      	$(".steps").hide();
				 resultsShow ();	 
			  }, 500);
					
	      }

      	  }
      })
       
        $(".fragen div").on("touchstart click",function () {
        
			if(!($(this).hasClass("frage"))) {
				
			
			$(this).parent().find(".active").each(function () {
				$(this).removeClass("active");
			});
			
			$(this).toggleClass("active");
			
			}
			
			})
			
			
			
			$(".steps div").on("click" , function () {
						
						thisstep = $(this).attr("class");
						//console.log(frage);
						switch(thisstep) {
							case "step1":
								  TweenMax.to($("#"+fragen+" .frage-1 .frage"), 0.2, {left: "0" ,delay:0.5});
							      TweenMax.to($("#"+fragen+" .frage-1 .first"), 0.3, {left: "0" ,delay:0.5});
							      TweenMax.to($("#"+fragen+" .frage-1 .second"), 0.4, {left: "0" ,delay:0.5});
							      TweenMax.to($("#"+fragen+" .frage-1 .third"), 0.5, {left: "0" ,delay:0.5});
							      TweenMax.to($("#"+fragen+" .frage-1 .weiter"), 0.6, {left: "0" ,delay:0.5});
								if(frage == 2 ){																      
								      TweenMax.to($("#"+fragen+" .frage-2 .frage"), 0.2, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-2 .first"), 0.3, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-2 .second"), 0.4, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-2 .third"), 0.5, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-2 .weiter"), 0.6, {left: "1000" ,delay:0,ease:Circ.easeIn});	
								      $(this).toggleClass("active");		
								      $(".active").removeClass("active");						     
								}else if(frage == 3){																	      
								      TweenMax.to($("#"+fragen+" .frage-3 .frage"), 0.2, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-3 .first"), 0.3, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-3 .second"), 0.4, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-3 .third"), 0.5, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      TweenMax.to($("#"+fragen+" .frage-3 .weiter"), 0.6, {left: "1000" ,delay:0,ease:Circ.easeIn});
								      $(this).toggleClass("active");	
								      $(".active").removeClass("active");
								      $(".frage-2 .reset").each(function() {
									      $(this).css({left:"1000px"});
								      })
								}
								frage = 1;	
								
							break;
							case "step2" :
								if(frage == 3) {
									TweenMax.to($("#"+fragen+" .frage-2 .frage"), 0.2, {left: "0" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-2 .first"), 0.3, {left: "0" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-2 .second"), 0.4, {left: "0" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-2 .third"), 0.5, {left: "0" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-2 .weiter"), 0.6, {left: "0" ,delay:0,ease:Circ.easeIn});	
								    
								    TweenMax.to($("#"+fragen+" .frage-3 .frage"), 0.2, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-3 .first"), 0.3, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-3 .second"), 0.4, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-3 .third"), 0.5, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    TweenMax.to($("#"+fragen+" .frage-3 .weiter"), 0.6, {left: "1000" ,delay:0,ease:Circ.easeIn});
								    $(this).toggleClass("active");		
								    $(".active").removeClass("active");	
								    frage = 2;					     

								}
							break;
							
						}
						
						
						
				
			})
			
			//open buchungs overlay & abfrage welche bestell möglichkeiten es gibt
			
			$(".training-suggestions div").on("click", function () {
				trainingId = $(this).text();
				
				$(".buchung, .headline-keine-zeit, .gutschein-back,.gutschein-bestellen").show();
				$(".buchung").css({height:"439px"});
				
				$(".final-training").text(trainingId);
				
				if(trainingId == "Personal Coaching") {
					$(".open-form").hide();
					$(".headline-keine-zeit").attr("src","img/page5/headline-jetzt.png");
				}else if(trainingId == "Racing Experience") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen, .gutschein-bestellen").hide();
				}else if(trainingId == "Supermoto Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Enduro Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Trial Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Training und Ausfahrt") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
					
					//$(".open-form").hide();
					//$(".headline-keine-zeit").attr("src","img/page5/headline-jetzt.png");
				}else if(trainingId == "Drive & Fly") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();	
				}else{
					$(".headline-keine-zeit").attr("src","img/page5/headline-keine-zeit.png");
					
				}
				
			})
			
			// Close  Gutschein
			$(".closer").on("click", function () {
				$('.buchung').hide();
				$(".open-form, .headline-keine-zeit").removeAttr("style");
			})
    }

    function onEnter() {
        
    }

    function onExit() {
        TweenMax.killAll();
        //$("#slide5 .reset").removeAttr('style');
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();


function resultsShow () {
	total = new Array();
	 	total = [];
	 	total[0] = result1;
	 	total[1] = result2;
	 	total[2] = result3;
	 	
	 	blau = 0;
	 	lila = 0;
	 	gruen = 0;
	 	
	 	for(var i = 0; i < total.length; ++i){
		 	if(total[i] == "blau") { 
		 		blau ++;
		 	} else if (total[i] == "gruen") {
			 	gruen ++;
		 	} else if (total[i] == "lila") {
			 	lila ++; 
		 	}
		 }


         //TweenMax.to($(".filme-text"), 0.5, {top:490, delay:3});
         
         TweenMax.to($(".again"), 0.5, {top:140, delay:6, ease:Bounce.easeInOut});
		 
		 
		 if (blau > 1 ) {
		 	 $("#antwort-a").fadeIn();
		 	 

		 	 } else if (gruen > 1) {
		 	
		 	 $("#antwort-b").fadeIn();
			
			 
			 
		 } else if (lila > 1) {
		 	 
		 	 $("#antwort-c").fadeIn();
		 	 
		 	 		 	 
		 } else {
		 	 
		 	$("#antwort-b").fadeIn();
		 	
		 }
}


function setQuestionpos() {
	$(".frage-2 .reset").each(function () {
				$(this).css({left:"1000px"})
		})
			
		$(".frage-3 .reset").each(function () {
			$(this).css({left:"1000px"})
		})

}

function resetQuiz() {
	//console.log("in");
	
	$(".antwort").each(function(){
		$(this).removeAttr("style");
	})
	
	$(".fragen div").each(function(){
		$(this).removeAttr("style");
	})
	
	$(".fragen .active").each(function(){
    	 $(this).removeClass("active")
	 })
	
	$("#first").removeAttr("style");
	
	setQuestionpos();
	
	TweenMax.to($(".again"), 0.5, {top:-440, delay:0, ease:Bounce.easeInOut});
	
}

