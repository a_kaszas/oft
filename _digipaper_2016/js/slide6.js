SLIDES.slide6 = function() {

    //private
    function onInit(){

    	baseurl = $(".slidesWrapper").attr("id");
    	
        fotoid = $(".foto-holder").attr("id");
        
        /*$('.kundenportalImg').click(function(){
			$(this).parents().find(".large_img").print();
			/* var mywindow = window.open('', '', 'height=400,width=600');
		    mywindow.document.write('<img src="'+$(this).parents().find(".large_img img").attr('src')+'">');
	        window.onload = function() {
                    mywindow.print();
                    setTimeout(function() {
                        mywindow.close();
                    }, 1000);
                };
	 	});
	 	*/
	 	
	 	$(".emailImg").on("click",function () {
		 	shareimg = $(this).parents().find(".large_img img").attr("src");
		 	//console.log(shareimg);
		 	$(".emailImg").attr("href","mailto:?subject="+$('#slide6 .subheadline').html()+"&body="+baseurl+encodeURIComponent(shareimg));
		 	
	 	})
	 	
	 	$(".facebookImg").on("click", function (){
	 		//event.preventDefault();
		 	shareimg = $(this).parents().find(".large_img img").attr("src");
		 	//console.log(baseurl+shareimg);
		 	imglink = baseurl+shareimg;
			facebook_link = 'https://www.facebook.com/sharer/sharer.php?u='+imglink+'&t='+$('.subheadline').html();
		    window.open(facebook_link,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=650,height=600');
	 	})


		var initMatrix = function(){
		
			Options= new Array ({
				Width: 1024,
				InnerWrapper: $('.matrix_inner_wraper'),
				ContainerLength: $('.matrix_inner_wraper .matrix_container').length,
				Draggalbe:$('.matrix_draggalbe'),
				Scroller:$('.matrix_scroller'),
				Items: $('.matrix_items'),
				Overlay: $('.matrix_layer_wrapper'),
				OverlayImg: $('.content_right .large_img'),
				BtLeft: $('.matrix_controller .bt_left'),
				BtRight: $('.matrix_controller .bt_right'),
				BtLeftOverlay: $('.bt_leftOverlay'),
				BtRightOverlay: $('.bt_rightOverlay'),
				DownloadImg: $('.downloadImg'),
				
				
				
				closeOverlay: $('.closeOverlay'),
				current: 0,
			});
		
			var m = Options[0];
			
			m.InnerWrapper.width(m.Width*m.ContainerLength);
			
			m.BtLeft.click(function(){
				if(m.current==0){
					return false;
				}else{
					$(this).hide();
					$(".block-btns").show();
					move('right');
					moveDragg('left');
				}
				setTimeout(function(){ $(".bt_left").show(); }, 1000);
			});
			
			m.BtRight.click(function(){

				if(m.current==m.ContainerLength-1){
					return false;
				}else{
					$(this).hide();
					$(".block-btns").show();
					move('left');
					moveDragg('right');
				}
				setTimeout(function(){ $(".bt_right").show(); }, 1000);
			});
			
			m.BtLeftOverlay.click(function(){
				if($('.matrix_items.current').prev().length){
					img = $('.matrix_items.current').prev().addClass('current_tmp').attr('data-img');
					m.Items.removeClass('current');
					$('.matrix_items.current_tmp').removeClass('current_tmp').addClass('current');
					m.OverlayImg.html('<img src="'+img+'" />');	
					setDownload();
				}else{
					closeLayer();
				}
			});
			
			m.BtRightOverlay.click(function(){
				console.log("in")
				if($('.matrix_items.current').next().length){
					img = $('.matrix_items.current').next().addClass('current_tmp').attr('data-img');
					m.Items.removeClass('current');
					$('.matrix_items.current_tmp').removeClass('current_tmp').addClass('current');
					m.OverlayImg.html('<img src="'+img+'" />');	
					setDownload();
				}else{
					closeLayer();
				}
			});
		
			
		m.Items.click(function(e){
			e.preventDefault();
			openLayer($(this).attr('data-img'));
			$(this).addClass('current');
			setDownload();
		});
		
		m.closeOverlay.click(function(e){
			e.preventDefault();
			closeLayer($(this).attr('data-img'));
			$('.nextNavBtn, .prevNavBtn').css('opacity', '1');
		});
		
		openLayer = function(img){
			m.OverlayImg.html('<img src="'+img+'" />');
			m.Overlay.show();
			$('.nextNavBtn, .prevNavBtn').css('opacity', '0.4');
		};
		
		closeLayer = function(){
			m.Overlay.hide();
			m.Items.removeClass('current');
			$('.nextNavBtn, .prevNavBtn').css('opacity', '1');
		};
		setDownload = function(){
			m.DownloadImg.attr('href',$('.matrix_items.current').attr('data-img'));
		}
		
		// Scroll left and right
			/*$('.matrix_scroller').mousemove(function(ev){
				var wrapperOffset = ev.offsetX;
				$('.matrix_draggalbe').mousedown(function(e){
					mOffset = e.offsetX;
					console.log(mOffset);
					if( mOffset > parseInt($('.matrix_draggalbe').position().left)){
						moveDragg('right');
						//move('right');
					}else {
						//moveDragg('left');
						//move('right');
					}
				});
				
			}); 
			
			*//*
			$(function() {
			  new Dragdealer('simple-slider', {
				  callback: function(x) {
			
				    if(x >= 0) {
					    //moveDragg('right');
					    move('left');
				    } else {
					    //moveDragg('left');
					    //move('right');
				    }
				    
				  },
				  steps: $('.matrix_container').length,
				  vertical: false
				});
			}) */
					
		    
			m.Draggalbe.width(parseInt(m.Scroller.width()) / (m.ContainerLength));
			
			
			move = function(mDirection){
				m.InnerWrapper.animate({
				    left: mDirection=='left' ? parseInt(m.InnerWrapper.css('left'))-m.Width : parseInt(m.InnerWrapper.css('left'))+m.Width,
				  }, 1000, function() {
				    $(".block-btns").hide();
				});
			}
			
			moveDragg = function(mDirection){
				m.Draggalbe.animate({
					left: mDirection=='left' ?  parseInt(m.Draggalbe.css('left'))-parseInt(m.Scroller.width()) / (m.ContainerLength) : parseInt(m.Draggalbe.css('left'))+parseInt(m.Scroller.width()) / (m.ContainerLength)}, 1000, function() {
				    mDirection=='left'? m.current--: m.current++;
				});
			};
			return true;	
		};
		
		initMatrix();
        
    }

    function onEnter() {
       
    }

    function onExit() {
       
    }

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();