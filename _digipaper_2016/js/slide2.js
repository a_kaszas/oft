SLIDES.slide2 = function() {

    //private
    function onInit(){
    	$('#play').hide();
    	
    	 $("#slide2 .reset").removeAttr('style');
    
    	header = $(".header-subtext2");
    	menuBox = $(".index-menu");
    	videoWrapper = $(".video-wrapper");
    	topB = $(".page2top");
        
        
        $(".page2-background").on("click", function () {
	      	pos = $(".index-menu").css("marginLeft");
	      	//console.log(pos); 
	      	$('video').get(0).pause();
	      	
	      	if(pos == "741px") {
		      	 TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
	      	}else{
		      	TweenMax.to($(".index-menu"), 0, {marginLeft: "741" ,delay:0, ease:Elastic.easeIn});
	      	}
	      	
        })
        
        menuBox.find("div").on("click", function () {
	        current_slide = $(this).attr("class");
        	switch (current_slide) {
		        case "link1" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 1);
		        break;
		        case "link2" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 2);
		        break;
		        case "link3" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 3);
		        break;
		        case "link4" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 4);
		        break;
		        case "link5" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 5);
		        break;
		        case "text-link1" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 1);
		        break;
		        case "text-link2" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 2);
		        break;
		        case "text-link3" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 3);
		        break;
		        case "text-link4" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 4);
		        break;
		        case "text-link5" :
		        $('#mainWrapper').find(".slidesWrapper").trigger("next", 5);
		        break;
	        }
        })
         
         $("video").on("click", function () {
	        $(this).get(0).play(); 
	        $('#play').hide();   
        })

		var checkTime = setInterval(function(){ 
		    if($('video').get(0).currentTime > 0 && $('video').get(0).ended!=true && $('video').get(0).paused != true) {
		       //console.log($('video').get(0).currentTime);
		       $('#play').hide();
		       TweenMax.to($(".header-subtext2"), 1, {marginLeft: "-380" ,delay:0, ease:Elastic.easeOut});
			   TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
		    } else {
				videoend();
			}
		   
	    }, 3000);  
    
	          
        $("#play").on("click", function () {
	        $('video').get(0).play();
	        //videoId = $(this).attr("class");
	        //console.log(videoId);
	        //$(this).parent().find("video").get(0).play();
	        //$(this).hide();
	        //TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
			//TweenMax.to($(".header-subtext2"), 1, {marginLeft: "-380" ,delay:0, ease:Elastic.easeOut});

        })
        
        
/*
        sublime.ready(function(){
			var player = sublime.player('59de3905');
			
			sublime.player('59de3905').on({
			play: function(player) { 
				TweenMax.to($(".index-menu"), 1, {marginLeft: "1000" ,delay:0, ease:Elastic.easeOut});
				TweenMax.to($(".header-subtext2"), 1, {marginLeft: "-380" ,delay:0, ease:Elastic.easeOut});
				
				 },
			end:   function(player) { 
				
				
				
				
			}
		});
		
		
		});
*/
		
        
    }

    function onEnter() {
	    TweenMax.to($(".index-menu"), 0, {marginLeft: "741" ,delay:0, ease:Elastic.easeIn});
	     TweenMax.to($(".header-subtext2"), 1, {marginLeft: "0" ,delay:0, ease:Elastic.easeIn});
        //TweenMax.from(header, 2, {left: "-400" ,delay:1, ease:Bounce.Elastic,onStart: function() {header.show()}});
        //TweenMax.from(menuBox, 1, {marginLeft: "1000" ,delay:2, ease:Bounce.Elastic,onStart: function() {menuBox.show()}});
        //TweenMax.from(videoWrapper, 3, {opacity: "0" ,delay:1, ease:Bounce.Elastic,onStart: function() {videoWrapper.show()}});
        //TweenMax.from(topB, 1, {top: "-292" ,delay:1, ease:Bounce.Elastic,onStart: function() {topB.show()}});
    }

    function onExit() {
    	 //$('#play').show();
    	 //console.log(videoId);
    	 //$("video").get(0).pause(); 
    	 //var player = sublime.player('59de3905');
    	 //player.pause();
    	 
       TweenMax.killAll();
        $("#slide2 .reset").removeAttr('style');
        //clearInterval(checkTime);
    }

	

    //public
    return {
        onInit: onInit,
        onEnter: onEnter,
        onExit: onExit
    }

}();

function videoend () {
		 $('#play').show();
	     
	     TweenMax.to($(".index-menu"), 0, {marginLeft: "741" ,delay:0, ease:Elastic.easeIn});
	     //TweenMax.to($(".header-subtext2"), 1, {marginLeft: "0" ,delay:0, ease:Elastic.easeIn});
	}