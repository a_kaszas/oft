var DIGIPAPERMOBILE = function() {

    function init() {
    	
    	    	
    	$(".click-here").on("click", function () {
	    	$(".cover").fadeOut();
    	})

        //eventhandler
        $(".singleSlide .toggle").on("click", function() {
            var $parent = $(this).parents(".singleSlide");
           
            $("video").get(0).pause();
           
            if(!$parent.hasClass("open")) {
                closeSlide($(".singleSlide.open"));
                openSlide($parent);

            } else {
                closeSlide($parent);
            }
        });
        
		//$("#slide7 .panel").show();
		        
        initTrainings ();
        initTrainingstype ();
        startSlideshow ();
        initZentren ();
        


        // if hash in URL then jump to slide
        var hash = window.location.hash;
        UTIL.elementExists($(hash)) && $(hash).find(".toggle").trigger("click");

    }

    function openSlide($slide) {
        $slide.addClass("open").find(".panel").slideDown(300, function() {
            adjustViewport($slide);
        })
    }

    function closeSlide($slide) {
        $slide.removeClass("open").find(".panel").slideUp(100);
    }

    function adjustViewport($slide) {
        $('html, body').animate({scrollTop: $slide.offset().top}, 'slow');
    }

    //public
    return {
        init: init
    }

}();

var UTIL = {
    elementExists: function($element){
        return $element.length;
    }
};

function initTrainingstype () {
	$(".frage-2, .frage-3").hide();
				
    $(".fragen div").on("touchstart click",function () {

	if(!($(this).hasClass("frage"))) {
		
	
	$(this).parent().find(".active").each(function () {
		$(this).removeClass("active");
	});
	
	$(this).toggleClass("active");
	
	}
	
	})
	
	     //start quiz js
       
      frage = 1;
      
      $(".weiter").on("click", function () {
      	  
      	  if($(this).parent().find(".active").length == 1){
	      	  	fragen = $(this).parent().parent().attr("id");
      	  
	      if(frage == 1) {
	      		
	      	  $(".frage-1").hide();
		      $(".frage-2").show();
		      frage = 2;
		      result1 = $(".frage-1").find(".active").attr("data");
		      $(".step1").removeClass("active");
		      $(".step2").addClass("active");
		      
	      }else if(frage == 2) {
	      	  $(".frage-2").hide();
		      $(".frage-3").show();
	      	  result2 = $(".frage-2").find(".active").attr("data");
		      frage = 3;
		      $(".step2").removeClass("active");
		      $(".step3").addClass("active");
		      
	      }else {
	      	  $(".frage-3").hide();
		      result3 = $(".frage-3").find(".active").attr("data");
		      frage = 1;
		      $(".steps").hide();
		      setTimeout(function() {
				 resultsShow ();	 
			  }, 500);
					
	      }

      	  }
      })
      
      //open buchungs overlay & abfrage welche bestell möglichkeiten es gibt
			
			$(".training-suggestions div").on("click", function () {
				trainingId = $(this).text();
				$(".buchung, .headline-keine-zeit, .gutschein-back,.gutschein-bestellen").show();
				$(".buchung").show();
				
				$(".final-training").text(trainingId);
				
				if(trainingId == "Personal Coaching") {
					$(".open-form").hide();
					$(".headline-keine-zeit").attr("src","img/headline-jetzt.png");
				}else if(trainingId == "Racing Experience") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen, .gutschein-bestellen").hide();
				}else if(trainingId == "Supermoto Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Enduro Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Trial Training") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
				}else if(trainingId == "Training und Ausfahrt") {
					$(".buchung").css({height:"286px"});
					$(".headline-keine-zeit, .gutschein-back, .gutschein-bestellen").hide();
					
					
				}else{
					$(".headline-keine-zeit").attr("src","img/headline-keine-zeit.png");
					
				}

				
			})

			// Close  Gutschein
			$(".closer").on("click", function () {
				$('.buchung').hide();
				$(".open-form, .headline-keine-zeit").removeAttr("style");
				console.log('ss');
			})
			
			
			 trainingTaken = $("#mainWrapper").attr("class")
        //console.log(trainingTaken);
        
        $("#"+trainingTaken).hide();
        
        // open buchungs formular with correct selection
        $(".open-form").on("click",function () {
	        ga('send', 'event', 'Formular', 'Button', trainingId);
		switch (trainingId) {
			case "Drift Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=310&preOrt=21');
			break;
			case "Schnee & Eis Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=128&preOrt=21');
			break;
			case "Racing Experience" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=581&preOrt=21');
			break;
			case "Personal Coaching" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=582&preOrt=21');
			break;
			case "Dynamik Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=37&preOrt=21');
					}else{
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=26&preOrt=21');
					}
			break;
			case "Speed Training" :
				if($(".final-training").attr("id") == "pkw"){
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=38&preOrt=21');
					}else{
						window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=38&preOrt=21');
					}
			break;
			case "Supermoto Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=579&preOrt=21');
			break;
			case "Trial Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=129&preOrt=21');
			break;
			case "Training und Ausfahrt" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=88&preOrt=21');
			break;
			case "Enduro Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=580&preOrt=21');
			break;
			case "Aktiv Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=28&preOrt=21');
			break;
			case "Speed Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=38&preOrt=21');
			break;
			case "Snow & Fun Training" :
				window.open('https://checkin.oeamtc.at/checkin/checkin.htm?anonymous=true&preTyp=289&preOrt=21');
			break;
			
		}	
	})
	
	     // open gutschein iframe seite with correct selection
        $(".gutschein-bestellen").on("click",function () {
	        ga('send', 'event', 'Gutschein', 'Button', trainingId);
		switch (trainingId) {
			case "Drift Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/drift');
			break;
			case "Schnee & Eis Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/schnee');
			break;
			case "Snow & Fun Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/schnee');
			break;
			case "Racing Experience" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Personal Coaching" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/coach');
			break;
			case "Dynamik Training" :
					window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/dynamik');
			break;
			case "Speed Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/speed');
			break;
			case "Supermoto Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Trial Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Training und Ausfahrt" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgg');
			break;
			case "Enduro Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/wertgutschein');
			break;
			case "Aktiv Training" :
				window.open('https://fahrtechnik-gutschein.at/shopinshop/l/personalisierung/aktiv');
			break;
			
		}	
	})
	
	
	
	//steps clicks 
	
	$(".steps div").on("click" , function () {
						
						thisstep = $(this).attr("class");
						//console.log(frage);
						switch(thisstep) {
							case "step1":
								   $(".frage-1").show();
								if(frage == 2 ){																      
								      $(".frage-2").hide();	
								      $(this).toggleClass("active");		
								      $(".step2").removeClass("active");						     
								}else if(frage == 3){																	      
								      $(".frage-3").hide();								      
								      $(this).toggleClass("active");	
								      $(".step3").removeClass("active");
								     
								}
								frage = 1;	
								
							break;
							case "step2" :
								if(frage == 3) {
									 $(".frage-2").show();
								    
								     $(".frage-3").hide();
								    $(this).toggleClass("active");		
								    $(".step3").removeClass("active");	
								    frage = 2;					     

								}
							break;
							
						}
						
						
						
				
			})




}

function initTrainings () {
	training = "off";
	
	$(".pkw-holder").on("click", function () {
		if(training == ".moto") {
			$(".pkw-holder").animate({marginTop:"-20px"});
			$(".moto-holder").animate({marginTop:"-60px"});
			$(".training.auto").animate({marginLeft:"-10px"})
			$(".training.moto").animate({marginLeft:"-400px"})
			training = ".auto";
		}else{
			$(".pkw-holder").animate({marginTop:"-20px"});
			$(".moto-holder").animate({marginTop:"-60px"});
			$(".training.auto").animate({marginLeft:"-10px"})
			training = ".auto";
		}
		
	})
	
	
	
	$(".moto-holder").on("click", function () {
		if(training == ".auto") {
			$(".moto-holder").animate({marginTop:"-20px"});
			$(".pkw-holder").animate({marginTop:"-60px"});
			$(".training.moto").animate({marginLeft:"-10px"})
			$(".training.auto").animate({marginLeft:"-400px"})
			training = ".moto";
		}else{
			$(".pkw-holder").animate({marginTop:"-60px"});
			$(".moto-holder").animate({marginTop:"-20px"});
			$(".training.auto").animate({marginLeft:"-400px"})
			$(".training.moto").animate({marginLeft:"-10px"})
			
			training = ".moto";
		}
		
	})
	
	 $(".next-training").on("click", function () {
	        $(training+" .training-holder.active").removeClass("active").next().addClass("active");
	        $(training+" .trainings-container.active").animate({marginLeft:"-=310px"}, function () {
		        checkSlider ();
	        })
        })
        
         $(".prev-training").on("click", function () {
	          $(training+" .training-holder.active").removeClass("active").prev().addClass("active");
			  $(training+" .trainings-container.active").animate({marginLeft:"+=310px"}, function () {
				  checkSlider ();
			  })
        })
        
        $(".training-menu div").on("click", function () {
        
        	$(training+" .training-holder.active").each(function () {
				$(this).removeClass("active");
			})
        
        	category = $(this).attr("class");
        	
        	if(category == "community") {
	        	$(training+" .training-scroll").addClass("hidden");
        	}else{
	        	$(training+" .training-scroll").removeClass("hidden");
        	}
        	
        	$(training+" .trainings-container.active").removeAttr('style').removeClass("active").addClass("hidden");
        	
        	$(training+" .trainings-container."+category).addClass("active").removeClass("hidden").find(".training-holder").first().addClass("active");
			
	        $(this).parent().find(".active").each(function () {
				$(this).removeClass("active");
			});
			
			$(this).toggleClass("active");
			
			checkSlider ();
			
		})
		
	
}

function checkSlider () {
	getActive = $(training+' .training-holder.active').index();
	total = $(training+' .trainings-container.active .training-holder').length;
	
	active = getActive + 1;
	
	if(getActive == 0) {
		$(training+" .prev-training").addClass("hidden");
	}else{
		$(training+" .prev-training").removeClass("hidden");
	}
	
	if(active == total) {
		$(training+" .next-training").addClass("hidden");
	}else {
		$(training+ " .next-training").removeClass("hidden");
	}
	
}

function resultsShow () {
	total = new Array();
	 	total = [];
	 	total[0] = result1;
	 	total[1] = result2;
	 	total[2] = result3;
	 	
	 	blau = 0;
	 	lila = 0;
	 	gruen = 0;
	 	
	 	for(var i = 0; i < total.length; ++i){
		 	if(total[i] == "blau") { 
		 		blau ++;
		 	} else if (total[i] == "gruen") {
			 	gruen ++;
		 	} else if (total[i] == "lila") {
			 	lila ++; 
		 	}
		 }		 
		 if (blau > 1 ) {
		 	 $("#antwort-a").fadeIn();
		 	 

		 	 } else if (gruen > 1) {
		 	
		 	 $("#antwort-b").fadeIn();
			
			 
			 
		 } else if (lila > 1) {
		 	 
		 	 $("#antwort-c").fadeIn();
		 	 
		 	 		 	 
		 } else {
		 	 
		 	$("#antwort-b").fadeIn();
		 	
		 }
}

function startSlideshow () {
	
	$(".slideshow-holder").load("/mobile/php/slide-6.php", function () {
		//setTimeout(function() {
		$('.slider').glide({
        	autoplay: "8000",
			arrows: false,
			navigation: false
		});
		
		$("#owl-example").owlCarousel({
			navigation:true,
			navigationText: [
	      "<img src='../img/global/pfeil_links.svg'>",
	      "<img src='../img/global/pfeil_rechts.svg'>"
	      ],
		});
			
			
			$(".download").on("click", function () {
				currentImg = $(this).parent().parent().find("img").attr("src");
				//console.log(currentImg);
				//window.open(currentImg);
				$(this).find('a').attr('href',currentImg);
			})
			
			$("#slide6 .email").on("click", function () {
				currentImg = $(this).parent().parent().find(".slider-img").attr("src");
				//console.log(currentImg);
				//window.open("mailto:?body="+currentImg);
				window.location='mailto:?subject='+$('#slide6 .subheadline').html()+'&body='+encodeURIComponent(currentImg); return false;
			})
			
			baseurl = $(".cover").attr("id");
    	
			$(".facebook").on("click", function (){
		 	shareimg = $(this).parents().find(".slide img").attr("src");
		 	//console.log(baseurl+shareimg);
		 	imglink = baseurl+shareimg;
			facebook_link = 'https://www.facebook.com/sharer/sharer.php?u='+imglink+'&t='+$('#slide6 .headline').html();
		    window.open(facebook_link,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=650,height=600');
		 	
		 	/*FB.ui({
			 	method: 'feed',
			 	href : baseurl,
			 	picture: imglink,
			 	
			}, function(response){});*/
	 	})
	 	
	 	
			
		//}, 15000);
		
		 //open Overlay image
        $(".slider-img").on("click", function() {
	
	       $('.'+$(this).attr('data-img')).show();
	      
			console.log($(this).attr('data-img'));
        });
        
        // close Overlay image
		$(".closeOverlaymobile").on("click", function() {
			$(this).parent('.slider-img-big').hide();
        });
	})
	
}

function initZentren() {
	$(".zentren-select li").on("click", function () {
		zentrum = $(this).attr("id");
		
		$("." + zentrum).removeClass("hidden");
		//console.log(zentrum);	
	})
	
	$(".zentrum .closer").on("click", function () {
		$(this).parent().addClass("hidden");
	})
	
}

