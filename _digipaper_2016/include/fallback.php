<!DOCTYPE html>

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Österreichische Fahrtechnik</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9">
    
    <link rel="stylesheet" href="../css/fallback.css"/>
   	
  
</head>
<body>

<div id="mainWrapper">
	
	<div class="page2top reset"><img src="/img/page2/hoch-gelb-back2.png" style="opacity: 1;"></div>
	
	<div class="browser-text">
		Diese Webseite wird von Ihrer Browser-Version nicht optimal unterstützt.<br />
		Bitte steigen Sie auf einen der folgenden Browser um:
	</div>
	
	<div class="browsers">
		
			<a target="_blank" href="http://windows.microsoft.com/de-at/internet-explorer/download-ie" class="ie">
				<img src="/img/ielogo1.png" />
			</a>
		
			<a target="_blank" href="http://www.mozilla.org/en-US/firefox/all/" class="firefox">
				<img src="/img/firefoxlogo.png" />
			</a>
			
			<a target="_blank" href="https://www.google.com/intl/de/chrome/webstore/features.html" class="chrome">
				<img src="/img/chromelogo.png" />
			</a>
			
			<a target="_blank" href="http://support.apple.com/de_DE/downloads/#safari" class="safari">
				<img src="/img/safarilogo.png" />
			</a>

		
		
		
		
	</div>

</div>


</body>
</html>