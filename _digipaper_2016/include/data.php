<?php 
	//establish db connection
	header("Content-Type: text/html; charset=utf-8");
	$con=mysqli_connect("localhost","db1181020-socke","Socke24!","db1181020-socke");
	$tabelle = "v_txt_digipaper";
	
	if (mysqli_connect_errno())
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

	//get id from URL for user
	;
	$url = $_SESSION['id'];
	//get user info from DB
	
	//echo $url;
	
	$userinfo = mysqli_query($con,"SELECT PERS_ANREDE, PERS_VORNAME, PERS_NACHNAME, KURS_KURZ, FOTOCODE FROM v_txt_digipaper WHERE ANM_HASH = '$url'");

	while($row = mysqli_fetch_array($userinfo))
  {
	  
	  $anrede = mysqli_real_escape_string($con, $row['PERS_ANREDE']); 
	  $vorname = mysqli_real_escape_string($con, $row['PERS_VORNAME']);  
	  $nachname = mysqli_real_escape_string($con, $row['PERS_NACHNAME']);   
	  $trainincode = mysqli_real_escape_string($con, $row['KURS_KURZ']); 
	  $bildercode = mysqli_real_escape_string($con, $row['FOTOCODE']); 
	  
	  $nachname = utf8_encode($nachname);
	 
	  $_SESSION['bildercode'] = $bildercode;

	  
	  if($anrede == "Sehr geehrter Herr") {
		  $anrede = "Lieber";
	  }	else {
		   $anrede = "Liebe";
	  }
	  
	  $coverThumb = "img/global/_0005_1.jpg";
	  $selektorThumb = "img/global/_0002_4.png";
	  
	   
	  if($trainincode == "HST"){
		  $trainincode = "SMT";
	  }

	  
	  switch($trainincode) {
		  case "PIT" :
		  $traininglong = "Ihrem Aktiv Training";
		  $videoThumb = "img/global/pkw-aktiv.jpg";
		  break;
		  case "PIN" :
		  $traininglong = "Ihrem Intensiv Training";
		  $videoThumb = "img/global/pkw-aktiv.jpg";
		  break;
		  case "P3T" :
		  $traininglong = "Ihrem Speed Training";
		  $videoThumb = "img/global/pkw-aktiv.jpg";
		  break;
		  case "P2T" :
		  $traininglong = "Ihrem Dynamik Training";
		  $videoThumb = "img/global/pkw-dynamik.jpg";
		  break;
		  case "DR1" :
		  $traininglong = "Ihrem Drift Training";
		  $videoThumb = "img/global/pkw-drift.jpg";
		  break;
		  case "PMF" :
		  $traininglong = "Ihrem Mehrphasen Training";
		  $videoThumb = "img/global/pkw-mehrphasen.jpg";
		  break;
		  case "OF1" :
		  $traininglong = "Ihrem Offroad Training";
		  $videoThumb = "img/global/offroad.jpg";
		  $coverThumb = "img/global/offroad-cover.jpg";
		  $selektorThumb = "img/global/offroad-selektor.jpg";
		  break;
		  case "PRE" :
		  $traininglong = "Ihrer Racing Experience";
		  $videoThumb = "img/global/pkw-racing.jpg";
		  break;
		  case "SET" :
		  $traininglong = "Ihrem Schnee & Eis Training";
		  $videoThumb = "img/global/pkw-schnee.jpg";
		  break;
		  case "SFN" :
		  $traininglong = "Ihrem Snow & Fun Training";
		  $videoThumb = "img/global/pkw-schnee.jpg";
		  break;
		  case "SF1" :
		  $traininglong = "Ihrem Snow & Fun Training";
		  $videoThumb = "img/global/pkw-schnee.jpg";
		  break;
		  case "SF2" :
		  $traininglong = "Ihrem Snow & Fun Training";
		  $videoThumb = "img/global/pkw-schnee.jpg";
		  break;
		  case "SUV" :
		  $traininglong = "Ihrem SUV Training";
		  $videoThumb = "img/global/pkw-suv.jpg";
		  break;
		  case "MPX" :
		  $traininglong = "Ihrem Motorrad 125er Training";
		  $videoThumb = "img/global/moto-125.jpg";
		  $coverThumb = "img/global/moto-cover.jpg";
		  $selektorThumb = "img/global/moto-selektor.jpg";
		  break;
		  case "MIT" :
		  $traininglong = "Ihrem Aktiv Training";
		  $videoThumb = "img/global/moto-aktiv.jpg";
		  $coverThumb = "img/global/moto-cover.jpg";
		  $selektorThumb = "img/global/moto-selektor.jpg";
		  break;
		  case "M2T" :
		  $traininglong = "Ihrem Dynamik Training";
		  $videoThumb = "img/global/moto-dynamik.jpg";
		  $coverThumb = "img/global/moto-cover.jpg";
		  $selektorThumb = "img/global/moto-selektor.jpg";
		  break;
		  case "MMF" :
		  $traininglong = "Ihrem Mehrphasen Training";
		  $videoThumb = "img/global/moto-mehrphasen.jpg";
		  $coverThumb = "img/global/moto-cover.jpg";
		  $selektorThumb = "img/global/moto-selektor.jpg";
		  break;
		  case "MOX" :
		  $traininglong = "Ihrem Mopedführerschein AM";
		  $videoThumb = "img/global/moped.jpg";
		  $coverThumb = "img/global/moto-cover.jpg";
		  $selektorThumb = "img/global/moto-selektor.jpg";
		  break;
		  case "SMT" :
		  $traininglong = "Ihrem Speed Training";
		  $videoThumb = "img/global/moto-speed.jpg";
		  $coverThumb = "img/global/moto-cover.jpg";
		  $selektorThumb = "img/global/moto-selektor.jpg";
		  break;
		  case "MTT" :
		  $traininglong = "Ihrem Trial Training";
		  $videoThumb = "img/global/moto-trial.jpg";
		  $coverThumb = "img/global/moto-cover.jpg";
		  $selektorThumb = "img/global/moto-selektor.jpg";
		  break;
		  case "MWU" :
		  $traininglong = "Ihrem Warm up";
		  $videoThumb = "img/global/moto-warmup.jpg";
		  $coverThumb = "img/global/moto-cover.jpg";
		  $selektorThumb = "img/global/moto-selektor.jpg";
		  break;
		  default :
		  $traininglong = "Ihrem Aktiv Training";
		  $videoThumb = "img/global/pkw-aktiv.jpg";
		  break;
	  }
	  
	  
	 
  
  }

$gutscheincode = "ICH-WILL-MEHR";

$today = date('m');

if($today >= 10 && $today <= 12){
	$winter = 1;
}
	
	
	
mysqli_close($con);	
?>
