 <div class="page2top reset"><img src="img/page2/hoch-gelb-back2.png" /></div>
			
			
			
			<div class="header-subtext2 reset fg-cond"> Das war Ihr Training. </div>
			
			<div class="index-menu reset fg-cond">
				<div class="page2-background"><img src="img/page2/hintergrund-2.png" /></div>
				
				<div class="link1"><img src="img/page2/produkte.png" /></div>
	
				<div class="link2"><img src="img/page2/trainingstyp.png" /></div>
				
				<div class="link3"><img src="img/page2/kundenportal.png" /></div>
				
				<div class="link4"><img src="img/page2/fotogalerie.png" /></div>
				
				<div class="link5"><img src="img/page2/fahrtechnik-2.png" /></div>
	
				<div class="text-link1">Trainings<br>der ÖAMTC<br>Fahrtechnik </div>
				
				<div class="text-link2">Welcher<br>Trainingstyp<br>sind Sie?</div>
				
				<div class="text-link3">Kundenportal<br>"Meine Fahrtechnik"</div>
				
				<div class="text-link4">
					
					<?php 
						$getLength = file_get_contents('http://oeamtc1.it-wms.com/archive/years/group_ajax.php?callback=meincallback&group='.$bildercode);
						if ($bildercode == '388991') { ?>
					Ihr persönliches<br/>Trainingsfoto
					<?php } else if($getLength != "meincallback(empty)"){ ?>
					Fotos von Ihrem Training
					<?php } else { ?>
					Ihre Fahrtechnik<br/>Fotogalerie
					<?php } ?>
				</div>
	
				<div class="text-link5">Fahrtechnik<br>Zentren</div>
			
			</div>

<div class="video-wrapper reset">		
<img src="img/page2/btn-play.png" id="play" class="video1" />	
	<?php 
	//echo $trainincode;
	switch($trainincode) {
			case "PIT" : ?>
				<video id="59de3905" controls="true" preload="auto" class="sublime" poster="/videos/01pkwaktivtrainingcrm.jpg" width="1024" height="576" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/01pkwaktivtrainingcrm.ipad.mp4" />
  <source src="/videos/01pkwaktivtrainingcrm.ipad.webmsd.webm" />
</video>

			<?php break;
			case "P2T": ?>
			
				<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/02pkwdynamiktrainingcrm.jpg" width="1024" height="576" title="PKW Dynamik Training" data-uid="59de3905">
  <source src="/videos/02pkwdynamiktrainingcrm.ipad.mp4" />
  <source src="/videos/02pkwdynamiktrainingcrm.ipad.webmsd.webm" />
</video>	

<?php break;
			case "PIN": ?>
			
				<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/PKW_IntensivTraining_CRM_converted.jpg" width="1024" height="576" title="PKW Intensiv Training" data-uid="59de3905">
  <source src="/videos/PKW_IntensivTraining_CRM_converted.mp4" />
  <source src="/videos/PKW_IntensivTraining_CRM_converted.webm" />
</video>	
<?php break;
			case "P3T": ?>
			
				<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/PKW_Speed_Training_CRM_converted.jpg" width="1024" height="576" title="PKW Speed Training" data-uid="59de3905">
  <source src="/videos/PKW_Speed_Training_CRM_converted.mp4" />
  <source src="/videos/PKW_Speed_Training_CRM_converted.webm" />
</video>					
				
				
			<?php break;
			case "DR1": ?>
			
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/03pkwdrifttrainingcrm.jpg" width="1024" height="576" title="PKW Drift Training" data-uid="59de3905">
  <source src="/videos/03pkwdrifttrainingcrm.ipad.mp4" />
  <source src="/videos/03pkwdrifttrainingcrm.ipad.webmsd.webm" />
</video>
			<?php break;
			case "PMF": ?>
			
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/04pkwmehrphasecrm.jpg" width="1024" height="576" title="PKW Mehrphasen Training" data-uid="59de3905">
  <source src="/videos/04pkwmehrphasecrm.ipad.mp4" />
  <source src="/videos/04pkwmehrphasecrm.ipad.webmsd.webm" />
</video>
			<?php break;
			case "OF1": ?>
			<video id="59de3905" controls="true" class="sublime" poster="/videos/05pkwoffroadtrainingcrm.jpg" width="1024" height="576" title="PKW offroad Training" data-uid="59de3905">
  <source src="/videos/05pkwoffroadtrainingcrm.ipad.mp4" />
  <source src="/videos/05pkwoffroadtrainingcrm.ipad.webmsd.webm" />
</video>
			

			<?php break;
			case "PRE": ?>
			<video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/06pkwracingexperiencecrm.jpg" width="1024" height="576" title="PKW Racing experience" data-uid="59de3905">
  <source src="/videos/06pkwracingexperiencecrm.ipad.mp4" />
  <source src="/videos/06pkwracingexperiencecrm.ipad.webmsd.webm" />
</video>


			
			<?php break;
			case "SET": ?>
			<video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/07pkwschneeeistrainingcrm.jpg" width="1024" height="576" title="PKW Schneeundeis Training" data-uid="59de3905">
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />
</video>

<?php break;
			case "SFN": ?>
			<video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/07pkwschneeeistrainingcrm.jpg" width="1024" height="576" title="PKW Schnee und eis Training" data-uid="59de3905">
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />
</video>


<?php break;
			case "SF1": ?>
			<video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/07pkwschneeeistrainingcrm.jpg" width="1024" height="576" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />
</video>

<?php break;
			case "SF2": ?>
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/07pkwschneeeistrainingcrm.jpg" width="1024" height="576" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.mp4" />
  <source src="/videos/07pkwschneeeistrainingcrm.ipad.webmsd.webm" />
</video>
				
			<?php break;
			case "SUV": ?>
			<video  preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/08pkwsuvtrainingcrm.jpg" width="1024" height="576" title="PKW SUV Training" data-uid="59de3905">
  <source src="/videos/08pkwsuvtrainingcrm.ipad.mp4" />
  <source src="/videos/08pkwsuvtrainingcrm.ipad.webmsd.webm" />
</video>



				
			<?php break;
			case "MPX": ?>
			
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/09mot125ertrainingcrm.jpg" width="1024" height="576" title="125 Training" data-uid="59de3905">
  <source src="/videos/09mot125ertrainingcrm.ipad.mp4" />
  <source src="/videos/09mot125ertrainingcrm.ipad.webmsd.webm" />
</video>



				
			<?php break;
			case "MIT": ?>
			<video preload="auto" controls="true" id="59de3905" class="sublime" poster="/videos/10motaktivtrainingcrm.jpg" width="1024" height="576" title=" Motorrad Aktiv Training" data-uid="59de3905">
  <source src="/videos/10motaktivtrainingcrm.ipad.mp4" />
  <source src="/videos/10motaktivtrainingcrm.ipad.webmsd.webm" />
</video>

				
			<?php break;
			case "M2T": ?>
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/11motdynamiktrainingcrm.jpg" width="1024" height="576" title=" Moto Dynamik Training" data-uid="59de3905">
  <source src="/videos/11motdynamiktrainingcrm.ipad.mp4" />
  <source src="/videos/11motdynamiktrainingcrm.ipad.webmsd.webm" />
</video>



				
			<?php break;
			case "MMF": ?>
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/12motmehrphasecrm.jpg" width="1024" height="576" title="125 mehrphasen Training" data-uid="59de3905">
  <source src="/videos/12motmehrphasecrm.ipad.mp4" />
  <source src="/videos/12motmehrphasecrm.ipad.webmsd.webm" />
</video>
			<?php break;
			case "MOX": ?>
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/13motmopedtrainingcrm.jpg" width="1024" height="576" title=" Moped Training" data-uid="59de3905">
  <source src="/videos/13motmopedtrainingcrm.ipad.mp4" />
  <source src="/videos/13motmopedtrainingcrm.ipad.webmsd.webm" />
</video>

				
			<?php break;
			case "SMT": ?>
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/14motspeedtrainingcrm.jpg" width="1024" height="576" title="Moto Speed Training" data-uid="59de3905" >
  <source src="/videos/14motspeedtrainingcrm.ipad.mp4" />
  <source src="/videos/14motspeedtrainingcrm.ipad.webmsd.webm" />
</video>

			<?php break;
			case "MTT": ?>
			
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/15mottrialtrainingcrm.jpg" width="1024" height="576" title="Moto trial Training" data-uid="59de3905" >
  <source src="/videos/15mottrialtrainingcrm.ipad.mp4" />
  <source src="/videos/15mottrialtrainingcrm.ipad.webmsd.webm" />
</video>

				
			<?php break;
			case "MWU": ?>
			<video preload="auto" controls="true"  id="59de3905" class="sublime" poster="/videos/16motwarmupcrm.jpg" width="1024" height="576" title="Moto warm up Training" data-uid="59de3905" >
  <source src="/videos/16motwarmupcrm.ipad.mp4" />
  <source src="/videos/16motwarmupcrm.ipad.webmsd.webm" />
</video>

			<?php break; 
			default : ?>
<video id="59de3905" controls="true" preload="auto"  class="sublime" poster="/videos/01pkwaktivtrainingcrm.jpg" width="1024" height="576" title="PKW Aktiv Training" data-uid="59de3905">
  <source src="/videos/01pkwaktivtrainingcrm.ipad.mp4" />
  <source src="/videos/01pkwaktivtrainingcrm.ipad.webmsd.webm" />
</video>


			<?php break;
		}
	?>
	<?php if($traininglong != "") {?>
	<div class="bottom-vid"><?php echo substr($traininglong, 6); ?></div>
	<?php }else{?>
	<div class="bottom-vid">Aktiv Training</div>

	<?php }?>
</div>
