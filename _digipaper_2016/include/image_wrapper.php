<?php
// demo image wrapper for digipaper oeamtc, v 1.0 2014_07_28 Robert Schelander
$server_url = "http://oeamtc1.it-wms.com/archive/years/";	// external server url
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

$group = $_REQUEST['group'];
$imgname = urldecode($_REQUEST['imgname']);

/* check if input is valid */
if ( preg_match("/^[0-9]+$/", $group) !== 1 )
	exit();
if ( preg_match("/[^0-9tmbig\-_]/", $imgname) === 1 )
	exit();

$imgurl = $server_url . $group . "/" . $imgname . ".jpg";

function load_jpeg($imgloc)
{
    /* Attempt to open */
    $im = @imagecreatefromjpeg($imgloc);

    /* See if it failed */
    if(!$im)
    {
        $im  = imagecreatetruecolor(550, 30);
        $bgc = imagecolorallocate($im, 255, 255, 255);
        $tc  = imagecolorallocate($im, 0, 0, 0);

        imagefilledrectangle($im, 0, 0, 550, 30, $bgc);

        /* Output an error message */
        //imagestring($im, 1, 5, 5, 'Error loading image ' . $imgloc, $tc); // security: only use for debug
		imagestring($im, 1, 5, 5, 'Error loading image', $tc);
    }

    return $im;
}

header('Content-Type: image/jpeg');		// set content type to jpeg

$img = load_jpeg($imgurl);				// load external image

imagejpeg($img);						// output image
imagedestroy($img);						// destroy in memory

?>