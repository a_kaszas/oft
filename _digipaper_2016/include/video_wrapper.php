<?php
// demo video wrapper for digipaper oeamtc
$server_url = "http://oeamtc1.it-wms.com/archive/years/";	// external server url
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

$group = $_REQUEST['group'];
$videoname = urldecode($_REQUEST['videoname']);

/* check if input is valid */
if ( preg_match("/^[0-9]+$/", $group) !== 1 )
	exit();
if ( preg_match("/[^0-9tmbig\-_]/", $videoname) === 1 )
	exit();

$videourl = $server_url . $group . "/" . $videoname . ".mp4";

file_put_contents($videoname,file_get_contents($videourl));
header('Content-Description: File Transfer');
header('Content-Type: video/mp4');
header('Content-Length: ' . filesize($videourl));
header('Content-Disposition: attachment; filename=' . basename($videoname));
readfile($videourl);

?>