<div class="hochformat">
						
			<!-- <img class="simageHolder" src="img/vorlage/page1.jpg" /> -->
			
			<div class="page1top"><img src="img/page1/hoch-gelb-back.png" /></div>
			
			
			<?php if($anrede != "") { ?>
				
				<div class="header-text1"> <?php print $anrede . " " . $vorname . " " . $nachname;?>, </div>
			
			<?php } ?>
			
			
			<div class="header-subtext1"> Fahrprofis werden nicht geboren, sondern gemacht. </div>
			
			<?php 
				require_once 'include/vendor/Mobile_Detect.php';
				$detect = new Mobile_Detect;
				if($detect->isTablet()){
			?>
			<img src="img/page2/btn-play.png" id="play-start">
			<?php }?>
			
			<?php 
				if(in_array($trainincode ,array("MPX","MIT","M2T","MMF","MOX", "SMT", "MTT", "MWU"))) { ?>
					<div class="page1-background">
					
						<!-- <img src="img/page1/start-back-moto.jpg" /> -->
						<video preload poster="/intro-videos/motorrad-back.jpg" width="526" height="678">
							<source src="/intro-videos/motorrad.mp4.mp4" type="video/mp4" />
							<source src="/intro-videos/motorrad.mp4.webmsd.webm" type="video/webm" />
						</video>
						
						
					
					</div>
				<?php }else if ($trainincode == "OF1") {?>
					<div class="page1-background">
						<!-- <img src="img/page1/start-back-suv.jpg" /> -->
						<video preload poster="/intro-videos/jeep-back.jpg" width="526" height="678">
							<source src="/intro-videos/jeep.mp4.mp4" type="video/mp4" />
							<source src="/intro-videos/jeep.mp4.webmsd.webm" type="video/webm" />
						</video>
					</div>
				<?php }else{?>
					<div class="page1-background">
						<!-- <img src="img/page1/start-back-auto.jpg" /> -->
						<video preload poster="/intro-videos/pkw-back.jpg" width="526" height="678">
							<source src="/intro-videos/pkw.mp4.mp4" type="video/mp4" />
							<source src="/intro-videos/pkw.mp4.webmsd.webm" type="video/webm" />
						</video>
					</div>
				<?php }
				
				
			?>		
			

		</div>
